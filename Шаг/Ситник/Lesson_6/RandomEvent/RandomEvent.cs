﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RandomEvent
{
    class MyEventArgs : EventArgs
    {
        public int value { get; set; }
    }

    delegate void MyEventHandler(object sender, MyEventArgs e);

    class RandomEvent
    {
        public event MyEventHandler MyEvent;
        MyEventArgs arg = new MyEventArgs();
        protected void OnMyEvent(MyEventArgs e)
        {
            if (MyEvent != null)
            {
                Thread.Sleep(500);
                MyEvent(this, e);
            }
        }

        public void InitArray(int[] arr)
        {
            Random r = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = r.Next(-100, 100);
                if (arr[i] < 0)
                {
                    arg.value = arr[i];
                    OnMyEvent(arg);
                }
            }
        }

        public void ShowArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i] + " ");
            }
        }
    }
}
