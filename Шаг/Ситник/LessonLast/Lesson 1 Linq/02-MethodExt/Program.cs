﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_MethodExt
{
    static class A
    {
        public static string MyExtMethod(this string s, int a)
        {
            return s + a.ToString();
        }

        public static void MyExtMethod1(this string s, int a)
        {
            Console.WriteLine(s + a.ToString());
        }

    }

    class Program
    {
        static void Main(string[] args)
        {

            string s = "Add int - ";

            string str = s.MyExtMethod(33);
            s.MyExtMethod1(44);
            Console.WriteLine(str);
        }
    }
}
