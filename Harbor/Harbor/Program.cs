﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harbor
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding enc = Encoding.GetEncoding(1251);
            StreamReader sr = new StreamReader("1.txt", enc);
            string line;

            List Harbor = new List();

            NodeList temp;

            while (!sr.EndOfStream)
            {
                Harbor.Add(new List());
                temp = Harbor.Head;
                int i = 1;
                while(i < Harbor.Count)
                {
                    temp = temp.Next;
                    i++;
                }
                line = sr.ReadLine();
                string[] split = line.Split(new Char[] { ' ' });
                foreach (string s in split)
                {
                    if (s != "")
                        temp.list.Add(Convert.ToInt32(s));
                }
            }
            Harbor.Show();
        }
    }

    class NodeList
    {
        public int data;
        public List list;
        public NodeList Next;
        public NodeList Prev;

        public NodeList(int data)
        {
            this.data = data;
            this.list = null;
            this.Next = null;
            this.Prev = null;
        }

        public NodeList(List list)
        {
            this.data = -1;
            this.list = list;
            this.Next = null;
            this.Prev = null;
        }
    }

    class List
    {
        public NodeList Head;
        private int count;
        public int Count
        {
            get { return count; }
        }

        public List()
        {
            this.Head = null;
            this.count = 0;
        }

        public List(int data) : this()
        {
            Add(data);
        }

        public List(List list) : this()
        {
            Add(list);
        }

        public void Insert(int data, int pos)
        {
            int ans;
            if (this.Head == null)
            {
                Console.WriteLine("Список пуст, желаете вставить элемент в начало списка? Yes=1, No=0.");
                ans = Convert.ToInt32(Console.ReadLine());
                if (ans == 1)
                {
                    Add(1, data);
                }
                return;
            }
            int i = 1;
            NodeList temp = this.Head;
            while (i < pos)
            {
                if (temp.Next == null)
                {
                    Console.WriteLine("В списке {0} элементов, желаете вставить элемент на след. позицию? Yes=1, No=0.", i);
                    ans = Convert.ToInt32(Console.ReadLine());
                    if (ans == 1)
                    {
                        //Add(data);
                    }
                    return;
                }
                if (i != pos - 1)
                    temp = temp.Next;
                i++;
            }
            NodeList temp2 = temp.Next;
            temp.Next = new NodeList(data);
            temp = temp.Next;
            temp.Next = temp2;
        }

        public void Add(int data)
        {
            if (this.Head == null)
            {
                this.Head = new NodeList(data);
                this.count++;
                return;
            }
            NodeList temp = this.Head;
            int i = 1;
            while (i != this.count)
            {
                temp = temp.Next;
                i++;
            }
            temp.Next = new NodeList(data);
            temp.Next.Prev = temp;
            this.count++;
        }

        public void Add(int num, int data)
        {
            if (this.Head == null)
            {
                Console.WriteLine("Порт не содежит ни одного контейнера, желаете добавить контейнер и коробку? Yes=1, No=0.");
                int ans = Convert.ToInt32(Console.ReadLine());
                if (ans == 1)
                {
                    this.Head = new NodeList(new List(data));
                    this.count++;
                }
                return;
            }
            NodeList temp = this.Head;
            int i = 1;
            while (i != num)
            {
                temp = temp.Next;
                i++;
            }
            if (num > count)
            {
                Console.WriteLine("В порту нет контейнера с номером {0}, желаете добавить его под номером {1}? Yes=1, No=0", num, count + 1);
                int ans = Convert.ToInt32(Console.ReadLine());
                if (ans == 1)
                {
                    temp.Next = new NodeList(new List(data));
                    temp.Next.Prev = temp;
                }
                return;
            }
            temp = temp.list.Head;
            if (temp == null)
            {
                temp = new NodeList(data);
                return;
            }
            while (temp.Next != null)
            {
                temp = temp.Next;
            }
            temp.Next = new NodeList(data);
            temp.Next.Prev = temp;
            temp.list.count++;
        }

        public void Add(List list)
        {
            if (this.Head == null)
            {
                this.Head = new NodeList(list);
                this.count++;
                return;
            }
            NodeList temp = this.Head;
            int i = 1;
            while (i != this.count)
            {
                temp = temp.Next;
                i++;
            }
            temp.Next = new NodeList(list);
            temp.Next.Prev = temp;
            this.count++;
        }

        public void Remove()
        {
            if (this.Head == null)
            {
                Console.WriteLine("Пустой список, нечего удалять!");
                return;
            }
            count--;
            if (count == 0)
            {
                this.Head = null;
                return;
            }
            NodeList temp = this.Head;
            int i = 1;
            while (i++ < count)
            {
                temp = temp.Next;
            }
            temp.Next = null;
        }

        public void Show()
        {
            NodeList temp = this.Head;
            if (temp == null)
            {
                Console.WriteLine("Порт пуст!");
                return;
            }
            int i = 1;
            while (temp != null)
            {
                NodeList temp2 = temp.list.Head;
                if (temp2 == null)
                {
                    Console.WriteLine("Контейнер номер {0} пуст!", i);
                    temp = temp.Next;
                    i++;
                    continue;
                }
                Console.WriteLine("Содержимое контейнера номер {0}:", i);
                int j = 1;
                while(temp2 != null)
                {
                    Console.WriteLine("Ящик номер {0} содержит яблок: {1}.", j, temp2.data);
                    temp2 = temp2.Next;
                    j++;
                }
                i++;
                temp = temp.Next;
            }
        }
    }
}
