﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Same_level
{
    class Program
    {
        static void Main(string[] args)
        {
            Node tmp = new Node(1);
            tmp.left = new Node(2);
            tmp.right = new Node(3);
            tmp.left.left = new Node(4);
            tmp.right.left = new Node(5);
            tmp.right.right = new Node(6);
            linkSameLevel(tmp);
            Console.Read();
        }

        public static void linkSameLevel(Node t)
        {
            Queue<Node> q = new Queue<Node>();
            int i = 1;
            q.Enqueue(t);
            while (q.Count > 0)
            {
                Node tmp = q.Dequeue();
                if (tmp.left != null)
                    q.Enqueue(tmp.left);
                if (tmp.right != null)
                    q.Enqueue(tmp.right);
                if (i > 1)
                {
                    tmp.level = q.Peek();
                    i--;
                }
                else
                    i = q.Count;
            }
        }

        public class Node
        {
            public int n; // value of node
            public Node left; // left subtree
            public Node right; // right subtree
            public Node level; // level pointer (node “to the right”)

            public Node(int n)
            {
                this.n = n;
                this.left = null;
                this.right = null;
                this.level = null;
            }
        }
    }
}
