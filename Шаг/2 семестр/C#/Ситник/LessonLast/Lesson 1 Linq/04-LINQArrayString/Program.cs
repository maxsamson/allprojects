﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_LINQArrayString
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] cars = { "Alfa Romeo", "Aston Martin", "Audi", "Nissan", "Chevrolet", "Chrysler", "Dodge", "BMW", "Ferrari", "Bentley", "Ford", "Lexus", "Mercedes", "Toyota", "Volvo", "Subaru", "Жигули:)" };

            //var lQry = cars.Where(delegate(string t) { return t.Length == 4; });
            //var lQry = cars.Where(t => t.Length == 4);

            var lQry = from p in cars
                       where p.Length == 4
                       select p;
                           


            foreach (var item in lQry)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("------");


            var lQry2 = cars
                .Where(s => s.StartsWith("C", StringComparison.OrdinalIgnoreCase));
                //.ToList();

            foreach (var item in lQry2)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("-------------");
            cars[cars.Length - 1] = "ccc";
            foreach (var item in lQry2)
            {
                Console.WriteLine(item);
            }

            string[] numbers = { "40", "5", "2012", "17621", "5" };
            var tmp1 = numbers.OrderByDescending(n => n);
            foreach (var item in tmp1)
            {
                Console.WriteLine(item);
            }

            var t1 = numbers.Select(p => Int32.Parse(p));
            var t2 = t1.Distinct();
            var t3 = t2.OrderBy(p => p);
            
            var tmpnumbers = numbers
                .Select(p => Int32.Parse(p))
                .Distinct()
                .OrderBy(p => p)
                .ToList()
                .ForEach(Console.WriteLine);

            foreach (var item in tmpnumbers)
            {
                Console.WriteLine(item);
            }
        }
    }
}
