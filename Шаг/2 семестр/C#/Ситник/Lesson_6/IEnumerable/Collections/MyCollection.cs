﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerable.Collection
{
    class MyCollection : IEnumerable
    {
        readonly Point[] elements = new Point[4];

        public Point this[int index]
        {
            get { return elements[index]; }
            set { elements[index] = value; }
        }


    }
}
