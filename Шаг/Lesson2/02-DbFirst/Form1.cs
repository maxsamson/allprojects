﻿using _02_DbFirst.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.Entity;

namespace _02_DbFirst
{
    public partial class Form1 : Form
    {
        HumanResources context = new HumanResources();


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            context.Employees.Load();
            context.EmpPromotions.Load();

            cbEmployee.DisplayMember = "FullName";
            cbEmployee.ValueMember = "EmployeeId";
            var cbData = (from emp in context.Employees.Local
                          select new { emp.EmployeeId, FullName = emp.FirstName + " " + emp.LastName }).ToList();
            cbEmployee.DataSource = cbData;

            //foreach (var item in context.Employees.Local)
            //{
            //    cbEmployee.Items.Add(item.FirstName + "" + item.LastName);cbEmployee.SelectedIndex = 0;
            //}

        }

        private void cbEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(cbEmployee.SelectedValue);

            var dataProms = (from p in context.EmpPromotions.Local
                             where p.EmployeeId == id
                             select p).ToList();
            dgvPromotion.DataSource = dataProms;
        }
    }
}
