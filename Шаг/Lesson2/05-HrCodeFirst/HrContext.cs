﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_HrCodeFirst
{
    public class HrContext : DbContext
    {
        public HrContext() : base("name=Hr")
        {
            Database.SetInitializer<HrContext>(new DropCreateDatabaseAlways<HrContext>());
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmpPromotion> EmpPromotions { get; set; }
        public DbSet<JobTitle> JobTitles { get; set; }
    }
}
