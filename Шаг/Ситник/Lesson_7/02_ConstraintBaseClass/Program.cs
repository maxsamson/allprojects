﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ConstraintBaseClass
{
    class Program
    {
        static void Main(string[] args)
        {
            PhoneList<Friend> friend = new PhoneList<Friend>();

            friend.Add(new Friend("Vasja", "11111111", false));
            friend.Add(new Friend("Kolja", "11111112", false));

            PhoneList<Supplier> supplier = new PhoneList<Supplier>();
            supplier.Add(new Supplier("Kuzja", "2121111"));

            //PhoneList<Customer> customer = new PhoneList<Customer>();
        }
    }
}
