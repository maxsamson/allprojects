﻿using IEnumerable.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            Collection.MyCollection collection = new Collection.MyCollection();
            collection[0] = new Point(1, 2);
            collection[1] = new Point(3, 4);
            collection[2] = new Point(5, 6);
            collection[3] = new Point(7, 8);
        }
    }
}
