#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <string>
#include <locale.h>
#include <time.h>

using namespace std;

void MaxIndex(int A[], int size, int max);

void main()
{
	setlocale(LC_ALL,"RUS");
	srand(time(NULL));
	const int size=10;
	int A[size];
	for (int i = 0; i < size; i++)
		A[i] = rand() % 10;
	for (int i = 0; i < size; i++)
		cout << A[i] << " ";
	cout << endl;
	int max = 0;
	MaxIndex(A, size-1, max);
	cout << endl;
	system("pause");
}

void MaxIndex(int A[], int size, int max)
{
	if (A[size] >= A[max])
		max = size;
	if (size > 0)
		MaxIndex(A, size - 1, max);
	else
		cout << max;
}