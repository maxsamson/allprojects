﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_GenericType
{
    class A
    {

    }

    class MyObj<T>
    {
        T obj;
        public MyObj(T obj)
        {
            this.obj = obj;
        }
        public void objectType()
        {
            Console.WriteLine("Value: {0}, Тип объекта: {1}", obj, typeof(T));
        }
    }

    class MyObjects<T, V, E>
    {
        T obj1;
        V obj2;
        E obj3;
        public MyObjects(T obj1, V obj2, E obj3)
        {
            this.obj1 = obj1;
            this.obj2 = obj2;
            this.obj3 = obj3;
        }
        public void objectsType()
        {
            Console.WriteLine("\nТип объекта 1: " + typeof(T) + "\nТип объекта 2: " + typeof(V) + "\nТип объекта 3: " + typeof(E));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyObj<int> intMyObj = new MyObj<int>(21);
            intMyObj.objectType();

            Console.WriteLine("-----------------\n");

            MyObj<string> stringMyObj = new MyObj<string>("Hello!");
            stringMyObj.objectType();

            Console.WriteLine("-----------------\n");

            MyObj<A> aMyObj = new MyObj<A>(new A());
            aMyObj.objectType();

            Console.WriteLine("-----------------\n");

            MyObjects<string, int, char> my = new MyObjects<string, int, char>("Hello", 21, 'O');
            my.objectsType();
        }
    }
}
