﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using System.Windows.Forms.DataVisualization.Charting;

namespace TestFormApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            #region
            /*
            //создаем элемент Chart
            var myChart = new Chart();
            //кладем его на форму и растягиваем на все окно.
            myChart.Parent = this;
            myChart.Dock = DockStyle.Fill;
            //добавляем в Chart область для рисования графиков, их может быть
            //много, поэтому даем ей имя.
            myChart.ChartAreas.Add(new ChartArea("Math functions"));
            //Создаем и настраиваем набор точек для рисования графика, в том
            //не забыв указать имя области на которой хотим отобразить этот
            //набор точек.
            var mySeriesOfPoint = new Series("Sinus");
            mySeriesOfPoint.ChartType = SeriesChartType.Line;
            mySeriesOfPoint.ChartArea = "Math functions";
            for (var x = -Math.PI; x <= Math.PI; x += Math.PI / 10.0)
            {
                mySeriesOfPoint.Points.AddXY(x, Math.Sin(x));
            }
            //Добавляем созданный набор точек в Chart
            myChart.Series.Add(mySeriesOfPoint);
            */
            #endregion


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string X = DateTime.Now.ToString("HH:mm:ss");
            chart1.Series[0].Points.AddXY(X, 5);
            EditMessageBox.InputBox();
        }

        private void chart1_MouseClick(object sender, MouseEventArgs e)
        {
            var hitTestResult = chart1.HitTest(e.X, e.Y);

            if (hitTestResult.ChartElementType == ChartElementType.DataPoint)
            {
                foreach (var annotation in chart1.Annotations)
                {
                    if (annotation.AnchorDataPoint.Equals(hitTestResult.Series.Points[hitTestResult.PointIndex]))
                    {

                    }
                }
            }
        }
    }
}
