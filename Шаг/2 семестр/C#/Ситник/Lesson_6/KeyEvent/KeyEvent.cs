﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyEvent
{
    class KeyEventArgs : EventArgs
    {
        public char cKey;
    }

    delegate void MyEventHandler(object sender, KeyEventArgs e);

    class KeyEvent
    {
        public event MyEventHandler KeyPress;

        public void OnKeyPress(char key)
        {
            KeyEventArgs arg = new KeyEventArgs();
            if (KeyPress != null)
            {
                arg.cKey = key;
                KeyPress(this, arg);
            }
        }
    }
}
