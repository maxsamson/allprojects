
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>
#include <conio.h>

#define N 10// ������ ����
#define M 20//������ ����



enum Key { M_UP = 72, M_LEFT = 75, M_RIGHT = 77, M_DOWN = 80, M_ESC = 27, M_ENTER = 13, M_Tab = 9 };




struct Records
{
	int Number;
	char Name[20];
	int Score;
};


Records Record[11];//������ ��� �������� ��������
char Temp = 0;//������ ��� ������� ������
char Next = 0;//������ ��� ��������� ������
int Side[M + 1][N + 1];//������, �������� ��� ����
COORD Center;//�����, ������ �������� �������������� ������
COORD Next_Fig[4];//������ ���������� ��������� ������
COORD tmp[4];//������ ���������� ������� ������
COORD FirstPOS;//������ ������ ������ ����
COORD NullPos;//������� ������ ����
COORD NextFigPos;//������� ��������� ������
COORD ScorePos;//�������, ��� ������������ ����
char Name[20];//��� ������
char Pos = 0;//������� ��������
int Score = 0;//����
int level = 1;//�������
int timer;//����� ������ �������
int lines = 0;//����� �� ����.������


void Pr()
{
	FILE *f;
	f = fopen("1.txt", "w");
	for (int i = 0; i <= M; i++)
	{
		for (int j = 0; j <= N; j++)
			fprintf(f, "%d ", Side[i][j]);
		fprintf(f, "\n");
	}
	fclose(f);
}

void gotoxy(COORD i)//������� � ������
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsole, i);
}


void Print_Figures(COORD coord, char c)//����� �����
{
	gotoxy(coord);
	printf("%c", c);
}




void Sort()//����������
{
	Records tmp;
	int min = Record[0].Score;
	int idiv, jmin = 0;
	for (idiv = 0; idiv<10; idiv++)
	{
		for (int j = idiv; j<11; j++)
			if (Record[j].Score >= min)
			{
				min = Record[j].Score;
				jmin = j;
			}
		tmp = Record[jmin];
		Record[jmin] = Record[idiv];
		Record[idiv] = tmp;
		min = Record[10].Score;


	}
}

int strlength(const char *str)
{
	int i = 0;
	while (str[i] != '\0'&&str[i] != '\n')
	{
		i++;
	}
	return i;
}

void RecordsDialog()//������ ��������
{
	int j, k, i, length;
	char str[256];
	char strSc[50];
	FILE *f;
	f = fopen("Records.txt", "r");
	for (i = 0; i<10; i++)
	{
		Record[i].Number = i + 1;
		fgets(str, 256, f);
		length = strlength(str);
		j = 0;
		while (!isalpha(str[j]))
			j++;
		k = 0;
		while (isalpha(str[j]))
		{
			Record[i].Name[k] = str[j];
			j++;
			k++;
		}
		while (!isdigit(str[j]))
			j++;
		k = 0;
		while (isdigit(str[j]) && j<length)
		{
			strSc[k] = str[j];
			j++;
			k++;
		}
		Record[i].Score = atoi(strSc);
		for (int t = 0; t<50; t++)
			strSc[t] = '\0';
	}
	fclose(f);
	k = strlength(Name);
	for (i = 0; i<k; i++)
		Record[10].Name[i] = Name[i];
	Record[10].Score = Score;
	Record[10].Number = 11;
	if (Record[10].Score == 0)
	{
		for (i = 0; i<10; i++)
			printf("%d.  %s - %d\n", Record[i].Number, Record[i].Name, Record[i].Score);
		return;
	}
	else
	{
		Sort();
		int GamerNum = 11;
		for (i = 0; i<10; i++)
		{
			if (Record[i].Number == 11) GamerNum = i + 1;
			Record[i].Number = i + 1;
			printf("%d.  %s - %d\n", Record[i].Number, Record[i].Name, Record[i].Score);
		}
		printf("\n\n");
		if (GamerNum<10)
			printf("CONGRATULATIONS!!! YOUR PlACE IS %d\n", GamerNum);
		else
			printf("Next time you`ll win!\n\n");
		f = fopen("Records.txt", "w");
		for (i = 0; i<10; i++)
			fprintf(f, "%d.  %s - %d\n", Record[i].Number, Record[i].Name, Record[i].Score);
		fclose(f);
	}

	system("pause");

}

void Dialog()//������
{
	char c = '\0';
	COORD Instruction;//���������� ����������
	Instruction.X = N + 20;
	Instruction.Y = 0;
	printf("Please, write your name\n");
	scanf("%20s", Name);
	system("cls");
	printf("Hi %s!\nPlease choose level of game 1-20.\n", Name);
	scanf_s("%d", &level);
	if (level<1)
		level = 1;
	if (level>20)
		level = 20;
	system("cls");
	printf("Press Enter to start!\n");
	printf("Press Tab to open Records\n\n");
	while (1)
	{
		if (_kbhit())
		{
			c = _getch();
			if (c == '\0')
				c = _getch();
		}
		if (c == M_Tab)
		{
			RecordsDialog();
			c = '\0';
		}
		if (c == M_ENTER)
		{
			system("cls");
			gotoxy(Instruction);
			printf("Press Esc to exit");//����� �����������
			gotoxy(ScorePos);
			printf("Score : %d", Score);//����� ����� 
			ScorePos.Y++;
			gotoxy(ScorePos);
			printf("Level %d", level);//����� level 
			ScorePos.Y++;
			gotoxy(ScorePos);
			printf("Lines to next Level %d", level * 10);//����� level 
			ScorePos.Y = ScorePos.Y - 2;
			return;
		}
	}
}

void PrintArray(int m, int n, COORD coord)//��������� ����� �������
{
	gotoxy(coord);
	for (int i = 0; i <= m; i++)
	{
		for (int j = 0; j <= n; j++)
			if (j == 0 || i == m || j == n)
				printf("%c", '#');
			else
				printf("%c", ' ');
		printf("\n");
	}
}


void PrintSecArray(int m, int n, COORD coord)//��������� ����� �������, ��� ������������ ��������� ������
{
	coord.Y = FirstPOS.Y - 1;
	gotoxy(coord);
	for (int i = 0; i <= m; i++)
	{
		gotoxy(coord);
		for (int j = 0; j <= n; j++)
			if (i == 0 || j == 0 || i == m || j == n)
				printf("%c", '#');
			else
				printf("%c", ' ');
		printf("\n");
		coord.Y++;
	}
}

void RePrintArray(COORD coord)//����� ������� ��� ������������ �����
{
	Pr();
	for (int i = 1; i<M; i++)
	{
		gotoxy(coord);
		for (int j = 1; j<N; j++)
		{
			if (Side[i][j] == 1)
				printf("%c", 178);
			else
				printf(" ");
		}
		printf("\n");
		coord.Y++;
	}
}





void Set_Figure(int num, int n)//��������� ��������� ������
{

	switch (num)
	{
	case 0://Line
		tmp[0].X = n / 2 - 2;
		tmp[0].Y = 2;
		for (int i = 1; i<4; i++)
		{
			tmp[i].X = tmp[i - 1].X + 1;
			tmp[i].Y = tmp[i - 1].Y;
		}
		Center = tmp[1];
		break;
	case 1://L-block
		tmp[0].X = n / 2 - 1;
		tmp[0].Y = 1;
		tmp[1].X = tmp[0].X;
		tmp[1].Y = tmp[0].Y + 1;
		for (int i = 2; i<4; i++)
		{
			tmp[i].X = tmp[i - 1].X + 1;
			tmp[i].Y = tmp[i - 1].Y;
		}
		Center.X = tmp[0].X + 1;
		Center.Y = tmp[0].Y;
		break;
	case 2://Reverse L-block
		tmp[0].X = n / 2 + 1;
		tmp[0].Y = 2;
		tmp[1].Y = tmp[0].Y + 1;
		tmp[1].X = tmp[0].X;
		for (int i = 2; i<4; i++)
		{
			tmp[i].X = tmp[i - 1].X - 1;
			tmp[i].Y = tmp[i - 1].Y;
		}
		Center.X = tmp[0].X - 1;
		Center.Y = tmp[0].Y;
		break;

	case 3://Square
		tmp[0].X = n / 2 - 2;
		tmp[0].Y = 1;
		tmp[1].X = tmp[0].X + 1;
		tmp[1].Y = tmp[0].Y;
		tmp[2].X = tmp[0].X;
		tmp[2].Y = tmp[0].Y + 1;
		tmp[3].X = tmp[1].X;
		tmp[3].Y = tmp[2].Y;

		break;
	case 4://Z-block
		tmp[0].X = n / 2 - 1;
		tmp[0].Y = 2;
		tmp[1].X = tmp[0].X + 1;
		tmp[1].Y = tmp[0].Y;
		tmp[2].X = tmp[1].X;
		tmp[2].Y = tmp[0].Y - 1;
		tmp[3].X = tmp[1].X + 1;
		tmp[3].Y = tmp[2].Y;
		Center = tmp[1];
		break;
	case 5://Reverse Z-block
		tmp[0].X = n / 2 - 1;
		tmp[0].Y = 1;
		tmp[1].X = tmp[0].X + 1;
		tmp[1].Y = tmp[0].Y;
		tmp[2].X = tmp[1].X;
		tmp[2].Y = tmp[0].Y + 1;
		tmp[3].X = tmp[1].X + 1;
		tmp[3].Y = tmp[2].Y;
		Center = tmp[1];
		break;
	case 6://T-block
		tmp[0].X = n / 2 - 1;
		tmp[0].Y = 2;
		tmp[1].X = tmp[0].X + 1;
		tmp[1].Y = tmp[0].Y;
		tmp[2].X = tmp[1].X;
		tmp[2].Y = tmp[1].Y - 1;
		tmp[3].X = tmp[2].X + 1;
		tmp[3].Y = tmp[1].Y;
		Center = tmp[1];
		break;


	}
	Pos = 0;


}



void SetN_Figure(int num, int n)//��������� ��������� ��������� ������
{

	switch (num)
	{
	case 0://Line
		Next_Fig[0].X = n / 2 - 2;
		Next_Fig[0].Y = 3;
		for (int i = 1; i<4; i++)
		{
			Next_Fig[i].X = Next_Fig[i - 1].X + 1;
			Next_Fig[i].Y = Next_Fig[i - 1].Y;
		}
		break;
	case 1://L-block
		Next_Fig[0].X = n / 2 - 1;
		Next_Fig[0].Y = 3;
		Next_Fig[1].X = Next_Fig[0].X;
		Next_Fig[1].Y = Next_Fig[0].Y + 1;
		for (int i = 2; i<4; i++)
		{
			Next_Fig[i].X = Next_Fig[i - 1].X + 1;
			Next_Fig[i].Y = Next_Fig[i - 1].Y;
		}
		break;
	case 2://Reverse L-block
		Next_Fig[0].X = n / 2 + 1;
		Next_Fig[0].Y = 3;
		Next_Fig[1].Y = Next_Fig[0].Y + 1;
		Next_Fig[1].X = Next_Fig[0].X;
		for (int i = 2; i<4; i++)
		{
			Next_Fig[i].X = Next_Fig[i - 1].X - 1;
			Next_Fig[i].Y = Next_Fig[i - 1].Y;
		}
		break;

	case 3://Square
		Next_Fig[0].X = n / 2 - 2;
		Next_Fig[0].Y = 3;
		Next_Fig[1].X = Next_Fig[0].X + 1;
		Next_Fig[1].Y = Next_Fig[0].Y;
		Next_Fig[2].X = Next_Fig[0].X;
		Next_Fig[2].Y = Next_Fig[0].Y + 1;
		Next_Fig[3].X = Next_Fig[1].X;
		Next_Fig[3].Y = Next_Fig[2].Y;

		break;
	case 4://Z-block
		Next_Fig[0].X = n / 2 - 1;
		Next_Fig[0].Y = 3;
		Next_Fig[1].X = Next_Fig[0].X + 1;
		Next_Fig[1].Y = Next_Fig[0].Y;
		Next_Fig[2].X = Next_Fig[1].X;
		Next_Fig[2].Y = Next_Fig[0].Y - 1;
		Next_Fig[3].X = Next_Fig[1].X + 1;
		Next_Fig[3].Y = Next_Fig[2].Y;
		break;
	case 5://Reverse Z-block
		Next_Fig[0].X = n / 2 - 1;
		Next_Fig[0].Y = 3;
		Next_Fig[1].X = Next_Fig[0].X + 1;
		Next_Fig[1].Y = Next_Fig[0].Y;
		Next_Fig[2].X = Next_Fig[1].X;
		Next_Fig[2].Y = Next_Fig[0].Y + 1;
		Next_Fig[3].X = Next_Fig[1].X + 1;
		Next_Fig[3].Y = Next_Fig[2].Y;
		break;
	case 6://T-block
		Next_Fig[0].X = n / 2 - 1;
		Next_Fig[0].Y = 3;
		Next_Fig[1].X = Next_Fig[0].X + 1;
		Next_Fig[1].Y = Next_Fig[0].Y;
		Next_Fig[2].X = Next_Fig[1].X;
		Next_Fig[2].Y = Next_Fig[1].Y - 1;
		Next_Fig[3].X = Next_Fig[2].X + 1;
		Next_Fig[3].Y = Next_Fig[1].Y;
		break;


	}
	Pos = 0;


}

void Set_Game()//��������� ��������� ����
{
	for (int i = 0; i <= M; i++)
		for (int j = 0; j <= N; j++)
			if (i == 0 || j == 0 || i == M || j == N)
				Side[i][j] = 1;
			else
				Side[i][j] = 0;
	srand(time(NULL));
	Center.X = 0;
	Center.Y = 0;
	NullPos = Center;
	Temp = rand() % 7;
	Set_Figure(Temp, N);
	Next = rand() % 7;
	FirstPOS.X = 1;
	FirstPOS.Y = 1;
	NextFigPos.X = N + 5;
	NextFigPos.Y = 4;
	SetN_Figure(Next, (NextFigPos.X) * 2 + 9);
	ScorePos.X = NextFigPos.X;
	ScorePos.Y = M - 1;

}

void Random_Fig()//��������� ����� ������
{

	Temp = Next;
	Next = rand() % 7;
	Set_Figure(Temp, N);
	SetN_Figure(Next, (NextFigPos.X) * 2 + 9);

}

void Turn()//�������
{
	if ((Center.X == 1 || Center.X >= N - 2) && (Temp == 0))
		return;
	if ((Temp == 1 || Temp == 2 || Temp == 5 || Temp == 4) && (Center.X == 1 || Center.X>N - 2))
		return;
	for (int i = 0; i<4; i++)
	{
		Side[tmp[i].Y][tmp[i].X] = 0;
		Print_Figures(tmp[i], ' ');
	}
	switch (Temp)
	{

	case 0:

		if (Pos == 0)
		{

			Pos = 1;
			tmp[0].X = Center.X;
			tmp[0].Y = Center.Y - 1;
			tmp[2].X = Center.X;
			tmp[2].Y = Center.Y + 1;
			tmp[3].X = Center.X;
			tmp[3].Y = Center.Y + 2;
		}
		else
		{
			Pos = 0;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X - 1;
			tmp[2].Y = Center.Y;
			tmp[2].X = Center.X + 1;
			tmp[3].Y = Center.Y;
			tmp[3].X = Center.X + 2;
		}

		break;
	case 1:
		if (Pos == 0)
		{
			Pos = 1;
			tmp[0].X = Center.X;
			tmp[0].Y = Center.Y - 1;
			tmp[1].X = Center.X - 1;
			tmp[1].Y = Center.Y - 1;
			tmp[2].X = Center.X - 1;
			tmp[2].Y = Center.Y;
			tmp[3].X = Center.X - 1;
			tmp[3].Y = Center.Y + 1;
		}
		else if (Pos == 1)
		{
			Pos = 2;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X + 1;
			tmp[1].X = Center.X + 1;
			tmp[1].Y = Center.Y - 1;
			tmp[2].Y = Center.Y - 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y - 1;
			tmp[3].X = Center.X - 1;
		}

		else if (Pos == 2)
		{
			Pos = 3;
			tmp[0].Y = Center.Y + 1;
			tmp[0].X = Center.X;
			tmp[1].X = Center.X + 1;
			tmp[1].Y = Center.Y + 1;
			tmp[2].Y = Center.Y;
			tmp[2].X = Center.X + 1;
			tmp[3].Y = Center.Y - 1;
			tmp[3].X = Center.X + 1;
		}

		else if (Pos == 3)
		{
			Pos = 0;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X - 1;
			tmp[1].X = Center.X - 1;
			tmp[1].Y = Center.Y + 1;
			tmp[2].Y = Center.Y + 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y + 1;
			tmp[3].X = Center.X + 1;
		}

		break;
	case 2:
		if (Pos == 0)
		{
			Pos = 1;
			tmp[0].X = Center.X;
			tmp[0].Y = Center.Y + 1;
			tmp[1].X = Center.X - 1;
			tmp[1].Y = Center.Y + 1;
			tmp[2].X = Center.X - 1;
			tmp[2].Y = Center.Y;
			tmp[3].X = Center.X - 1;
			tmp[3].Y = Center.Y - 1;
		}
		else if (Pos == 1)
		{
			Pos = 2;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X - 1;
			tmp[1].X = Center.X - 1;
			tmp[1].Y = Center.Y - 1;
			tmp[2].Y = Center.Y - 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y - 1;
			tmp[3].X = Center.X + 1;
		}

		else if (Pos == 2)
		{
			Pos = 3;
			tmp[0].Y = Center.Y - 1;
			tmp[0].X = Center.X;
			tmp[1].X = Center.X + 1;
			tmp[1].Y = Center.Y - 1;
			tmp[2].Y = Center.Y;
			tmp[2].X = Center.X + 1;
			tmp[3].Y = Center.Y + 1;
			tmp[3].X = Center.X + 1;
		}

		else if (Pos == 3)
		{
			Pos = 0;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X + 1;
			tmp[1].X = Center.X + 1;
			tmp[1].Y = Center.Y + 1;
			tmp[2].Y = Center.Y + 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y + 1;
			tmp[3].X = Center.X - 1;
		}
		break;

	case 4:
		if (Pos == 0)
		{
			Pos = 1;
			tmp[0].X = Center.X;
			tmp[0].Y = Center.Y + 1;
			tmp[2].X = Center.X - 1;
			tmp[2].Y = Center.Y;
			tmp[3].X = Center.X - 1;
			tmp[3].Y = Center.Y - 1;
		}
		else
		{
			Pos = 0;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X + 1;
			tmp[2].Y = Center.Y + 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y + 1;
			tmp[3].X = Center.X - 1;
		}
		break;
	case 5:
		if (Pos == 0)
		{
			Pos = 1;
			tmp[0].X = Center.X;
			tmp[0].Y = Center.Y - 1;
			tmp[2].X = Center.X - 1;
			tmp[2].Y = Center.Y;
			tmp[3].X = Center.X - 1;
			tmp[3].Y = Center.Y + 1;
		}
		else
		{
			Pos = 0;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X - 1;
			tmp[2].Y = Center.Y + 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y + 1;
			tmp[3].X = Center.X + 1;
		}
		break;
	case 6:
		if (Pos == 0)
		{
			Pos = 1;
			tmp[0].X = Center.X;
			tmp[0].Y = Center.Y - 1;
			tmp[2].X = Center.X + 1;
			tmp[2].Y = Center.Y;
			tmp[3].X = Center.X;
			tmp[3].Y = Center.Y + 1;
		}
		else if (Pos == 1)
		{
			Pos = 2;
			tmp[0].Y = Center.Y;
			tmp[0].X = Center.X - 1;
			tmp[2].Y = Center.Y + 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y;
			tmp[3].X = Center.X + 1;
		}

		else if (Pos == 2)
		{
			Pos = 3;
			tmp[0].Y = Center.Y + 1;
			tmp[0].X = Center.X;
			tmp[2].Y = Center.Y;
			tmp[2].X = Center.X - 1;
			tmp[3].Y = Center.Y - 1;
			tmp[3].X = Center.X;
		}

		else if (Pos == 3)
		{
			Pos = 0;
			tmp[0].X = Center.X - 1;
			tmp[0].Y = Center.Y;
			tmp[2].Y = Center.Y - 1;
			tmp[2].X = Center.X;
			tmp[3].Y = Center.Y;
			tmp[3].X = Center.X + 1;
		}
		break;


	}
	for (int i = 0; i<4; i++)
	{
		Side[tmp[i].Y][tmp[i].X] = -1;
		Print_Figures(tmp[i], 178);
	}


}


void Set_Center()//��������� ������ ������
{
	switch (Temp)
	{
	case 0://Line
		Center = tmp[1];
		break;
	case 1://L-block
		Center.X = tmp[0].X + 1;
		Center.Y = tmp[0].Y;
		break;
	case 2://Reverse L-block
		Center.Y = tmp[0].Y;
		Center.X = tmp[0].X - 1;
		break;
	case 4://Z-block
		Center = tmp[1];
		break;
	case 5://Reverse Z-block
		Center = tmp[1];
		break;
	case 6://T-block
		Center = tmp[1];
		break;


	}
}


void Shift(int change)//�������� �����-������
{
	for (int i = 0; i<4; i++)
		if (Side[tmp[i].Y][tmp[i].X + change] == 1)
			return;
	for (int i = 0; i<4; i++)
	{
		Print_Figures(tmp[i], ' ');
		Side[tmp[i].Y][tmp[i].X] = 0;
		tmp[i].X = tmp[i].X + change;
		Side[tmp[i].Y][tmp[i].X] = -1;
	}
	for (int i = 0; i<4; i++)
		Print_Figures(tmp[i], 178);
	Set_Center();
}




bool Check_Bump()//��������, ����� �� ������ �� �����
{
	bool f = false;
	for (int i = 0; i<4; i++)
		if (Side[tmp[i].Y + 1][tmp[i].X] == 1)
		{
			f = true;
			break;
		}
	if (f)
	{
		for (int i = 0; i<4; i++)
		{
			Side[tmp[i].Y][tmp[i].X] = 1;
			tmp[i].X = tmp[i].Y = 0;
		}
		Pr();
		return true;
	}
	return false;

}

bool Check_Lines()//������ ����� �� ����, ������� �����, � ���������� �����
{
	int temp_el = 0;
	bool f = false;
	for (int i = M - 1; i>0; i--)
	{
		for (int j = 1; j<N; j++)
		{
			if (Side[i][j] == 1)
				temp_el++;
		}
		if (temp_el == N - 1)
		{
			f = true;
			Score = Score + 10;
			lines--;
			gotoxy(ScorePos);
			printf("Score : %d  ", Score);//����� ����� 
			ScorePos.Y++;
			gotoxy(ScorePos);
			printf("Level %d  ", level);//����� level 
			ScorePos.Y++;
			gotoxy(ScorePos);
			printf("Lines to next Level %d  ", lines);//����� level 
			ScorePos.Y = ScorePos.Y - 2;
			for (int k = 1; k<N; k++)
				Side[i][k] = 0;
		}
		temp_el = 0;
	}
	if (f)
		return true;
	return false;


}



void Falling()//������� ������ ���� �� 1
{
	Center.Y++;
	for (int i = 0; i<4; i++)
	{
		Side[tmp[i].Y][tmp[i].X] = 0;
		Print_Figures(tmp[i], ' ');
		tmp[i].Y++;
	}
	for (int i = 0; i<4; i++)
	{
		Side[tmp[i].Y][tmp[i].X] = -1;
		Print_Figures(tmp[i], 178);
	}
}

void Del_EmptyLines()//�������� ����� �� �������
{
	int res = 0;
	int i, j, k = -1, p;
	for (i = 1; i<M; i++)
	{
		for (j = 1; j<N; j++)
		{
			if (Side[i][j] != 0)
			{
				k = i;
				break;
			}

		}
		if (k != -1)
			break;
	}
	for (i = M - 1; i>k;)
	{
		for (j = 1; j<N; j++)
		{
			if (Side[i][j] == 0)
				res++;
		}
		if (res == N - 1)
		{
			for (int o = i; o>k - 1; o--)
				for (p = 1; p<N; p++)
					Side[o][p] = Side[o - 1][p];
			k++;
		}
		else
			i--;
		res = 0;
	}


}

bool IsNextLevel()//�������� �������� �� ��������� �������
{
	int res = 0;
	for (int i = level; i>0; i--)
		res = res + i * 100;
	if (Score >= res)
		return true;
	return false;


}

bool game_over()//�������� �� ��������
{
	for (int i = 1; i<3; i++)
		for (int j = N / 2 - 2; j<N / 2 + 2; j++)
			if (Side[i][j] == 1)
				return true;
	return false;

}

void NextStep()//��������� ���, ���� �� ���� �������� ������-����� � ���������
{
	if (Check_Bump())
	{
		for (int i = 0; i<4; i++)
		{
			Print_Figures(Next_Fig[i], ' ');
		}
		if (Check_Lines())
		{
			Sleep(500);
			Del_EmptyLines();
			RePrintArray(FirstPOS);
			if (IsNextLevel())
			{
				level++;
				Set_Game();
				system("cls");
				printf("Next Level\n");
				system("pause");
				system("cls");
				PrintArray(M, N, NullPos);
				PrintSecArray(5, 7, NextFigPos);
				gotoxy(ScorePos);
				printf("Score : %d", Score);//����� ����� 
				ScorePos.Y++;
				gotoxy(ScorePos);
				printf("Level %d", level);//����� level 
				ScorePos.Y++;
				gotoxy(ScorePos);
				printf("Lines to next Level %d", lines);//����� level 
				ScorePos.Y = ScorePos.Y - 2;
				for (int i = 0; i<4; i++)
				{
					Side[tmp[i].Y][tmp[i].X] = -1;
					Print_Figures(tmp[i], 178);
					Print_Figures(Next_Fig[i], 178);
				}
				return;
			}

		}
		if (game_over())
		{
			system("cls");
			FirstPOS.Y--;
			FirstPOS.X--;
			gotoxy(FirstPOS);
			RecordsDialog();
			system("pause");
			exit(0);
		}
		Random_Fig();
		for (int i = 0; i<4; i++)
		{
			Side[tmp[i].Y][tmp[i].X] = -1;
			Print_Figures(tmp[i], 178);
			Print_Figures(Next_Fig[i], 178);

		}

	}
	else
		Falling();


}






int main()
{
	int cur;
	char ch;
	Set_Game();
	Dialog();
	lines = level * 10;
	PrintArray(M, N, NullPos);
	PrintSecArray(5, 7, NextFigPos);
	for (int i = 0; i<4; i++)
	{
		Side[tmp[i].Y][tmp[i].X] = -1;
		Print_Figures(tmp[i], 178);
		Print_Figures(Next_Fig[i], 178);
	}
	Pr();
	while (1)
	{
		timer++;
		ch = '\0';
		if (_kbhit())
		{
			ch = _getch();
			if (ch<0)
				ch = _getch();
		}
		switch (ch)
		{
		case M_LEFT:
			Shift(-1);
			break;
		case M_RIGHT:
			Shift(1);
			break;
		case M_ESC:
			exit(0);
			break;
		case M_ENTER:
			Turn();
			break;
		case M_DOWN:
			while (!Check_Bump())
				Falling();
			timer = 0;
			NextStep();
			break;
		default:
			break;
		}
		if (level<6)
			cur = 20000;
		else if (level<12)
			cur = 15000;
		else cur = 10000;
		if (timer >= cur)
		{
			timer = 0;
			NextStep();
		}


	}


}