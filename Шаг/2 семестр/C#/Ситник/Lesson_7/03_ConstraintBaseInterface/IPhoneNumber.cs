﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ConstraintBaseInterface
{
    interface IPhoneNumber
    {
        string Number { get; set; }
        string Name { get; set; }
    }

    interface IShow
    {
        void Display();
    }
}
