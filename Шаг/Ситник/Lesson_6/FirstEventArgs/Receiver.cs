﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEventArgs
{
    class Receiver1
    {
        public void Message(int a)
        {
            Console.WriteLine("Receiver1 - уже {0}!", a);
        }
    }
}
