﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassSender sender = new ClassSender();
            sender.SomeEvent += sender.ShowEvent;

            Receiver1 r1 = new Receiver1();
            Receiver2 r2 = new Receiver2();

            sender.SomeEvent += r1.Message;
            sender.SomeEvent += r2.Display;

            

            int sum = sender.Counter();
            Console.WriteLine("Summa = {0}", sum);

            Console.WriteLine("\n====== r1 не слушает ========\n");
            sender.SomeEvent -= r1.Message;
            sum = sender.Counter();
        }
    }
}
