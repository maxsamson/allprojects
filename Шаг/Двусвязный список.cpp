class Node
{
	int key;
	Node* next;
	Node* prev;

public:
	Node(int n = 0)
	{
		key = n;
		next = prev = nullptr;
	}

	void set_key(int k) 
	{
		key = k;
	}
	
	int get_key() 
	{
		return key;
	}

	void set_next(Node* n) 
	{
		next = n;
	}
	
	void set_prev(Node* p) 
	{
		prev = p;
	}

	Node* get_next() 
	{
		return next;
	}
	
	Node* get_prev() 
	{
		return prev;
	}
};

class List
{
	int size;
	Node* head;
	Node* tail;

public:
	List()
	{
		size = 0;
		head = tail = nullptr;
	}

	List(const List& obj)
	{
		head = tail = nullptr;
		size = 0;
		Node* tmp = obj.head;
		while (tmp) 
		{
			Push(tmp->get_key());
			tmp = tmp->get_next();
		}
	}

	~List()
	{
		while (size)
			Pop();
	}

	void Push(int n)
	{
		Node* tmp = new Node(n);
		if (!size) head = tail = tmp;
		else
		{
			tmp->set_prev(tail);
			tail->set_next(tmp);
			tail = tmp;
		}
		size++;
	}

	void Push_position(int pos, int n)
	{
		if (size == 0) 
			Push(n);
		else
		{
			if (pos < 1 || pos > size) 
				return;
			Node* tmp = new Node(n);
			if (pos == 1)
			{
				tmp->set_next(head);
				head->set_prev(tmp);
				head = tmp;
				size++;
				return;
			}
			Node* cur = head;
			for (int i = 1; i < (pos - 1); i++)
				cur = cur->get_next();
			tmp->set_next(cur->get_next());
			cur->get_next()->set_prev(tmp);
			tmp->set_prev(cur);
			cur->set_next(tmp);
		}
	}

	void Pop()
	{
		if (size == 0) 
			return;
		if (size == 1)
		{
			delete head;
			head = tail = nullptr;
			size = 0;
			return;
		}
		head = head->get_next();
		delete head->get_prev();
		head->set_prev(nullptr);
		size--;
	}

	void Pop_position(int pos)
	{
		if (size == 0) 
			return;
		if (pos < 1 || pos > size) 
			return;
		if (pos == 1)
		{
			Pop();
			return;
		}
		Node* cur = head;
		Node* tmp = new Node;
		for (int i = 1; i < (pos - 1); i++)
			cur = cur->get_next();
		tmp = cur->get_next();
		cur->set_next(tmp->get_next());
		tmp->get_next()->set_prev(cur);
		delete tmp;
		size--;

	}

	List& operator = (const List& obj)
	{
		if (this == &obj) 
			return *this;
		while (size) 
			Pop();
		Node* tmp = obj.head;
		while (tmp)
		{
			this->Push(tmp->get_key());
			tmp = tmp->get_next();
		}
		return *this;

	}

	void sort_asc()
	{
		if (size == 0 || size == 1) 
			return;
		Node* tmp = new Node;
		Node* node1 = head;
		Node* node2 = node1->get_next();
		for (int i = 0; i < size; i++)
		{
			node1 = head;
			node2 = node1->get_next();
			while (node2)
			{
				if (node1->get_key() > node2->get_key())
				{
					tmp->set_key(node1->get_key());
					node1->set_key(node2->get_key());
					node2->set_key(tmp->get_key());
				}
				node1 = node2;
				node2 = node2->get_next();
			}
		}
		delete tmp;
	}
	void sort_desc()
	{
		if (size == 0 || size == 1) 
			return;
		Node* tmp = new Node;
		Node* node1 = head;
		Node* node2 = node1->get_next();
		for (int i(0); i < size; i++)
		{
			Node* node1 = head;
			Node* node2 = node1->get_next();
			while (node2) {
				if (node1->get_key() < node2->get_key())
				{
					tmp->set_key(node1->get_key());
					node1->set_key(node2->get_key());
					node2->set_key(tmp->get_key());
				}
				node1 = node2;
				node2 = node2->get_next();
			}
		}
		delete tmp;
	}

	void Show()
	{
		if (size == 0)
		{
			cout << "������ ����" << endl;
			return;
		}
		Node* tmp = head;
		if (tmp)
		{
			int i = 1;
			while (tmp)
			{
				cout << "������� �" << i << ": " << tmp->get_key() << endl;
				tmp = tmp->get_next();
				i++;
			}
		}
	}
};