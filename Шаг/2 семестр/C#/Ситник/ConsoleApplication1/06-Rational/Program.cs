﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Rational
{
    class Program
    {
        static void Main(string[] args)
        {
            Rational r1 = new Rational(7, 21);
            Rational r2 = new Rational(121, 363);
            //Rational r = r1 + Rational.Zero;
            
            r1.PrintRational("7/21 = ");
            r2.PrintRational("121/363 = ");

            Console.ReadLine();
        }
    }
}
