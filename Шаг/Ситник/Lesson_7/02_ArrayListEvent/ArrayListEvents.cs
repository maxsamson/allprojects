﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ArrayListEvent
{
    public class ChangedEventArgs : EventArgs
    {
        private object item;
        private bool permit;
        public object Item
        {
            get { return (item); }
            set { item = value; }
        }
        public bool Permit
        {
            get { return (permit); }
            set { permit = value; }
        }
    }

    delegate void ChangedEventHandler(object sender, ChangedEventArgs e);

    class ArrayListEvents : ArrayList
    {
        public event ChangedEventHandler Changed;
        public virtual void OnChanged(ChangedEventArgs e)
        {
            if (Changed != null) Changed(this, e);
        }

        private ChangedEventArgs evargs = new ChangedEventArgs();

        public override int Add(object value)
        {
            int i = 0;
            evargs.Item = value;
            OnChanged(evargs);
            if (evargs.Permit)
                i = base.Add(value);
            else
                Console.WriteLine("Добавление элемента запрещено. Значение = {0}", value);
            return i;
        }

        public override object this[int index]
        {
            set
            {
                evargs.Item = value;
                OnChanged(evargs);
                if (evargs.Permit)
                    base[index] = value;
                else
                    Console.WriteLine("Замена элемента запрещена. Значение = {0}", value);
            }
            get
            {
                return (base[index]);
            }
        }
    }
}
