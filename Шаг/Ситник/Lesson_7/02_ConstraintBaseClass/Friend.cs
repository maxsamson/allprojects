﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ConstraintBaseClass
{
    class Friend : PhoneNumber
    {
        public Friend(string n, string num, bool wk) : base(n, num)
        {
            IsWorkNumber = wk;
        }
        //в класс Friend введено свойство IsWorkNumber
        public bool IsWorkNumber { get; private set; }
    }

    // Класс для телефонных номеров поставщиков. 
    class Supplier : PhoneNumber
    {
        public Supplier(string n, string num) : base(n, num) { }
    }

    class Customer
    {
        public string name { get; set; }
        public Customer(string name)
        {
            this.name = name;
        }
    }
}
