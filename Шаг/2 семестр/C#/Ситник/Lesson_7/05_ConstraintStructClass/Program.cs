﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_ConstraintStructClass
{
    //class Supplier
    //{
    //    public string Name { get; set; }

    //    public Supplier()
    //    {

    //    }

    //    public void Display()
    //    {
    //        Console.WriteLine(Name);
    //    }
    //}

    //class Customer
    //{
    //    public string Name { get; set; }

    //    public Customer()
    //    {

    //    }

    //    public Customer(string name)
    //    {
    //        Name = name;
    //    }

    //    public void Display()
    //    {
    //        Console.WriteLine(Name);
    //    }
    //}

    //class MyList<T> where T : new()
    //{
    //    List<T> phList;
    //    public MyList()
    //    {
    //        phList = new List<T>();
    //    }

    //    public bool Add(T newEntry)
    //    {
    //        phList.Add(newEntry);
    //        return true;
    //    }
    //}

    class Type1<T> where T : class
    {
        T obj;
        public void Show()
        {
            Console.WriteLine("Type {0}, Value - {1}", typeof(T), obj);
        }
    }

    class Type2<T> where T : struct
    {
        T obj;
        public void Show()
        {
            obj = default(T);
            Console.WriteLine("Type {0}, Value - {1}", obj.GetType(), obj);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Type1<string> t1 = new Type1<string>();
            Type2<int> t2 = new Type2<int>();
        }
    }
}
