﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;

namespace l4v2
{
    public partial class Form1 : Form
    {
        //-------------------------------
        //---------КОНСТАНТЫ-------------
        const float MIN_SIZE = 60, CYCLES_DOWN = 10;
        const int MIN_STARS = 1, MAX_STARS = 10;
        //-----------КОНЕЦ---------------
        //-------------------------------
        //---------КЛАСС ТОЧКИ-----------
        public class point
        {
            public double x, y;
            public point()
            {
                x = 0;
                y = 0;
            }
            public point(float x, float y)
            {
                this.x = x;
                this.y = y;
            }
            public Point ToPoint()
            {
                return new Point((int)Math.Floor(x), (int)Math.Floor(y));
            }
            public int X()
            {
                return (int)(Math.Floor(x));
            }
            public int Y()
            {
                return (int)(Math.Floor(y));
            }
        };
        //-------------------------------
        //---------РАБОЧИЙ КЛАСС---------
        public class Star
        {
            private point center;                  //точка центра
            private point end;                     //точка, куда движемся
            private point D;                       //прирост
            private double size;                      //размер квадратной звезды
            private int c_form, c_move;                    //количество циклов анимации
            private Color c;                       //цвет фигуры
            private double at;                      //темп падения/изменения размера
            public void SetAT(int a) { at = a; }
            private void CyclesCount(Form1 f)      //подсчет циклов анимации
            {
                Random r = new Random();
                //1) Посчитаем циклы уменьшения, округлим вниз
                c_form = (int)(Math.Floor((size - MIN_SIZE) / (at*CYCLES_DOWN)));
                end.x = (f.Size.Width - 2 * MIN_SIZE+1) * r.NextDouble() + MIN_SIZE;
                end.y = f.Size.Height - f.pictureBox1.Size.Height - (size - at * CYCLES_DOWN * c_form) / 2;
                //2) Посчитать циклы перемещения
                c_move = (int)(Math.Floor(Math.Sqrt(Math.Pow(end.x - center.x, 2) + Math.Pow(end.y - center.y, 2)) / (at * CYCLES_DOWN)));
                D.x = (end.x - center.x) / (c_move);
                D.y = (end.y - center.y) / (c_move);
            }
            public double GetSize() { return size; }
            public int GetCycles() { return c_form + c_move; }
            public Star(Form1 f)                          //конструктор по умолчанию
            {
                Random r = new Random();
                center = new point();
                end = new point();
                D = new point();
                double maxsize;
                do
                {
                    center.x = (f.Size.Width - 2 * MIN_SIZE + 1) * r.NextDouble() + MIN_SIZE;
                    center.y = (f.Size.Height - f.pictureBox1.Size.Height - 2 * MIN_SIZE + 1 - f.menuStrip1.Size.Height) * r.NextDouble() + f.menuStrip1.Size.Height + MIN_SIZE;
                    maxsize = Math.Min(Math.Min(center.x, f.Size.Width - center.x), Math.Min(center.y - f.menuStrip1.Size.Height, f.Size.Height - f.pictureBox1.Size.Height - center.y));
                }
                while (maxsize < MIN_SIZE);
                at = 1;
                size = (maxsize - MIN_SIZE) * r.NextDouble() + MIN_SIZE;
                c = Color.FromArgb(r.Next(255), r.Next(255), r.Next(255), 255);
                CyclesCount(f);
            }
            public Star(Form1 f, point cnt, double s, Color col, double tmp = 1d)
            {
                end = new point();
                D = new point();
                center = cnt;
                size = s;
                c = col;
                at = tmp;
                CyclesCount(f);
                display(f);
            }
            private void display(Form f)
            {
                System.Drawing.Graphics g = f.CreateGraphics();
                System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(c);
                Point[] lp = { new Point(center.X(),(int)(Math.Floor(center.y - (at*3*size)/10))),
                               new Point((int)(Math.Floor(center.x-at*size/2)),center.Y()),
                               new Point(center.X(),(int)(Math.Floor(center.y+(at*3*size)/10))),
                               new Point((int)(Math.Floor(center.x+at*size/2)),center.Y())};
                Point[] tp = { new Point(center.X(),(int)(Math.Floor(center.y - at * size / 2))),
                               new Point((int)(Math.Floor(center.x-(at*3*size)/10)),center.Y()),
                               new Point(center.X(),(int)(Math.Floor(center.y+at*size/2))),
                               new Point((int)(Math.Floor(center.x+(at*3*size)/10)),center.Y())};
                g.FillPolygon(brush1, lp);
                g.FillPolygon(brush1, tp);
                brush1.Dispose();
                g.Dispose();
            }
            public void frame(Form1 f)
            {
                if (Math.Sqrt(Math.Pow(end.y - center.y, 2) + Math.Pow(end.x - center.x, 2)) < CYCLES_DOWN) return;
                else
                {
                    if (size >= (MIN_SIZE + CYCLES_DOWN))
                        size -= at * CYCLES_DOWN;
                    else
                    {
                        center.y += D.y;
                        center.x += D.x;
                    }
                    display(f);
                }
            }
        };
        //-----------КОНЕЦ---------------
        //Не обращаем внимания
        const int SC_CLOSE = 0xF010;
        const int MF_BYCOMMAND = 0;
        const int WM_NCLBUTTONDOWN = 0x00A1;
        const int WM_NCHITTEST = 0x0084;
        const int HTCAPTION = 2;
        [DllImport("User32.dll")]
        static extern int SendMessage(IntPtr hWnd,
        int Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("User32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("User32.dll")]
        static extern bool RemoveMenu(IntPtr hMenu, int uPosition, int uFlags);
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCLBUTTONDOWN)
            {
                int result = SendMessage(m.HWnd, WM_NCHITTEST,
                IntPtr.Zero, m.LParam);
                if (result == HTCAPTION)
                    return;
            }
            base.WndProc(ref m);
        }
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            IntPtr hMenu = GetSystemMenu(Handle, false);
            RemoveMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
        }
        //Аж до сюда =))
        //-------------------------------
        //---------ПЕРЕМЕННЫЕ------------
        Star[] S;                       //звёзды
        int mnof;                       //максимальное число циклов
        bool init = false;              //неинициализированные данные
        bool mode = false, mode2 = false;              //false - ускорение, true - замедление
        //-----------КОНЕЦ---------------
        //-------------------------------
        //----------ФУНКЦИИ--------------
        private void DrawString(string drawString, float x, float y, int font_size)
        {
            System.Drawing.Graphics formGraphics = pictureBox1.CreateGraphics();
            System.Drawing.Font drawFont = new System.Drawing.Font("Arial", font_size);
            System.Drawing.SolidBrush drawBrush = new
            System.Drawing.SolidBrush(System.Drawing.Color.Black);
            formGraphics.Clear(pictureBox1.BackColor);
            formGraphics.DrawString(drawString, drawFont, drawBrush, x, y);
            drawFont.Dispose();
            drawBrush.Dispose();
            formGraphics.Dispose();
        }
        //-------------------------------
        public Form1()
        {
            InitializeComponent();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Хотите выйти из программы?", "Завершение", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.Exit();
        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            DrawString("Лабораторная работа Кошеверова А.С.", 0, 0, 14);
        }

        private void инициализацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;                             //остановим таймер, на всякий пожарный
            progressBar1.Value = progressBar1.Minimum;          //и обнулим прогрес бар
            init = true;                                        //инициализация прошла успешно!
            mnof = 0;                                             //для пересчета количества кадров
            System.Drawing.Graphics g = this.CreateGraphics();  //
            g.Clear(this.BackColor);                            //Очистка формы
            g.Dispose();                                        //
            Random r = new Random();
            int num = r.Next(MIN_STARS,MAX_STARS+1);
            S = new Star[num];                                  //выделим память под звёзды
            for (int i = 0; i < num; i++)
            {
                double maxsize, size;
                point center = new point();
                Color c;
                do
                {
                    center.x = (this.Size.Width - 2 * MIN_SIZE + 1) * r.NextDouble() + MIN_SIZE;
                    center.y = (this.Size.Height - this.pictureBox1.Size.Height - 3 * MIN_SIZE + 1 - this.menuStrip1.Size.Height) * r.NextDouble() + this.menuStrip1.Size.Height + MIN_SIZE;
                    maxsize = Math.Min(Math.Min(center.x, this.Size.Width - center.x), Math.Min(center.y - this.menuStrip1.Size.Height, this.Size.Height - this.pictureBox1.Size.Height - center.y));
                }
                while (maxsize < MIN_SIZE);
                size = (maxsize-MIN_SIZE) * r.NextDouble() + MIN_SIZE;
                c = Color.FromArgb(r.Next(150), r.Next(150), r.Next(150));
                S[i] = new Star(this, center, size, c);
                mnof = mnof < S[i].GetCycles() ? S[i].GetCycles() : mnof;
            }
            DrawString(string.Format("Создано {0:00} объектов\nАнимация осуществляется за {1:#00} циклов",num,mnof), 0, 0, 14);
            progressBar1.Value = progressBar1.Minimum;
            progressBar1.Maximum = mnof;
        }

        private void запускToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!init || timer1.Enabled) инициализацияToolStripMenuItem_Click(sender, e);
            timer1.Enabled = true;
        }

        private void остановкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            init = true;
            timer1.Enabled = false;
        }

        private void увеличениеускорениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!mode)
            {
                увеличениеускорениеToolStripMenuItem.Text = "Уменьшение";
                for (int i = 0; i < S.Length; i++) S[i].SetAT(2);
                mode = !mode;
            }
            else
            {
                увеличениеускорениеToolStripMenuItem.Text = "Увеличение";
                for (int i = 0; i < S.Length; i++) S[i].SetAT(1);
                mode = !mode;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value == mnof)
            {
                timer1.Enabled = false;
                init = !init;
                return;
            }
            else
            {
                System.Drawing.Graphics g = this.CreateGraphics();
                g.Clear(this.BackColor);
                g.Dispose();
                progressBar1.Value++;
                for (int i = 0; i < S.Length; i++)
                    S[i].frame(this);
            }
        }

        private void ускорениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!mode2)
            {
                timer1.Interval /= 4;
                ускорениеToolStripMenuItem.Text = "Замедление";
                mode2 = !mode2;
            }
            else
            {
                ускорениеToolStripMenuItem.Text = "Ускорение";
                timer1.Interval *= 4;
                mode2 = !mode2;
            }
        }
    }
}
