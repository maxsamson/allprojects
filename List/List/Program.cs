﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List
{
    class Program
    {
        static void Main(string[] args)
        {
            List list = new List();
            int a = 1;
            int b = 2;
            int c = 3;
            list.Add(a);
            list.Add(b);
            list.Add(c);
            list.Show();
            Console.WriteLine("______________");
            list.Insert(7, 2);
            list.Show();
            Console.WriteLine("______________");
            list.Remove();
            list.Show();
            Console.WriteLine("______________");
            list.Remove();
            list.Show();
            Console.WriteLine("______________");
            list.Remove();
            list.Show();
            Console.WriteLine("______________");
            list.Remove();
            list.Show();
            Console.WriteLine("______________");
            list.Insert(3, 3);
            list.Show();
            Console.WriteLine("______________");
        }
    }

    class NodeList
    {
        public int data;
        public NodeList Next;

        public NodeList (int data)
        {
            this.data = data;
            this.Next = null;
        }
    }

    class List
    {
        public NodeList Head;
        private int count;
        public int Count
        {
            get { return count; } 
        }

        public List()
        {
            this.Head = null;
            this.count = 0;
        }

        public List(int data):this()
        {
            Add(data);
        }

        public void Insert(int data, int pos)
        {
            int ans;
            if (this.Head == null)
            {
                Console.WriteLine("Список пуст, желаете вставить элемент в начало списка? Yes=1, No=0.");
                ans = Convert.ToInt32(Console.ReadLine());
                if (ans == 1)
                {
                    Add(data);
                }
                return;
            }
            int i = 1;
            NodeList temp = this.Head;
            while(i < pos)
            {
                if (temp.Next == null)
                {
                    Console.WriteLine("В списке {0} элементов, желаете вставить элемент на след. позицию? Yes=1, No=0.", i);
                    ans = Convert.ToInt32(Console.ReadLine());
                    if (ans == 1)
                    {
                        Add(data);
                    }
                    return;
                }
                if (i != pos - 1)
                    temp = temp.Next;
                i++;
            }
            NodeList temp2 = temp.Next;
            temp.Next = new NodeList(data);
            temp = temp.Next;
            temp.Next = temp2;
        }

        public void Add(int data)
        {
            count++;
            if (this.Head == null)
            {
                this.Head = new NodeList(data);
                return;
            }
            NodeList temp = this.Head;
            while(temp.Next != null)
            {
                temp = temp.Next;
            }
            temp.Next = new NodeList(data);
        }

        public void Remove()
        {
            if (this.Head == null)
            {
                Console.WriteLine("Пустой список, нечего удалять!");
                return;
            }
            count--;
            if (count == 0)
            {
                this.Head = null;
                return;
            }
            NodeList temp = this.Head;
            int i = 1;
            while (i++ < count)
            {
                temp = temp.Next;
            }
            temp.Next = null;
        }

        public void Show()
        {
            NodeList temp = this.Head;
            if (temp == null)
            {
                Console.WriteLine("Пусто!");
                return;
            }
            while (temp != null)
            {
                Console.WriteLine(temp.data);
                temp = temp.Next;
            }
        }
    }
}
