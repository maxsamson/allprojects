﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_TwoDimension
{
    class Program
    {
        static void InitArray(int[,] a)
        {
            Random r = new Random();
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    a[i,j] = r.Next(0, 1000);
                }
            }
        }
        static void ShowArray(int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    Console.Write(a[i,j] + "\t");
                }
                Console.WriteLine("\n");
            }
        }

        static void Main(string[] args)
        {
            int[,] array = new int[5, 5];
            InitArray(array);
            ShowArray(array);
            Console.ReadLine();
        }
    }
}
