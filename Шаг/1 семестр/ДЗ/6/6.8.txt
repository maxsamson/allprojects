#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <string>
#include <locale.h>

using namespace std;

void Perevod(int x, int sis);

void main()
{
	setlocale(LC_ALL,"RUS");
	int sis, x;
	cout << "������� �������: ";
	cin >> sis;
	cout << endl;
	cout << "������� �����: ";
	cin >> x;
	cout << endl;
	Perevod(x, sis);
	cout << endl;
	system("pause");
}

void Perevod(int x, int sis)
{
	int ost = x%sis;
	x /= sis;
	if (x != 0)
		Perevod(x, sis);
	if (ost == 10)
		cout << "A";
	else if (ost == 11)
		cout << "B";
	else if (ost == 12)
		cout << "C";
	else if (ost == 13)
		cout << "D";
	else if (ost == 14)
		cout << "E";
	else if (ost == 15)
		cout << "F";
	else
		cout << ost;
}