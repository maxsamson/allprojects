﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_ConstraintNew
{
    class Customer
    {
        public string Name { get; set; }

        public Customer()
        {

        }

        public Customer(string name)
        {
            Name = name;
        }

        public void Display()
        {
            Console.WriteLine(Name);
        }
    }

    class CustomerList<T> where T : new()
    {
        List<T> phList;
        public CustomerList()
        {
            phList = new List<T>();
        }

        public bool Add(T newEntry)
        {
            phList.Add(newEntry);
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            CustomerList<Customer> customer = new CustomerList<Customer>();
        }
    }
}
