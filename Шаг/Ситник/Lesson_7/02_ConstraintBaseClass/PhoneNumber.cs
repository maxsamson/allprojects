﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ConstraintBaseClass
{
    class PhoneNumber
    {
        public PhoneNumber(string n, string num)
        {
            Name = n;
            Number = num;
        }
        public string Number { get; set; }
        public string Name { get; set; }
    }
}
