﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_HrCodeFirst
{
    public class Employee
    {
        public Employee()
        {
            EmpPromotions = new List<EmpPromotion>();
        }

        public int EmployeeId { get; set; }
        [MaxLength(32)]
        public string FirstName { get; set; }
        [MaxLength(32)]
        public string LastName { get; set; }
        public System.DateTime DateBirthday { get; set; }
        [MaxLength(12)]
        public string INN { get; set; }

        public virtual ICollection<EmpPromotion> EmpPromotions { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1:d} {2} {3}", EmployeeId, DateBirthday, FirstName, LastName);
        }
    }
    public class EmpPromotion
    {
        public int EmpPromotionId { get; set; }
        public int EmployeeId { get; set; }
        public int JobTitleId { get; set; }
        public System.DateTime HireDate { get; set; }
        public decimal Salary { get; set; }


        public virtual Employee Employee { get; set; }
    }
    public class JobTitle
    {
        public int JobTitleId { get; set; }
        [MaxLength(32)]
        public string NameJobTitle { get; set; }
    }
}
