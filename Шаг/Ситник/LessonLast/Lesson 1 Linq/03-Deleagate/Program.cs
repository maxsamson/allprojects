﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Deleagate
{
    public delegate bool IntFilter(int i);
    public class Common
    {
        public static int[] FilterArrayOfInts(int[] ints, IntFilter filter)
        {
            ArrayList aList = new ArrayList();
            foreach (int i in ints)
            {
                if (filter(i))
                {
                    aList.Add(i);
                }
            }
            return ((int[])aList.ToArray(typeof(int)));
        }
    }

    class Program
    {
        static void InitArray(int[] arr)
        {
            Random r = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = r.Next(-50, 50);
            }
        }
        static void Display(int[] arr, string s)
        {
            Console.WriteLine(s);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]+ " ");
            }
            Console.WriteLine();
        }

        //static bool LessZero(int a)
        //{
        //    return a < 0;
        //}
        //static bool Even(int a)
        //{
        //    return a % 2 == 0;
        //}
        
        static void Main(string[] args)
        {
            //Array
            int[] arr = new int[15];
            InitArray(arr);
            Display(arr, "Исходный массив");
            int[] arrLessZero = Common.FilterArrayOfInts(arr, delegate(int a) { return a < 0; });
            Display(arrLessZero, "Массив чисел меньше нуля");


            int[] arrEven = Common.FilterArrayOfInts(arr, a => { return a % 2 == 0; });
            Display(arrEven, "Массив четных чисел");


            int[] arrOddMoreZero = Common.FilterArrayOfInts(arr, a => a>0 && !(a % 2 == 0) );
            Display(arrOddMoreZero, "Массив нечетных чисел больше нуля");
            //() => Console.WriteLine(str);
        }
    }
}
