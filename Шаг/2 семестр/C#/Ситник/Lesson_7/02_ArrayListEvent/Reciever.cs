﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ArrayListEvent
{
    class EventReceiver1
    {
        public void RegisterEvent(ArrayListEvents list)
        {
            list.Changed += ListChanged;
        }
        //Обработчик события - выдает сообщение.
        //Recevier1 Разрешает добавление элементов, меньших 10. 
        private void ListChanged(object sender, ChangedEventArgs args)
        {
            Console.WriteLine("EventReceiver1: Сообщаю об изменениях:" + "Item ={0}", args.Item);
            args.Permit = ((int)args.Item < 10);
        }

        public void UnRegisterEvent(ArrayListEvents list)
        {
            //Отсоединяет обработчик от события и удаляет список 
            //List.Changed -= new ChangedEventHandler(ListChanged);
            list.Changed -= ListChanged;
        }
    }

    class EventReceiver2
    {
        public void RegisterEvent(ArrayListEvents list)
        {
            list.Changed += ListChanged;
        }
        //Обработчик события - выдает сообщение.
        //Recevier1 Разрешает добавление элементов, меньших 10. 
        private void ListChanged(object sender, ChangedEventArgs args)
        {
            Console.WriteLine("EventReceiver1: Сообщаю об изменениях:" + "Item ={0}", args.Item);
            args.Permit = ((int)args.Item < 10 & (int)args.Item % 3 == 0);
        }

        public void UnRegisterEvent(ArrayListEvents list)
        {
            //Отсоединяет обработчик от события и удаляет список 
            //List.Changed -= new ChangedEventHandler(ListChanged);
            list.Changed -= ListChanged;
        }
    }
}
