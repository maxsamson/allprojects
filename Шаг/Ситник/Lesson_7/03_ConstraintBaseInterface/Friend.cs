﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ConstraintBaseInterface
{
    class Friend : IPhoneNumber, IShow
    {
        public Friend(string name, string number)
        {
            Name = name;
            Number = number;
        }

        public string Name { get; set; }
        public string Number { get; set; }

        public void Display()
        {
            Console.WriteLine("Name - {0}, Number - {1}", Name, Number);
        }
    }

    class Customer : IPhoneNumber
    {
        public Customer(string name, string number)
        {
            Name = name;
            Number = number;
        }

        public string Name { get; set; }
        public string Number { get; set; }

        public void Display()
        {
            Console.WriteLine("Name - {0}, Number - {1}", Name, Number);
        }
    }
}
