﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

using System.ComponentModel.DataAnnotations;

namespace _04_StudentCodeFirst
{
    class Student
    {
        [Key]
        public int StudentId { get; set; }


        [MaxLength(32, ErrorMessage ="Length max =32")]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(32, ErrorMessage = "Length max =32")]
        public string LastName { get; set; }

        public DateTime? DateBirthday { get; set; }
    }

    class StudentContext : DbContext
    {
        public StudentContext() : base("name=StudentTest")
        {
            Database.SetInitializer<StudentContext>(new DropCreateDatabaseAlways<StudentContext>());
        }

        public DbSet<Student> Students { get; set; }
    }


    class Program
    {
        static void Main(string[] args)
        {
            StudentContext context = new StudentContext();


            Student st1 = new Student() { StudentId = 1, FirstName = "Vasja", LastName = "Dudkin", DateBirthday = new DateTime(1995, 12, 31) };

            Student st2 = new Student() { StudentId = 2, FirstName = "Vasja", LastName = "Pupkin556575", DateBirthday = new DateTime(1998, 12, 31) };

            context.Students.AddOrUpdate(st1);
            context.Students.AddOrUpdate(st2);

            context.SaveChanges();


        }
    }
}
