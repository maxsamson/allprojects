﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Properties
{
    class Singleton
    {
        private static Singleton singleton;
        static Singleton()
        {
            Console.WriteLine("Work static constructor");
        }
        private Singleton()
        {

        }
        public static Singleton CreateInstance()
        {
            if (singleton == null)
            {
                lock (typeof(Singleton))
                {
                    if (singleton == null)
                        singleton = new Singleton();
                }
            }
            return singleton; 
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Singleton s = Singleton.CreateInstance();
            Console.ReadLine();
        }
    }
}
