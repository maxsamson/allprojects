#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <locale.h>
using namespace std;

class Stack  // ������ �� ������ (FILO)
{
	char mas[50];
	int top;

public:
	Stack()
	{
		top = -1;
	}

	void push(int data)
	{
		if (top < 19)
		{
			mas[++top] = data;
		}
	}

	int getTop()
	{
		return mas[top];
	}

	void pop()
	{
		if (top != -1)
		{
			top--;
		}
	}

	void clear()
	{
		top = -1;
	}

	bool isempty()
	{
		if (top != -1)
			return 1;
		return 0;
	}
};

void main()
{
	setlocale(LC_ALL, "RUS");
	char A[50];
	cout << "������� ���������: ";
	gets(A);
	cout << endl;
	Stack s;
	int k = 0;
	for (int i = 0; A[i] != '\0'; i++)
	{
		if ((A[i] == '(') || (A[i] == '[') || (A[i] == '{') || (A[i] == '<'))
			s.push(A[i]);
		if (A[i] == ')')
		{
			int n = s.getTop();
			if (n == '(')
				s.pop();
			else
			{
				k = 1;
				cout << "��������� ������������.";
			}
		}
		else if (A[i] == ']')
		{
			int n = s.getTop();
			if (n == '[')
				s.pop();
			else
			{
				k = 1;
				cout << "��������� ������������.";
			}
		}
		else if (A[i] == '}')
		{
			int n = s.getTop();
			if (n == '{')
				s.pop();
			else
			{
				k = 1;
				cout << "��������� ������������.";
			}
		}
		else if (A[i] == '>')
		{
			int n = s.getTop();
			if (n == '<')
				s.pop();
			else
			{
				k = 1;
				cout << "��������� ������������.";
			}
		}
		if (k == 1)
			break;
	}
	if (s.isempty())
		cout << "��������� ������������.";
	else if (k == 0)
		cout << "��������� ���������!";
	cout << endl;
	system("pause");
}