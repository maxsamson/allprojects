#include <iostream>
#include <locale.h>
#include <math.h>
#include <conio.h>
#include <time.h>
#include <windows.h>

using namespace std;

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	const int size = 8;
	char A[size], B[size];
	for (int i = 0; i < size; i+=2){
		A[i] = '#';
		A[i + 1] = ' ';
	}
	for (int i = 0; i < size; i+=2){
		B[i] = ' ';
		B[i + 1] = '#';
	}
	for (int i = 0; i < size / 2; i++){
		for (int i = 0; i < size; i++){
			cout << A[i];
		}
		cout << endl;
		for (int i = 0; i < size; i++){
			cout << B[i];
		}
		cout << endl;
	}
	system("pause");
}