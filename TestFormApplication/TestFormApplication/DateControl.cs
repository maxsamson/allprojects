﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestFormApplication
{
    internal class DateControl
    {
        private Label _lbDay;
        private ComboBox _cbDay;
        private Label _lbMonth;
        private ComboBox _cbMonth;
        private Label _lbYear;
        private ComboBox _cbYear;

        public DateControl()
        {
            _lbDay = new Label();
            _cbDay = new ComboBox();
            _lbMonth = new Label();
            _cbMonth = new ComboBox();
            _lbYear = new Label();
            _cbYear = new ComboBox();
        }

        public void FillDay()
        {
            _lbDay.Text = "День:";
            for (var i = 1; i <= 31; i++)
            {
                _cbDay.Items.Add(i);
            }
            _cbDay.SelectedIndex = DateTime.Now.Day - 1;
        }

        public void FillMonth()
        {
            _lbMonth.Text = "Месяц:";
            _cbMonth.Items.AddRange(DateTimeFormatInfo.CurrentInfo.MonthNames);
            _cbMonth.Items.RemoveAt(_cbMonth.Items.Count - 1);
            _cbMonth.SelectedIndex = DateTime.Now.Month - 1;
        }

        public void FillYear()
        {
            _lbYear.Text = "Год:";
            for (var i = 0; i < 20; i++)
            {
                _cbYear.Items.Add(DateTime.Now.Year - i);
            }
            _cbYear.SelectedIndex = 0;
        }

        public void SetBounds()
        {
            _lbDay.SetBounds(32, 26, 124, 13);
            _cbDay.SetBounds(35, 42, 40, 21);

            _lbMonth.SetBounds(92, 26, 124, 13);
            _cbMonth.SetBounds(95, 42, 90, 21);

            _lbYear.SetBounds(202, 26, 124, 13);
            _cbYear.SetBounds(205, 42, 50, 21);
        }
    }
}