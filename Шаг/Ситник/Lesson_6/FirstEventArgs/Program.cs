﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEventArgs
{
    class FirstEventArgs
    {
        static void Main(string[] args)
        {
            ClassSender sender = new ClassSender();
            sender.SomeEvent += sender.ShowEvent;

            Receiver1 r1 = new Receiver1();
            sender.SomeEvent += r1.Message;

            Console.WriteLine("\n----------------------------\n");

            ClassSender sender2 = new ClassSender();
            sender2.SomeEvent += sender2.ShowEvent;

            sender2.SomeEvent += r1.Message;

            sender.SomeEvent = null;

            //sender2.SomeEvent -= sender2.ShowEvent;
            //sender.SomeEvent -= r1.Message;

            int sum = sender.Counter();
            int sum1 = sender2.Counter();
        }
    }
}
