#include "Functions.h"


using namespace std;

GOODS* AddStruct(GOODS* Obj, const int amount) // ������� ���������� ����� � �������
{
	if (amount == 0)
	{
		Obj = new GOODS[amount + 1]; // ��������� ������ ��� ������ ���������
	}
	else
	{
		GOODS* tempObj = new GOODS[amount + 1];

		for (int i = 0; i < amount; i++)
		{
			tempObj[i] = Obj[i]; // �������� �� ��������� ������
		}
		delete[] Obj;

		Obj = tempObj;
	}
	return Obj;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setData(GOODS* Obj, const int amount) // ������� ���������� ������ ���������� ���������
{
	cout << "����� ������: ";
	cin.getline(Obj[amount].name, 32);
	cout << "������� ������: ";
	cin >> Obj[amount].cost;
	cout << "ʳ������ ������: ";
	cin >> Obj[amount].count;
	cout << "������ ����������:" << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << "��������� #" << i + 1 << ": ";
		cin >> Obj[amount].suppliers[i];
	}
	cin.get();
	cout << endl;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void showData(const GOODS* Obj, const int amount) // ������� ����������� �������
{
	system("cls");
	for (int i = 0; i < amount; i++)
	{
		cout << "����� �" << i + 1 << ":" << endl;
		cout << "\t�����: " << Obj[i].name << endl <<
			"\t�������: " << Obj[i].cost << endl <<
			"\tʳ������: " << Obj[i].count << endl <<
			"\t����������: ";
		for (int j = 0; j < 3; j++)
		{
			cout << Obj[i].suppliers[j];
			if (j < 2)
				cout << "; ";
			else
				cout << ".";
		}
		cout << endl;
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void sortData(GOODS* Obj, const int amount) // ������� ���������� ������� �� ��������� ���������
{
	GOODS tmp;
	bool exit = false;
	while (!exit)
	{
		exit = true;
		for (int i = 0; i < amount - 1; i++)
		{
			if (strcmp(Obj[i].name, Obj[i + 1].name) > 0)
			{
				tmp = Obj[i];
				Obj[i] = Obj[i + 1];
				Obj[i + 1] = tmp;
				exit = false;
			}
		}
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void searchData(const GOODS* Obj, const int amount) // ������� ������ �������� �� �����
{
	system("cls");
	cout << "������ ����� ������ ��� ������: ";
	char name[80];
	cin >> name;
	bool b = 0;
	for (int i = 0; i < amount; i++)
	{
		if (strcmp(Obj[i].name, name) == 0)
		{
			cout << endl << "����������: ";
			for (int j = 0; j < 3; j++)
			{
				cout << Obj[i].suppliers[j];
				if (j < 2)
					cout << "; ";
				else
					cout << ".";
			}
			cout << endl << "ʳ������ ������: " << Obj[i].count;
			b = 1;
		}
		if (b == 1)
			break;
	}
	if (b == 0)
		cout << "������ � ������ ��������� �� �������!" << endl;
}