﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
namespace _01_LeftJoin
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> emps = DataManager.GetEmployee();
            List<EmpPromotion> proms = DataManager.GetEmpPromotion();


            var query = from e in emps
                        join p in proms on e.EmployeeId equals p.EmployeeId
                        select new { e.EmployeeId, e.FirstName, p.HireDate, p.Salary };


            foreach (var item in query)
            {
                Console.WriteLine("{0}, {1:d} {2} {3}", item.EmployeeId, item.HireDate, item.FirstName, item.Salary);
            }
            //LEFT JOIN
            var queryLeftJoin = from e in emps
                        join p in proms on e.EmployeeId equals p.EmployeeId into grp
                        from g in grp.DefaultIfEmpty(new EmpPromotion() { })
                        select new { e.EmployeeId, e.FirstName, g.HireDate, g.Salary };
            Console.WriteLine("================Left Join");
            foreach (var item in queryLeftJoin)
            {
                Console.WriteLine("{0}, {1:d} {2} {3}", item.EmployeeId,   item.HireDate, item.FirstName, item.Salary);
            }

        }
    }
}
