﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_GenericInterface
{
    class Program
    {
        public interface ISeries<T> where T : struct
        {
            T GetNext();// возвратить следующее по порядку число
            void Reset(); // генерировать ряд последовательных чисел с самого начала 
            void SetStart(T v); // задать начальное значение
        }

        class ByTwos<T> : ISeries<T> where T : struct
        {
            T start;
            T val;

            // Этот делегат определяет форму метода, вызываемого для генерирования
            // очередного элемента в ряду последовательных значений. 
            public delegate T IncByTwo(T v);
            // Этой ссылке на делегат будет присвоен метод,
            // передаваемый конструктору класса ByTwos. 
            IncByTwo incr;
            public ByTwos(IncByTwo incrMeth)
            {
                start = default(T);
                val = default(T);
                incr = incrMeth;
            }

            public T GetNext()
            {
                val = incr(val);
                return val;
            }

            public void Reset()
            {
                val = start;
            }

            public void SetStart(T v)
            {
                start = v;
                val = start;
            }
        }

        static void Main(string[] args)
        {
            ByTwos<int> bytwo1 = new ByTwos<int>(p => p + 21);

        }
    }
}
