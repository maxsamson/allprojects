﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WpfApplication5
{
    
    public partial class MainWindow : Window
    {
        BitmapImage bmpImgKrest;
        BitmapImage bmpImgNol;
        Boolean XMovement = true;

        CellState[] cellStates = new CellState[9]
            {
                CellState.NotSelected, CellState.NotSelected, CellState.NotSelected,
                CellState.NotSelected, CellState.NotSelected, CellState.NotSelected,
                CellState.NotSelected, CellState.NotSelected, CellState.NotSelected
            };

        public MainWindow()
        {
            InitializeComponent();
            Uri uriImgNol = new Uri(@"pack://application:,,,/Resources/Nol.png", UriKind.Absolute);
            Uri uriImgKrest = new Uri(@"pack://application:,,,/Resources/Krest.png", UriKind.Absolute);
            bmpImgNol = GetBitmapImage(uriImgNol);
            bmpImgKrest = GetBitmapImage(uriImgKrest);

            for (int i = 0; i < LayoutRoot.Children.Count; i++)
            {
                if (LayoutRoot.Children[i] is Button)
                {
                    Button btn = (Button)LayoutRoot.Children[i];
                    btn.Click += new RoutedEventHandler(Button_Click);
                }
            }
        }

        BitmapImage GetBitmapImage(Uri uri)
        {
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = uri;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Image img = (Image)btn.Content;
            int btnNumber = Convert.ToInt32((string)btn.Tag);

            if (XMovement && cellStates[btnNumber] == CellState.NotSelected)
            {
                img.Source = bmpImgKrest;
                cellStates[btnNumber] = CellState.X;
                XMovement = false;

            }
            else if (cellStates[btnNumber] == CellState.NotSelected)
            {
                img.Source = bmpImgNol;
                cellStates[btnNumber] = CellState.O;
                XMovement = true;
            }

            switch (GetGameResult())
            {
                case GameResult.CrossWin:
                    MessageBox.Show(this, "Выйграли крестики");
                    ResetGame();
                    break;
                case GameResult.ZeroWin:
                    MessageBox.Show(this, "Выйграли нолики");
                    ResetGame();
                    break;
                case GameResult.Draw: 
                    MessageBox.Show(this, "Ничья");
                    ResetGame();
                    break;
                case GameResult.ContinueToPlay:
                    break;
            }
        }

        private void ResetGame()
        {
            for (int i = 0; i < cellStates.Length; i++)
                cellStates[i] = CellState.NotSelected;
            for (int i = 0; i < LayoutRoot.Children.Count; i++)
            {
                if (LayoutRoot.Children[i] is Button)
                {
                    Button btn = (Button)LayoutRoot.Children[i];
                    ((Image)btn.Content).Source = null;
                }
            }
        }

        private GameResult GetGameResult()
        {
            if (cellStates.All<CellState>(c => c != CellState.NotSelected)) //Проверить каждый элемент массива cellStates не равен ли он CellState.NotSelected. Если все неравны, то ничья.
                return GameResult.Draw;
            if (CheckCellState(CellState.O))
                return GameResult.ZeroWin;
            if (CheckCellState(CellState.X))
                return GameResult.CrossWin;
            return GameResult.ContinueToPlay;
        }

        private Boolean CheckCellState(CellState cellState)
        {
            if (cellStates[0] == cellState && cellStates[1] == cellState && cellStates[2] == cellState)
                return true;
            else
                if (cellStates[3] == cellState && cellStates[4] == cellState && cellStates[5] == cellState)
                    return true;
                else
                    if (cellStates[6] == cellState && cellStates[7] == cellState && cellStates[8] == cellState)
                        return true;
                    else
                        if (cellStates[0] == cellState && cellStates[3] == cellState && cellStates[6] == cellState)
                            return true;
                        else
                            if (cellStates[1] == cellState && cellStates[4] == cellState && cellStates[7] == cellState)
                                return true;
                            else
                                if (cellStates[2] == cellState && cellStates[5] == cellState && cellStates[8] == cellState)
                                    return true;
                                else
                                    if (cellStates[0] == cellState && cellStates[4] == cellState && cellStates[8] == cellState)
                                        return true;
                                    else
                                        if (cellStates[2] == cellState && cellStates[4] == cellState && cellStates[6] == cellState)
                                            return true;
            return false;
        }
    }
}