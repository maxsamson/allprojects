#pragma once
#include <iostream>
#include <windows.h> 
#include <algorithm>
#include <fstream>

struct GOODS
{
	char name[80]; //����� ������
	float cost; //������� ������
	int count; //ʳ������ ������
	char suppliers[3][80]; //������ ���������� (����� � ����� ��������)
};

GOODS* AddStruct(GOODS* Obj, const int amount);

void setData(GOODS* &Obj, int &amount);

void showData(const GOODS* Obj, const int amount);

void sortData(GOODS* Obj, const int amount);

void searchData(const GOODS* Obj, const int amount);