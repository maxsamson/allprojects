﻿using System;

namespace ConsoleApplication1
{
    class Car
    {
        // Старые поля и методы...
        public string CarName { get; set; }
        public int CarSpeed { get; set; }
        public Car()
        {

        }
        public Car(string name)
            : this(name, 100)
        {
            CarName = name;
            //currSpeed = 10;
        }
        public Car(string name, int speed)
        {
            CarName = name; CarSpeed = speed;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("Audi 100");
            Car car1 = new Car() { CarName = "Merc", CarSpeed = 160 };
        }
    }
}
