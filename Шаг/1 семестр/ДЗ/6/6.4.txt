#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <string>
#include <locale.h>
#include <time.h>

using namespace std;

double Pow(double x, int N);

void main()
{
	setlocale(LC_ALL,"RUS");
	srand(time(NULL));
	double x;
	int N;
	cout << "������� �����: ";
	cin >> x;
	cout << endl << "������� �������: ";
	cin >> N;
	cout << endl;
	cout << Pow(x,N);
	cout << endl;
	system("pause");
}

double Pow(double x, int N)
{
	if (N >= 1)
		return x*Pow(x, N - 1);
	else
		return 1;
}