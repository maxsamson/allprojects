﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDA
{
    class Program
    {
        static int Feb(int n, int i, int j)
        {
            if (n == 0)
                return i;
            int k = j;
            j += i;
            i = k;
            return Feb(n - 1, i, j);
        }
        static void Main(string[] args)
        {
            int x = Feb(4, 0, 1);
            Console.WriteLine("Feb(4) = {0}", x);
        }
    }
}
