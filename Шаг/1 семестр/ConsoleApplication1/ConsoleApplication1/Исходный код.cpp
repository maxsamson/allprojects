#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

using namespace std;

enum Key{ M_UP = 72, M_LEFT = 75, M_RIGHT = 77, M_DOWN = 80, M_ENTER = 13 }; //���� ������ ����������
struct Student{ char f[30]; char i[20]; char o[30]; int age; float mark; };
struct Group{ Student *Students; int count; char name[10]; };
struct Teacher{ char f[30]; char i[20]; char o[30]; };
struct Academy{ Group *Groups; Teacher *Teachers; int countG; int countT; };
void StartMenu(struct Academy &a);
void MenuTeacher(struct Academy &a);
void MenuGroups(struct Academy &a);
void MenuStudents(struct Academy &a, int k);
void GoToXY(COORD i);
void FillGroup(struct Group &a);
void FillStudent(struct Student &a);
void FillTeacher(struct Teacher &a);
void FillAcademy(struct Academy &a);
void ShowGroupsAll(struct Academy &a);
void ShowGroup1(struct Group &a);
void ShowStudent(struct Student &a);
void ShowTeachers(struct Academy &a);
void AddGroup(struct Academy &a);
void AddStudent(struct Group &a);
void AddTeacher(struct Academy &a);
void DeleteGroup(struct Academy &a);
void DeleteStudent(struct Group &a);
void DeleteTeacher(struct Academy &a);
void TransferStudent(struct Academy &a, int k);
int CompareF(Student a, Student b);
int CompareAge(Student a, Student b);
int CompareQuantity(Group a, Group b);
int CompareMark(Group a, Group b);
template <typename T> void Sort(T* a, int count, int(*cmp)(T, T));

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	Academy *A = new Academy;
	StartMenu(*A);
	/*AddStudent(*X);
	for (int i = 0; i < X->count; i++)
	ShowStudent(X->Students[i]);
	int(*cmp)(Student, Student) = CompareF;
	Sort(X->Students, X->count, cmp);
	for (int i = 0; i < X->count; i++)
	ShowStudent(X->Students[i]);
	cout << endl;
	cmp = CompareAge;
	Sort(X->Students, X->count, cmp);
	for (int i = 0; i < X->count; i++)
	ShowStudent(X->Students[i]);
	cout << endl;*/
	/*DeleteStudent(*X);
	if (X->count == 0)
	cout << "� ������ ��� ���������." << endl;
	else
	for (int i = 0; i < X->count; i++)
	ShowStudent(X->Students[i]);*/
	system("pause");
}

void StartMenu(struct Academy &a)
{
	char c = '\0';
	char i = '<';
	cout << "��������� ��������" << endl;
	cout << "������������� (����)" << endl;
	cout << "������ (����)" << endl;
	cout << "�����" << endl;
	COORD Strelka;
	Strelka.X = 22;
	Strelka.Y = 0;
	GoToXY(Strelka);
	cout << i;
	for (;;)
	{
		if (_kbhit())
		{
			c = _getch();
			if (c == '\0')
				c = _getch();
		}
		if (c == M_DOWN)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 3)
				Strelka.Y = 0;
			else
				Strelka.Y++;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_UP)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 0)
				Strelka.Y = 3;
			else
				Strelka.Y--;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_ENTER)
		{
			if (Strelka.Y == 0)
			{
				system("cls");
				FillAcademy(a);
			}
			if (Strelka.Y == 1)
			{
				system("cls");
				MenuTeacher(a);
			}
			if (Strelka.Y == 2)
			{
				system("cls");
				MenuGroups(a);
			}
			if (Strelka.Y == 3)
				exit(0);
			cout << "��������� ��������" << endl;
			cout << "�������������" << endl;
			cout << "������" << endl;
			cout << "�����" << endl;
			GoToXY(Strelka);
			cout << i;
		}
		c = '\0';
	}
}

void MenuTeacher(struct Academy &a)
{
	char c = '\0';
	char i = '<';
	cout << "������� ���� ��������������" << endl;
	cout << "��������" << endl;
	cout << "�������" << endl;
	cout << "����� � ���� ��������" << endl;
	COORD Strelka;
	Strelka.X = 29;
	Strelka.Y = 0;
	GoToXY(Strelka);
	cout << i;
	for (;;)
	{
		if (_kbhit())
		{
			c = _getch();
			if (c == '\0')
				c = _getch();
		}
		if (c == M_DOWN)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 3)
				Strelka.Y = 0;
			else
				Strelka.Y++;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_UP)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 0)
				Strelka.Y = 3;
			else
				Strelka.Y--;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_ENTER)
		{
			if (Strelka.Y == 0)
			{
				system("cls");
				ShowTeachers(a);
			}
			if (Strelka.Y == 1)
			{
				system("cls");
				AddTeacher(a);
			}
			if (Strelka.Y == 2)
			{
				system("cls");
				DeleteTeacher(a);
			}
			if (Strelka.Y == 3)
			{
				system("cls");
				return;
			}
			system("cls");
			cout << "������� ����" << endl;
			cout << "��������" << endl;
			cout << "�������" << endl;
			cout << "����� � ���� ��������" << endl;
			GoToXY(Strelka);
			cout << i;
		}
		c = '\0';
	}
}

void MenuGroups(struct Academy &a)
{
	char c = '\0';
	char i = '<';
	cout << "������� ��� ������" << endl;
	cout << "�������� ������" << endl;
	cout << "������� ������" << endl;
	cout << "����������� ������ �� ���." << endl;
	cout << "����������� ������ �� ��.���." << endl;
	cout << "�������� (����)" << endl;
	cout << "����� � ���� ��������" << endl;
	COORD Strelka;
	Strelka.X = 31;
	Strelka.Y = 0;
	GoToXY(Strelka);
	cout << i;
	for (;;)
	{
		if (_kbhit())
		{
			c = _getch();
			if (c == '\0')
				c = _getch();
		}
		if (c == M_DOWN)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 6)
				Strelka.Y = 0;
			else
				Strelka.Y++;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_UP)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 0)
				Strelka.Y = 6;
			else
				Strelka.Y--;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_ENTER)
		{
			if (Strelka.Y == 0)
			{
				system("cls");
				ShowGroupsAll(a);
			}
			if (Strelka.Y == 1)
			{
				system("cls");
				AddGroup(a);
			}
			if (Strelka.Y == 2)
			{
				system("cls");
				DeleteGroup(a);
			}
			if (Strelka.Y == 3)
			{
				system("cls");
				int(*cmp)(Group, Group) = CompareQuantity;
				Sort(a.Groups, a.countG, (*cmp));
				cout << "���������� ��������� �������." << endl;
				system("pause");
			}
			if (Strelka.Y == 4)
			{
				system("cls");
				int(*cmp)(Group, Group) = CompareMark;
				Sort(a.Groups, a.countG, (*cmp));
				cout << "���������� ��������� �������." << endl;
				system("pause");
			}
			if (Strelka.Y == 5)
			{
				system("cls");
				char x[30];
				cout << "������� ����� ������: ";
				fflush(stdin);
				gets(x);
				bool count = 0;
				int k;
				for (int i = 0; i < a.countG; i++)
				{
					for (int j = 0; j < strlen(a.Groups[i].name); j++)
					{
						if (a.Groups[i].name[j] == x[j])
						{
							k = i;
							count = 0;
							continue;
						}
						else
						{
							count = 1;
							break;
						}
					}
					if (count == 0)
						break;
				}
				if (count == 1)
				{
					cout << "��� ����� ������." << endl;
					system("pause");
				}
				else
				{
					system("cls");
					MenuStudents(a, k);
				}
			}
			if (Strelka.Y == 6)
			{
				system("cls");
				return;
			}
			system("cls");
			cout << "������� ��� ������" << endl;
			cout << "�������� ������" << endl;
			cout << "������� ������" << endl;
			cout << "����������� ������ �� ���." << endl;
			cout << "����������� ������ �� ��.���." << endl;
			cout << "�������� (����)" << endl;
			cout << "����� � ���� ��������" << endl;
			GoToXY(Strelka);
			cout << i;
		}
		c = '\0';
	}
}

void MenuStudents(struct Academy &a, int k)
{
	char c = '\0';
	char i = '<';
	cout << "������� ���������" << endl;
	cout << "��������" << endl;
	cout << "�������" << endl;
	cout << "��������� ��������" << endl;
	cout << "����� � ���� �����" << endl;
	COORD Strelka;
	Strelka.X = 20;
	Strelka.Y = 0;
	GoToXY(Strelka);
	cout << i;
	for (;;)
	{
		if (_kbhit())
		{
			c = _getch();
			if (c == '\0')
				c = _getch();
		}
		if (c == M_DOWN)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 4)
				Strelka.Y = 0;
			else
				Strelka.Y++;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_UP)
		{
			GoToXY(Strelka);
			cout << " ";
			if (Strelka.Y == 0)
				Strelka.Y = 4;
			else
				Strelka.Y--;
			GoToXY(Strelka);
			cout << i;
		}
		if (c == M_ENTER)
		{
			if (Strelka.Y == 0)
			{
				system("cls");
				ShowGroup1(a.Groups[k]);
				system("pause");
			}
			if (Strelka.Y == 1)
			{
				system("cls");
				AddStudent(a.Groups[k]);
			}
			if (Strelka.Y == 2)
			{
				system("cls");
				DeleteStudent(a.Groups[k]);
			}
			if (Strelka.Y == 3)
			{
				system("cls");
				TransferStudent(a, k);
			}
			if (Strelka.Y == 4)
			{
				system("cls");
				return;
			}
			system("cls");
			cout << "������� ���������" << endl;
			cout << "��������" << endl;
			cout << "�������" << endl;
			cout << "��������� ��������" << endl;
			cout << "����� � ���� �����" << endl;
			GoToXY(Strelka);
			cout << i;
		}
		c = '\0';
	}
}

void GoToXY(COORD i)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsole, i);
}

void FillAcademy(struct Academy &a)
{
	cout << "������� ���������� �����: ";
	cin >> a.countG;
	system("cls");
	a.Groups = new Group[a.countG];
	for (int i = 0; i < a.countG; i++)
	{
		FillGroup(a.Groups[i]);
		system("cls");
	}
	cout << "������� ���������� ��������������: ";
	cin >> a.countT;
	cout << endl;
	if (a.countT == 0)
		system("cls");
	for (int i = 0; i < a.countT; i++)
	{
		a.Teachers = new Teacher[a.countT];
		FillTeacher(a.Teachers[i]);
		system("cls");
	}
}

void FillGroup(struct Group &a)
{
	cout << "������� �������� ������: ";
	fflush(stdin);
	gets(a.name);
	int x;
	cout << "������� ���������� ���������: ";
	cin >> x;
	a.count = x;
	cout << endl;
	if (a.count == 0)
		return;
	else
	{
		a.Students = new Student[a.count];
		for (int i = 0; i < a.count; i++)
			FillStudent(a.Students[i]);
	}
}

void FillStudent(struct Student &a)
{
	cout << "������� ������� ��������: ";
	fflush(stdin);
	gets(a.f);
	cout << "������� ��� ��������: ";
	fflush(stdin);
	gets(a.i);
	cout << "������� �������� ��������: ";
	fflush(stdin);
	gets(a.o);
	cout << "������� ������� ��������: ";
	cin >> a.age;
	cout << "������� ������� ���� ��������: ";
	cin >> a.mark;
	cout << endl;
}

void FillTeacher(struct Teacher &a)
{
	cout << "������� ������� �������������: ";
	fflush(stdin);
	gets(a.f);
	cout << "������� ��� �������������: ";
	fflush(stdin);
	gets(a.i);
	cout << "������� �������� �������������: ";
	fflush(stdin);
	gets(a.o);
	cout << endl;
}

void ShowGroupsAll(struct Academy &a)
{
	for (int i = 0; i < a.countG; i++)
		ShowGroup1(a.Groups[i]);
	system("pause");
}

void ShowGroup1(struct Group &a)
{
	cout << "������ " << a.name << endl << endl;
	for (int i = 0; i < a.count; i++)
		cout << i + 1 << ". ��� ��������: " << a.Students[i].f << " " << a.Students[i].i << " " << a.Students[i].o << "; �������: " << a.Students[i].age << " ���; ������� ����: " << a.Students[i].mark << "." << endl;
	cout << endl;
}

void ShowStudent(struct Student &a)
{
	cout << "���: " << a.f << " " << a.i << " " << a.o << "; �������: " << a.age << "; ������� ����: " << a.mark << "." << endl;
	cout << endl;
	system("pause");
}

void ShowTeachers(struct Academy &a)
{
	for (int i = 0; i < a.countT; i++)
		cout << i + 1 << ". " << a.Teachers[i].f << " " << a.Teachers[i].i << " " << a.Teachers[i].o << endl;
	cout << endl;
	system("pause");
}

void AddGroup(struct Academy &a)
{
	int x;
	cout << "������� ���������� ����������� �����: ";
	cin >> x;
	cout << endl;
	Group* A = new Group[a.countG + x];
	for (int i = 0; i < a.countG; i++)
		A[i] = a.Groups[i];
	for (int i = a.countG; i < a.countG + x; i++)
		FillGroup(A[i]);
	delete[] a.Groups;
	a.Groups = A;
	a.countG += x;
	system("pause");
}

void AddStudent(struct Group &a)
{
	int x;
	cout << "������� ���������� ����������� ���������: ";
	cin >> x;
	cout << endl;
	Student* A = new Student[a.count + x];
	for (int i = 0; i < a.count; i++)
		A[i] = a.Students[i];
	for (int i = a.count; i < a.count + x; i++)
		FillStudent(A[i]);
	if (a.count != 0)
		delete[] a.Students;
	a.Students = A;
	a.count += x;
	system("pause");
}

void AddTeacher(struct Academy &a)
{
	int x;
	cout << "������� ���������� ����������� ��������������: ";
	cin >> x;
	cout << endl;
	Teacher* A = new Teacher[a.countT + x];
	for (int i = 0; i < a.countT; i++)
		A[i] = a.Teachers[i];
	for (int i = a.countT; i < a.countT + x; i++)
		FillTeacher(A[i]);
	delete[] a.Teachers;
	a.Teachers = A;
	a.countT += x;
	system("pause");
}

void DeleteGroup(struct Academy &a)
{
	char x[30];
	cout << "������� �������� ������ ��� ��������: ";
	fflush(stdin);
	gets(x);
	bool count = 0;
	int k;
	for (int i = 0; i < a.countG; i++)
	{
		for (int j = 0; j < strlen(a.Groups[i].name); j++)
		{
			if (a.Groups[i].name[j] == x[j])
			{
				k = i;
				continue;
			}
			else
			{
				count = 1;
				break;
			}
		}
		if (count == 0)
			break;
	}
	if (count == 1)
	{
		cout << "��� ����� ������." << endl;
	}
	else
	{
		a.countG--;
		Group* A = new Group[a.countG];
		for (int i = 0; i < k; i++)
			A[i] = a.Groups[i];
		for (int i = k + 1; i <= a.countG; i++)
			A[i - 1] = a.Groups[i];
		delete[] a.Groups;
		a.Groups = A;
		cout << "�������� ����������� ������." << endl;
	}
	system("pause");
}

void DeleteStudent(struct Group &a)
{
	char x[30];
	cout << "������� ������� �������� ��� ��������: ";
	fflush(stdin);
	gets(x);
	bool count = 0;
	int k;
	for (int i = 0; i < a.count; i++)
	{
		for (int j = 0; j < strlen(a.Students[i].f); j++)
		{
			if (a.Students[i].f[j] == x[j])
			{
				k = i;
				continue;
			}
			else
			{
				count = 1;
				break;
			}
		}
		if (count == 0)
			break;
	}
	if (count == 1)
	{
		cout << "��� ������ ��������." << endl;
	}
	else
	{
		a.count--;
		Student* A = new Student[a.count];
		for (int i = 0; i < k; i++)
			A[i] = a.Students[i];
		for (int i = k + 1; i <= a.count; i++)
			A[i - 1] = a.Students[i];
		delete[] a.Students;
		a.Students = A;
		cout << "�������� ����������� ������." << endl;
	}
	system("pause");
}

void DeleteTeacher(struct Academy &a)
{
	char x[30];
	cout << "������� ������� ������������� ��� ����������: ";
	fflush(stdin);
	gets(x);
	bool count = 0;
	int k;
	for (int i = 0; i < a.countT; i++)
	{
		for (int j = 0; j < strlen(a.Teachers[i].f); j++)
		{
			if (a.Teachers[i].f[j] == x[j])
			{
				k = i;
				continue;
			}
			else
			{
				count = 1;
				break;
			}
		}
		if (count == 0)
			break;
	}
	if (count == 1)
	{
		cout << "��� ������ �������������." << endl;
	}
	else
	{
		a.countT--;
		Teacher* A = new Teacher[a.countT];
		for (int i = 0; i < k; i++)
			A[i] = a.Teachers[i];
		for (int i = k + 1; i <= a.countT; i++)
			A[i - 1] = a.Teachers[i];
		delete[] a.Teachers;
		a.Teachers = A;
		cout << "���������� ����������� ������." << endl;
	}
	system("pause");
}

void TransferStudent(struct Academy &a, int k)
{
	char x[30];
	cout << "������� �������� ������, � ������� ����� ����������� �������: ";
	fflush(stdin);
	gets(x);
	bool count = 0;
	int l;
	for (int i = 0; i < a.countG; i++)
	{
		for (int j = 0; j < strlen(a.Groups[i].name); j++)
		{
			if (a.Groups[i].name[j] == x[j])
			{
				l = i;
				count = 0;
				continue;
			}
			else
			{
				count = 1;
				break;
			}
		}
		if (count == 0)
			break;
	}
	if (count == 1)
	{
		cout << "��� ����� ������." << endl;
	}
	else
	{
		char x[30];
		cout << "������� ������� �������� ��� ��������: ";
		fflush(stdin);
		gets(x);
		bool count = 0;
		int m;
		for (int i = 0; i < a.Groups[k].count; i++)
		{
			for (int j = 0; j < strlen(a.Groups[k].Students[i].f); j++)
			{
				if (a.Groups[k].Students[i].f[j] == x[j])
				{
					m = i;
					continue;
				}
				else
				{
					count = 1;
					break;
				}
			}
			if (count == 0)
				break;
		}
		if (count == 1)
		{
			cout << "��� ������ ��������." << endl;
		}
		Student* A = new Student[a.Groups[l].count + 1];
		for (int i = 0; i < a.Groups[l].count; i++)
			A[i] = a.Groups[l].Students[i];
		for (int i = a.Groups[l].count; i < a.Groups[l].count + 1; i++)
			A[i] = a.Groups[k].Students[m];
		if (a.Groups[l].count != 0)
			delete[] a.Groups[l].Students;
		a.Groups[l].Students = A;
		a.Groups[l].count++;
		a.Groups[k].count--;
		Student* B = new Student[a.Groups[k].count];
		for (int i = 0; i < m; i++)
			A[i] = a.Groups[k].Students[i];
		for (int i = m + 1; i <= a.Groups[k].count; i++)
			A[i - 1] = a.Groups[k].Students[i];
		delete[] a.Groups[k].Students;
		a.Groups[k].Students = B;
		cout << "������� ���������� ������." << endl;
		system("pause");
	}
}

int CompareF(Student a, Student b)
{
	return _stricmp(a.f, b.f);
}

int CompareAge(Student a, Student b)
{
	if (a.age > b.age)
		return 1;
	return 0;
}

int CompareQuantity(Group a, Group b)
{
	if (a.count > b.count)
		return 1;
	return 0;
}

int CompareMark(Group a, Group b)
{
	float x = 0, y = 0;
	for (int i = 0; i < a.count; i++)
		x = (x + a.Students[i].mark);
	x /= a.count;
	for (int i = 0; i < b.count; i++)
		y = (y + b.Students[i].mark);
	y /= b.count;
	if (x > y)
		return 1;
	return 0;
}

template <typename T> void Sort(T* a, int count, int(*cmp)(T, T))
{
	for (int i = 0; i < count; i++)
	{
		for (int j = i; j < count; j++)
		{
			if ((*cmp)(a[i], a[j]) > 0)
			{
				T X[30];
				X[0] = a[i];
				a[i] = a[j];
				a[j] = X[0];
			}
		}
	}
}