﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetEvent
{
    delegate void MyHandler(object sender, EventArgs e);

    class Program
    {
        //EventHandler handler;
        static void Main(string[] args)
        {
            ClassSender sender = new ClassSender();
            sender.MyEvent += sender.Display;

            int sum = sender.Counter();
        }
    }
}
