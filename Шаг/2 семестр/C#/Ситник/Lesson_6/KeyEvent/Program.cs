﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            KeyEvent kev = new KeyEvent();
            ConsoleKeyInfo keyInfo;
            int count = 0;
            kev.KeyPress += (sender, e) => Console.WriteLine("Click key {0}", e.cKey);
            kev.KeyPress += (sender, e) => count++;
            Console.WriteLine("Input string\n\n");
            do
            {
                keyInfo = Console.ReadKey(true);
                kev.OnKeyPress(keyInfo.KeyChar);
            } while (keyInfo.KeyChar != '.');

            Console.WriteLine("Count key press = {0}", count);
        }
    }
}
