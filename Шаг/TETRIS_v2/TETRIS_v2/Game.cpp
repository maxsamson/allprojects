#include "stdafx.h"
#include "Main.h"
#include "ctime"

///////////////////////////////////////////////
/// ����� "������"
///////////////////////////////////////////////

Cell::Cell()
{
	cell_x = cell_y = id = 0;
	status = FALSE;
}

Cell::Cell(int x, int y, int id)
{
	this->cell_x = x;
	this->cell_y = y;
	this->id = id;
	status = TRUE;
}

Cell::~Cell() {};

void Cell::Draw(HWND h)
{
	CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, cell_x*cell_width+20, cell_y*cell_width+20, cell_width, cell_width, h, HMENU(id), NULL, NULL);
}

///////////////////////////////////////////////
/// ����� "������"
///////////////////////////////////////////////
Shape::Shape()
{
	for (int i = 0; i < 4; i++)
		shape_cell[i] = nullptr;
}

void Shape::Shape_Draw(HWND h) 
{
	for (int i = 0; i < 4; i++)
		shape_cell[i]->Draw(h);
}

void Shape::Shape_destroy(HWND h)
{
	for (int i = 0; i < 4; i++)
			DestroyWindow(GetDlgItem(h, shape_cell[i]->get_id()));
}

void Line::New_Shape(int x, int y, Cell** arr) 
{
	set_rot(0);

	for (int i = 0; i < 4; i++)
	{
		shape_cell[i] = &arr[x + i][y];
		shape_cell[i]->set_status(1);
	}
}

bool Line::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
	shape_cell[i]->set_status(0);

	if ((shape_cell[3]->get_cell_y() + 1) < row && arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status()==FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE && arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE)
	{
		for (int i = 0; i < 4; i++)
		{
			shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
			shape_cell[i]->set_status(1);
		}
		Shape_Draw(h);
		return 1;
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
	return 0;
};

void Line::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[3]->get_cell_x() + 1) < col && arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE)
		for (int i = 0; i < 4; i++)
		shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	
	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void Line::Move_left(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[0]->get_cell_x() - 1) >= 0 && arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE)
		for (int i = 0; i < 4; i++)
		shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
		
	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void Line::Rotate(Cell** arr, HWND h)
{
	Shape_destroy(h);

	switch (get_rot())
	{
	case 0:
		if (shape_cell[1]->get_cell_x() + 2 < col && shape_cell[1]->get_cell_x() - 1 >= 0 && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 2].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0); 

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 2];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(1);
		}
		Shape_Draw(h);
		break;
	case 1:
		if (shape_cell[1]->get_cell_y() + 2 < row && shape_cell[1]->get_cell_y() - 1 >= 0 && arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 2][shape_cell[1]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y() + 2][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(0);
		}
		Shape_Draw(h);
		break;
	}
}

void RightL::New_Shape(int x, int y, Cell** arr)
{
	set_rot(0);
	
	shape_cell[0] = &arr[x][y];
	shape_cell[1] = &arr[x + 1][y];
	shape_cell[2] = &arr[x + 2][y];
	shape_cell[3] = &arr[x + 2][y + 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
}

bool RightL::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 1)
	{
		if ((shape_cell[1]->get_cell_y() + 1) < row)
			if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE)
			{
				for (int i = 0; i < 4; i++)
				{
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
					shape_cell[i]->set_status(1);
				}
				Shape_Draw(h);
				return 1;
			}
	}
	else
	{
		if ((shape_cell[1]->get_cell_y() + 2) < row)
			if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE && arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE)
			{
				for (int i = 0; i < 4; i++)
				{
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
					shape_cell[i]->set_status(1);
				}
				Shape_Draw(h);
				return 1;
			}
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	Shape_Draw(h);
	return 0;
}

void RightL::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 2)
	{
		if ((shape_cell[1]->get_cell_x() + 1) < col)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	}
	else
	{
		if ((shape_cell[1]->get_cell_x() + 2) < col)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
}

void RightL::Move_left(Cell** arr, HWND h)
	{	
		Shape_destroy(h);

		for (int i = 0; i < 4; i++)
			shape_cell[i]->set_status(0);
		
		if (get_rot() == 0)
		{
			if ((shape_cell[1]->get_cell_x() - 1) >= 0)
				if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE)
					for (int i = 0; i < 4; i++)
						shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
		}
		else
		{
			if ((shape_cell[1]->get_cell_x() - 2) >= 0)
				if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE)
					for (int i = 0; i < 4; i++)
						shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
		}

		for (int i = 0; i < 4; i++)
			shape_cell[i]->set_status(1);

		Shape_Draw(h);
}

void RightL::Rotate(Cell** arr, HWND h)
{
	Shape_destroy(h);

	switch (get_rot())
	{
	case 0:
		if (shape_cell[1]->get_cell_x() - 1 >= 0 && arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[3] = &arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1];
			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(1);
		}
		Shape_Draw(h);
		break;
	case 1:
		if (shape_cell[1]->get_cell_y() + 1 < row && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[0]->get_cell_y() - 1][shape_cell[0]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[3] = &arr[shape_cell[0]->get_cell_y() - 1][shape_cell[0]->get_cell_x()];
			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(2);
		}
		Shape_Draw(h);
		break;
	case 2:
		if (shape_cell[1]->get_cell_x() + 1 < col && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE &&arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[3] = &arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1];
			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(3);
		}
		Shape_Draw(h);
		break;
	case 3:
		if (shape_cell[1]->get_cell_y() - 1 >=0 && arr[shape_cell[1]->get_cell_y()-1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[3] = &arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()];
			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(0);
		}
		Shape_Draw(h);
		break;
	}
}

void LeftL::New_Shape(int x, int y, Cell** arr)
{
	set_rot(0);

	shape_cell[0] = &arr[x][y];
	shape_cell[1] = &arr[x + 1][y];
	shape_cell[2] = &arr[x + 2][y];
	shape_cell[3] = &arr[x + 2][y - 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
}

bool LeftL::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 3)
	{
		if ((shape_cell[1]->get_cell_y() + 1) < row)
			if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE)
			{
				for (int i = 0; i < 4; i++)
				{
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
					shape_cell[i]->set_status(1);
				}
				Shape_Draw(h);
				return 1;
			}
	}
	else
	{
		if ((shape_cell[1]->get_cell_y() + 2) < row)
			if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE && arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE)
			{
				for (int i = 0; i < 4; i++)
				{
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
					shape_cell[i]->set_status(1);
				}
				Shape_Draw(h);
				return 1;
			}
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
	return 0;
}

void LeftL::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 0)
	{
		if ((shape_cell[1]->get_cell_x() + 1) < col)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	}
	else
	{
		if ((shape_cell[1]->get_cell_x() + 2) < col)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
}

void LeftL::Move_left(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 2)
	{
		if ((shape_cell[1]->get_cell_x() - 1) >= 0)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
	}
	else
	{
		if ((shape_cell[1]->get_cell_x() - 2) >= 0)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
}

void LeftL::Rotate(Cell** arr, HWND h)
{
	Shape_destroy(h);

	switch (get_rot())
	{
	case 0:
		if (shape_cell[1]->get_cell_x() + 1 < col && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[3] = &arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(1);
		}
		Shape_Draw(h);
		break;
	case 1:
		if (shape_cell[1]->get_cell_y() - 1 >= 0 && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() - 1][shape_cell[2]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[2]->get_cell_y() - 1][shape_cell[2]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(2);
		}
		Shape_Draw(h);
		break;
	case 2:
		if (shape_cell[1]->get_cell_x() - 1 >= 0 && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE &&arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];
			shape_cell[3] = &arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(3);
		}
		Shape_Draw(h);
		break;
	case 3:
		if (shape_cell[1]->get_cell_y() + 1 < row && arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(0);
		}
		Shape_Draw(h);
		break;
	}
}

void Square::New_Shape(int x, int y, Cell **arr)
{
	set_rot(0);

	shape_cell[0] = &arr[x][y];
	shape_cell[1] = &arr[x + 1][y];
	shape_cell[2] = &arr[x + 1][y - 1];
	shape_cell[3] = &arr[x][y - 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
}

bool Square::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (shape_cell[1]->get_cell_y() + 1 < row && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE)
	{
		for (int i = 0; i < 4; i++)
		{
			shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
			shape_cell[i]->set_status(1);
		}
		Shape_Draw(h);
		return 1;
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
	return 0;
}

void Square::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (shape_cell[0]->get_cell_x() + 1 < col && arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE)
		for (int i = 0; i < 4; i++)
			shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void Square::Move_left(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (shape_cell[3]->get_cell_x() - 1 >= 0 && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE)
		for (int i = 0; i < 4; i++)
			shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
}

void Pyramide::New_Shape(int x, int y, Cell **arr)
{
	set_rot(0);

	shape_cell[0] = &arr[x][y];
	shape_cell[1] = &arr[x + 1][y];
	shape_cell[2] = &arr[x + 1][y - 1];
	shape_cell[3] = &arr[x + 1][y + 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
}

bool Pyramide::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 0)
	{
		if ((shape_cell[1]->get_cell_y() + 1) < row)
			if (arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE)
			{
				for (int i = 0; i < 4; i++)
				{
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
					shape_cell[i]->set_status(1);
				}
				Shape_Draw(h);
				return 1;
			}
	}
	else
	{
		if ((shape_cell[1]->get_cell_y() + 2) < row)
			if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE && arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE)
			{
				for (int i = 0; i < 4; i++)
				{
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
					shape_cell[i]->set_status(1);
				}
				Shape_Draw(h);
				return 1;
			}
	}
	
	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
	return 0;
}

void Pyramide::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 1)
	{
		if ((shape_cell[1]->get_cell_x() + 1) < col)
			if (arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	}
	else
	{
		if ((shape_cell[1]->get_cell_x() + 2) < col)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void Pyramide::Move_left(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if (get_rot() == 3)
	{
		if ((shape_cell[1]->get_cell_x() - 1) >= 0)
			if (arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
	}
	else
	{
		if ((shape_cell[1]->get_cell_x() - 2) >= 0)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];
	}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
}

void Pyramide::Rotate(Cell** arr, HWND h)
{
	Shape_destroy(h);

	switch (get_rot())
	{
	case 0:
		if (shape_cell[1]->get_cell_y() + 1 < row && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(1);
		}
		Shape_Draw(h);
		break;
	case 1:
		if (shape_cell[1]->get_cell_x() + 1 < col && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(2);
		}
		Shape_Draw(h);
		break;
	case 2:
		if (shape_cell[1]->get_cell_y() - 1 >= 0 && arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(3);
		}
		Shape_Draw(h);
		break;
	case 3:
		if (shape_cell[1]->get_cell_x() - 1 >= 0 && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(0);
		}
		Shape_Draw(h);
		break;
	}
}

void RightZ::New_Shape(int x, int y, Cell **arr)
{
	set_rot(0);

	shape_cell[0] = &arr[x][y];
	shape_cell[1] = &arr[x + 1][y];
	shape_cell[2] = &arr[x + 1][y - 1];
	shape_cell[3] = &arr[x + 2][y - 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
}

bool RightZ::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[1]->get_cell_y() + 2) < row)
		if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()].get_status() == FALSE && arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
			{
				shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
				shape_cell[i]->set_status(1);
			}
			Shape_Draw(h);
			return 1;
		}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
	return 0;	
}

void RightZ::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[2]->get_cell_x() + 2) < col)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void RightZ::Move_left(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

		if ((shape_cell[1]->get_cell_x() - 2) >= 0)
			if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE)
				for (int i = 0; i < 4; i++)
					shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
}

void RightZ::Rotate(Cell** arr, HWND h)
{
	Shape_destroy(h);

	switch (get_rot())
	{
	case 0:
		if (shape_cell[3]->get_cell_x() + 2 < col && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 2].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x() + 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(1);
		}
		Shape_Draw(h);
		break;
	case 1:
		if (shape_cell[1]->get_cell_y() - 1 >= 0 && arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[1]->get_cell_y() - 1][shape_cell[1]->get_cell_x()];
			shape_cell[2] = &arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1];
			shape_cell[3] = &arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x() - 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(0);
		}
		Shape_Draw(h);
		break;
	}
}

void LeftZ::New_Shape(int x, int y, Cell **arr)
{
	set_rot(0);

	shape_cell[0] = &arr[x][y];
	shape_cell[1] = &arr[x + 1][y];
	shape_cell[2] = &arr[x + 1][y + 1];
	shape_cell[3] = &arr[x + 2][y + 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
}

bool LeftZ::Move_down(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[2]->get_cell_y() + 2) < row)
		if (arr[shape_cell[0]->get_cell_y() + 1][shape_cell[0]->get_cell_x()].get_status() == FALSE && arr[shape_cell[1]->get_cell_y() + 1][shape_cell[1]->get_cell_x()].get_status() == FALSE && arr[shape_cell[3]->get_cell_y() + 1][shape_cell[3]->get_cell_x()].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
			{
				shape_cell[i] = &arr[shape_cell[i]->get_cell_y() + 1][shape_cell[i]->get_cell_x()];
				shape_cell[i]->set_status(1);
			}
			Shape_Draw(h);
			return 1;
		}

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);

	Shape_Draw(h);
	return 0;
}

void LeftZ::Move_right(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[1]->get_cell_x() + 2) < col)
		if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() + 1].get_status() == FALSE)
			for (int i = 0; i < 4; i++)
				shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() + 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void LeftZ::Move_left(Cell** arr, HWND h)
{
	Shape_destroy(h);

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(0);

	if ((shape_cell[2]->get_cell_x() - 2) >= 0)
		if (arr[shape_cell[0]->get_cell_y()][shape_cell[0]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[1]->get_cell_y()][shape_cell[1]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[3]->get_cell_y()][shape_cell[3]->get_cell_x() - 1].get_status() == FALSE)
			for (int i = 0; i < 4; i++)
				shape_cell[i] = &arr[shape_cell[i]->get_cell_y()][shape_cell[i]->get_cell_x() - 1];

	for (int i = 0; i < 4; i++)
		shape_cell[i]->set_status(1);
	
	Shape_Draw(h);
}

void LeftZ::Rotate(Cell** arr, HWND h)
{
	Shape_destroy(h);

	switch (get_rot())
	{
	case 0:
		if (shape_cell[2]->get_cell_x() + 1 < col && arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x() - 1];
			shape_cell[1] = &arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()];
			shape_cell[3] = &arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() + 1];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(1);
		}
		Shape_Draw(h);
		break;
	case 1:
		if (shape_cell[2]->get_cell_y() - 1 >= 0 && arr[shape_cell[2]->get_cell_y() - 1][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE && arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1].get_status() == FALSE)
		{
			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(0);

			shape_cell[0] = &arr[shape_cell[2]->get_cell_y() - 1][shape_cell[2]->get_cell_x() - 1];
			shape_cell[1] = &arr[shape_cell[2]->get_cell_y()][shape_cell[2]->get_cell_x() - 1];
			shape_cell[3] = &arr[shape_cell[2]->get_cell_y() + 1][shape_cell[2]->get_cell_x()];

			for (int i = 0; i < 4; i++)
				shape_cell[i]->set_status(1);

			set_rot(0);
		}
		Shape_Draw(h);
		break;
	}
}

///////////////////////////////////////////////
/// ����� "�������" ��������� �����"
///////////////////////////////////////////////

void Queue::Push_back(int num)
{
	Node * tmp = new Node(num);

	if (size == 0) { start = end = tmp; }
	else
	{
		tmp->set_Prev(end);
		end->set_Next(tmp);
		end = tmp;
	}
	size++;
}

void Queue::Pop_start()
{
	if (size == 0)
		return;
	if (size == 1)
	{
		delete start;
		start = end = nullptr;
		size--;
		return;
	}
	start = start->get_Next();
	delete start->get_Prev();
	start->set_Prev(nullptr);
	size--;
}

Queue::Queue(const Queue & obj) // ������������ �����������
{
	start = end = nullptr;
	size = 0;
	Node * cur = obj.start;
	while (cur)
	{
		Push_back(cur->get_Data());
		cur = cur->get_Next();
	}
}

//void Queue::Show(int num)
//{
//	switch
//
//}

///////////////////////////////////////////////
/// ����� "����"
///////////////////////////////////////////////
Game::Game()
{
	cur_id = maxId;
	first_init = FALSE;
	arr = new Cell*[row];
	for (int i = 0; i < row; i++)
		arr[i] = new Cell[col];
	
	MyShape[0] = new Line;
	MyShape[1] = new RightL;
	MyShape[2] = new LeftL;
	MyShape[3] = new Square;
	MyShape[4] = new Pyramide;
	MyShape[5] = new RightZ;
	MyShape[6] = new LeftZ;

	next_shape = new Line;
}

Game::~Game()
{
	/*for (int i = 0; i < row; i++)
		delete arr[i];
	delete arr;*/
}

void Game::Game_init(HWND h, HWND Box_score, UINT_PTR timer)
{
	Game::Destroy(h);
	srand((unsigned int)time(0));

	if (first_init == FALSE)
	{
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
			{
				arr[i][j].set_id(cur_id++);
				arr[i][j].set_xy(j, i);
			}
		//score = 200;
		first_init = TRUE;

		for (int i = 0; i < 3; i++)
		{
			cur_shape = rand() % 6;
			ShapeList.Push_back(cur_shape);
		}
	}

	ShapeList.Pop_start();
	cur_shape = rand() % 6;
	ShapeList.Push_back(cur_shape);

	cur_shape = ShapeList.get_start()->get_Data();

		if (Game::Check_end(h, Box_score, arr, timer))
			return;

		MyShape[cur_shape]->New_Shape(0, 5, arr);
		Show_next(h, ShapeList.get_end()->get_Prev()->get_Data(), 0);
		Show_next(h, ShapeList.get_end()->get_Data(), 1);
		//MyShape[ShapeList.get_start()->get_Next()->get_Data()]->New_Shape(5, 20, arr);
		//next_shape->Next_Shape(10, 15, h);
		
	Game::Show(h, Box_score);
}

void Game::Show(HWND h, HWND Box_score)
{
	for (int i = 0; i < row; i++)
		for (int j = 0; j < col; j++)
			if (arr[i][j].get_status()==TRUE)
				arr[i][j].Draw(h);

	wchar_t buff[256];
	wsprintfW(buff, L"%d", score);
	SetWindowText(Box_score, buff);
}

void Game::Move_Down(HWND h, HWND Box_score, UINT_PTR timer)
{
	if (MyShape[cur_shape]->Move_down(arr, h) == FALSE)
	{
		Game::Check(h, Box_score);
		Game::Game_init(h, Box_score, timer);
	}
}

void Game::Move_Right(HWND h, HWND Box_score)
{
	MyShape[cur_shape]->Move_right(arr, h);
}

void Game::Move_Left(HWND h, HWND Box_score)
{
	MyShape[cur_shape]->Move_left(arr, h);
}

void Game::Rotation(HWND h, HWND Box_score)
{
	MyShape[cur_shape]->Rotate(arr, h);
}

void Game::Check(HWND h, HWND Box_score)
{
	for (int i = row - 1; i >= 0; i--)
	{
		int m = 0;

		for (int j = 0; j < col; j++)
		{
			if (arr[i][j].get_status() == TRUE)
				m++;	
		}

		if (m == col)
		{
			for (int j = 0; j < col; j++)
				arr[i][j].set_status(0);
			score = score + 10;

			for (int k = i; k > 0; k--)
				for (int l = 0; l < col; l++)
					arr[k][l].set_status(arr[k - 1][l].get_status());

			for (int n = 0; n < col; n++)
				arr[0][n].set_status(0);
			i++;
		}
	}

}

bool Game::Check_end(HWND h, HWND Box_score, Cell** arr, UINT_PTR timer)
{
	if (arr[0][5].get_status() == TRUE)
	{
		KillTimer(h, timer);
		wchar_t buff[256];
		wsprintfW(buff, L"%d", score);

		MessageBox(h, L"YOU LOST!", L"GAME OVER!", MB_ICONERROR);

		first_init = FALSE;
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				arr[i][j].set_status(0);

		return 1;		
	}
	return 0;
}

void Game::Destroy(HWND h)
{
		for (int i = maxId; i <= cur_id; i++)
			DestroyWindow(GetDlgItem(h, i));
}

void Game::Show_next(HWND h, int shape_type, int count)
{
	Cell Next_shape_cell[4];

	int idnum = 4000 + 4*count;
	int x = 320 + 100*count;
	int y = 50 + count;

	for (int i = idnum; i <= 4010; i++)
		DestroyWindow(GetDlgItem(h, i));

	switch (shape_type)
	{
	case 0:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x, y + cell_width * 2);
		Next_shape_cell[3].set_xy(x, y + cell_width * 3);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	case 1:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x, y + cell_width * 2);
		Next_shape_cell[3].set_xy(x + cell_width, y + cell_width * 2);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	case 2:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x, y + cell_width * 2);
		Next_shape_cell[3].set_xy(x - cell_width, y + cell_width * 2);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	case 3:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x + cell_width, y);
		Next_shape_cell[3].set_xy(x + cell_width, y + cell_width);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	case 4:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x + cell_width, y + cell_width);
		Next_shape_cell[3].set_xy(x - cell_width, y + cell_width);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	case 5:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x - cell_width, y + cell_width);
		Next_shape_cell[3].set_xy(x - cell_width, y + cell_width*2);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	case 6:
		Next_shape_cell[0].set_xy(x, y);
		Next_shape_cell[1].set_xy(x, y + cell_width);
		Next_shape_cell[2].set_xy(x + cell_width, y + cell_width);
		Next_shape_cell[3].set_xy(x + cell_width, y + cell_width * 2);

		for (int i = 0; i < 4; i++)
		{
			Next_shape_cell[i].set_id(idnum++);
			CreateWindow(L"STATIC", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, Next_shape_cell[i].get_cell_x(), Next_shape_cell[i].get_cell_y(), cell_width, cell_width, h, HMENU(Next_shape_cell[i].get_id()), NULL, NULL);
		}
		break;
	}
}