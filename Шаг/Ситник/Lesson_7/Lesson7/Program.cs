﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ArrayList
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList collection = new ArrayList();
            collection.Add(1);
            collection.Add("Hello ArrayList!");
            collection.Add(new object());
            collection.Add(new ArrayList());

            collection.AddRange(new object[3] { 1, "Test", 3.14 });

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }

            string hello = collection[1].ToString();
            Console.WriteLine(collection[1]);
        }
    }
}
