﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_HrCodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {


            HrContext context = new HrContext();
            JobTitle job = new JobTitle() { NameJobTitle = "Test" };
            context.JobTitles.Add(job);
            context.SaveChanges();

            Console.WriteLine("DB create OK");
        }
    }
}
