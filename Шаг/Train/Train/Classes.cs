﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace train
{
    class Place
    {
        public int Number { get; set; }
        public string Surname { get; set; }

        public Place(string s)
        {
            Surname = s;
        }

        public void Show()
        {
            Console.WriteLine("Number = {0}, Surname = {1}", Number, Surname);
        }
    }

    class Car
    {
        public event ChangedEventHandler Changed;
        public int type { get; set; }
        public int allPlace { get; set; }
        public int soldPlace { get; set; }
        public Place[] list;

        public Car(int t)
        {
            type = t;
            if (type == 1)
                allPlace = 24;
            else if (type == 2)
                allPlace = 36;
            else if (type == 3)
                allPlace = 48;
            list = new Place[allPlace];
            soldPlace = 0;
        }

        public void Show(Place p)
        {
            Console.WriteLine("Car Number = {0}", type);
            p.Show();
        }

        public void AddPlace(Place p)
        {
            if (soldPlace > allPlace * 0.85)
            {
                if (Changed != null)
                    Changed(this);
            }
            Random r = new Random(DateTime.Now.Millisecond);
            for (;;)
            {
                int y = r.Next(1, allPlace);
                if (list[y] == null)
                {
                    p.Number = y;
                    list[y - 1] = p;
                    soldPlace++;
                    break;
                }
            }
        }
    }

    class Train
    {
        public List<Car> CarList = new List<Car>();

        public Train()
        {

        }

        public void AddCar(Car c)
        {
            CarList.Add(c);
        }

        public void NessecityOfAdd(Car sender)
        {
            sender.Changed -= NessecityOfAdd;
            Car add = new Car(sender.type);
            add.Changed += NessecityOfAdd;
            AddCar(add);
        }
    }
}
