﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_Array
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
    class Program
    {
        static void InitArray(int[] arrayInt)
        {
            Random r = new Random();
            for (int i = 0; i < arrayInt.Length; i++)
            {
                arrayInt[i] = r.Next(-100, 100);
                Console.WriteLine(arrayInt[i]);
            }
        }
        static void Main(string[] args)
        {
            int size = 12;
            int[] arrayInt = new int[10];
            for (int i = 0; i < arrayInt.Length; i++)
            {
                Console.WriteLine(arrayInt[i]);
            }
            Console.WriteLine("\n-------------------\n");
            double[] arrayDouble = new double[size];
            for (int i = 0; i < arrayDouble.GetLength(0); i++)
            {
                Console.WriteLine(arrayDouble[i] + " ");
            }
            Console.WriteLine("\n------------------------\n");
            size = 13;
            Point[] arrayPoint = new Point[size];
            for (int i = 0; i < arrayPoint.GetLength(0); i++)
            {
                Console.WriteLine(arrayPoint[i] + " ");
            }
            InitArray(arrayInt);

            Console.ReadLine();
        }
    }
}
