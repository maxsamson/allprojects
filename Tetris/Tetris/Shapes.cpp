#include "Shapes.h"

static COORD operator+ (COORD a, COORD b)
{
	COORD c;
	c.X = a.X + b.X;
	c.Y = a.Y + b.Y;
	return c;
}

//////////
//Shape
//////////

Shape::Shape()
{

}

Shape::~Shape() {}

void Shape::setState()
{

}

void Shape::Turn()
{

}

//For moving down, left, right
void Shape::ChangePos(char c)
{
	// M_LEFT -> left shift, M_RIGHT -> right shift, (c==2) -> down shift.

	if (c == M_LEFT)
	{
		for (int i = 0; i < 4; i++)
		{
			Shape::pos[i].X--;
		}
	}
	if (c == M_RIGHT)
	{
		for (int i = 0; i < 4; i++)
		{
			Shape::pos[i].X++;
		}
	}
	if (c == M_DOWN)
	{
		for (int i = 0; i < 4; i++)
		{
			Shape::pos[i].Y++;
		}
	}
};

void Shape::Draw(char c)
{
	for (int i = 0; i < 4; i++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), Shape::pos[i]);
		putchar(c);
	}
};

COORD* Shape::getPos()
{
	return Shape::pos;
}

int Shape::getType()
{
	return Shape::type;
}

void Shape::setType(int t)
{
	Shape::type = t;
}

//////////
//Line
//////////

Line::Line(COORD c)
{
	for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + Line::coords[i];
	}
	Line::state = 1;
}

Line::~Line() {}

void Line::setState()
{
	Line::state += 1;
	if (Line::state >= 2)
		Line::state = 0;
}

void Line::Turn()
{
	if (Line::state == 0)
	{
		Shape::pos[0] = Shape::pos[0] + COORD{ 1, -1 };
		Shape::pos[1] = Shape::pos[1] + COORD{ 0, 0 };
		Shape::pos[2] = Shape::pos[2] + COORD{ -1, 1 };
		Shape::pos[3] = Shape::pos[3] + COORD{ -2, 2 };
	}
	else
	{
		Shape::pos[0] = Shape::pos[0] + COORD{ -1, 0 };
		Shape::pos[1] = Shape::pos[1] + COORD{ 0, -1 };
		Shape::pos[2] = Shape::pos[2] + COORD{ 1, -2 };
		Shape::pos[3] = Shape::pos[3] + COORD{ 2, -3 };
	}
}

//////////
//Lblock
//////////

Lblock::Lblock(COORD c) 
{
	for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + Lblock::coords[i];
	}
	Lblock::state = 3;
}

Lblock::~Lblock() {}

void Lblock::setState()
{
	Lblock::state += 1;
	if (Lblock::state >= 4)
		Lblock::state = 0;
}

void Lblock::Turn()
{
	if (Lblock::state == 0)
	{
		Shape::pos[0] = Shape::pos[0] + COORD{ 2, 0 };
		Shape::pos[1] = Shape::pos[1] + COORD{ 1, 1 };
		Shape::pos[2] = Shape::pos[2] + COORD{ 0, 2 };
		Shape::pos[3] = Shape::pos[3] + COORD{ 1, -1 };
	}
	else if (Lblock::state == 1)
	{
		Shape::pos[0] = Shape::pos[0] + COORD{ 0, 1 };
		Shape::pos[1] = Shape::pos[1] + COORD{ -1, 0 };
		Shape::pos[2] = Shape::pos[2] + COORD{ -2, -1 };
		Shape::pos[3] = Shape::pos[3] + COORD{ 1, 0 };
	}
	else if (Lblock::state == 2)
	{
		Shape::pos[0] = Shape::pos[0] + COORD{ -1, 1 };
		Shape::pos[1] = Shape::pos[1] + COORD{ 0, 0 };
		Shape::pos[2] = Shape::pos[2] + COORD{ 1, -1 };
		Shape::pos[3] = Shape::pos[3] + COORD{ 0, 2 };
	}
	else
	{
		Shape::pos[0] = Shape::pos[0] + COORD{ -1, -2 };
		Shape::pos[1] = Shape::pos[1] + COORD{ 0, -1 };
		Shape::pos[2] = Shape::pos[2] + COORD{ 1, 0 };
		Shape::pos[3] = Shape::pos[3] + COORD{ -2, -1 };
	}
}

//////////
//reverseLblock
//////////

ReverseLblock::ReverseLblock(COORD c) 
{
	/*for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + ReverseLblock::coords[i];
	}*/
}

ReverseLblock::~ReverseLblock() {}

//////////
//Zblock
//////////

Zblock::Zblock(COORD c) 
{
	/*for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + Zblock::coords[i];
	}*/
}

Zblock::~Zblock() {}

//////////
//reverseZblock
//////////

ReverseZblock::ReverseZblock(COORD c) 
{
	/*for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + ReverseZblock::coords[i];
	}*/
}

ReverseZblock::~ReverseZblock() {}

//////////
//Square
//////////

Square::Square(COORD c) 
{
	/*for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + Square::coords[i];
	}*/
}

Square::~Square() {}

//////////
//Tblock
//////////

Tblock::Tblock(COORD c) 
{
	/*for (int i = 0; i < 4; i++)
	{
		Shape::pos[i] = c + Tblock::coords[i];
	}*/
}

Tblock::~Tblock() {}