﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    public delegate void MethodHandler();

    class ClassSender
    {
        public MethodHandler SomeEvent;

        protected void OnSomeEvent()
        {
            if (SomeEvent != null)
                SomeEvent();
        }

        public int Counter()
        {
            int sum = 0;
            for (int i = 0; i <= 100; i++)
            {
                sum += i;
                if (i == 71)
                {
                    OnSomeEvent();
                }
            }
            return sum;
        }

        public void ShowEvent()
        {
            Console.WriteLine("Произошло событие - i = 71");
        }
    }
}
