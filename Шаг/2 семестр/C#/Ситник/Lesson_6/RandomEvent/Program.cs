﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            RandomEvent randEvent = new RandomEvent();

            Receiver r = new Receiver();

            randEvent.MyEvent += r.Message;

            int[] arr = new int[10];

            randEvent.InitArray(arr);

            Console.WriteLine("\n----------------------\n");

            randEvent.ShowArray(arr);

            Console.ReadKey();
        }
    }
}
