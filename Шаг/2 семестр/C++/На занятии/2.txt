#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

using namespace std;

 /*class Point3D
{
int x;
int y;
int z;
public:
Point3D(int x = 0, int y = 0, int z = 0)
{
this->x = x;
this->y = y;
this->z = z;
}

void SetX(int x)
{
this->x = x;
}

void SetY(int y)
{
this->y = y;
}

void SetZ(int z)
{
this->z = z;
}

int GetX(int x)
{
return x;
}

int GetY(int y)
{
return y;
}

int GetZ(int z)
{
return z;
}

void Show()
{
cout << "X = " << x << endl;
cout << "Y = " << y << endl;
cout << "Z = " << z << endl;
}
};

class Point2D
{
int x;
int y;
public:
Point2D(int x = 0, int y = 0)
{
this->x = x;
this->y = y;
}

void SetX(int x)
{
this->x = x;
}

void SetY(int y)
{
this->y = y;
}

int GetX(int x)
{
return x;
}

int GetY(int y)
{
return y;
}

void Show()
{
cout << "X = " << x << endl;
cout << "Y = " << y << endl;
}

Point2D operator+(const Point2D& obj)
{
return Point2D(x + obj.x, y + obj.y);
}

Point2D operator+(int a)
{
return Point2D(x + a, y + a);
}

bool operator<(const Point2D& obj)
{
return x < obj.x;
}

operator int()
{
return x;
}

explicit operator Point3D()
{
return Point3D(x, y, 0);
}

Point2D operator++(int)
{
Point2D tmp(x, y);
tmp.x++;
tmp.y++;
return tmp;
}

Point2D& operator=(const Point2D& obj)
{
x = obj.x;
y = obj.y;
return *this;
}

friend Point2D operator+(int, const Point2D&);
friend ostream& operator<<(ostream&, const Point2D&);
friend istream& operator>>(istream&, Point2D&);
};

Point2D operator+(int t, const Point2D& obj)
{
return Point2D(t + obj.x, t + obj.y);
}

ostream& operator<<(ostream& os, const Point2D& obj)
{
os << "X = " << obj.x << endl;
os << "Y = " << obj.y << endl;
return os;
}

istream& operator>>(istream& is, Point2D& obj)
{
cout << "������� x: ";
is >> obj.x;
cout << "������� y: ";
is >> obj.y;
return is;
}

class Arr
{
int size;
int* mass;
public:
Arr(int s = 0, int val = 0)
{
if (s == 0)
{
size = 0;
mass = nullptr;
}
else
{
mass = new int[s];
size = s;
for (int i = 0; i < size; i++)
mass[i] = val;
}
}

Arr(const Arr& obj)
{
if (obj.size == 0)
{
size = 0;
mass = nullptr;
}
else
{
mass = new int[obj.size];
size = obj.size;
for (int i = 0; i < size; i++)
mass[i] = obj.mass[i];
}
}

void Show()
{
for (int i = 0; i < size; i++)
cout << mass[i] << " ";
cout << endl;
}

Arr& operator=(const Arr& obj)
{
if (this == &obj)
return *this;
if (size)
delete[] mass;
if (obj.size == 0)
{
size = 0;
mass = nullptr;
}
else
{
mass = new int[obj.size];
size = obj.size;
for (int i = 0; i < size; i++)
mass[i] = obj.mass[i];
}
}

int& operator[](int pos)
{
return mass[pos];
}

void operator()(int s, int val)
{
if (size)
delete[] mass;
size = s;
mass = new int[size];
for (int i = 0; i < size; i++)
mass[i] = val;
}

friend void Print(Arr);
};

void Print(Arr obj)
{
for (int i = 0; i < obj.size; i++)
cout << obj.mass[i] << " ";
}*/

 /*template <class T>
class Point
{
	T x;
	T y;
public:
	Point(T _x=0, T _y=0)
	{
		x = _x;
		y = _y;
	}

	void SetX(T _x)
	{
		x = _x;
	}

	void SetY(T _y)
	{
		y = _y;
	}

	T GetX()
	{
		return x;
	}

	T GetY()
	{
		return y;
	}

	void Show()
	{
		cout << "X = " << x << endl;
		cout << "Y = " << y << endl;
	}
};

template <class T>
class Shape
{
	Point<T>*f;
	int size;
public:
	Shape(int s = 0)
	{
		if (s < 3)
		{
			f = nullptr;
			size = 0;
		}
		else
		{
			size = s;
			f = new Point<T>[size];
		}
	}

	void Init()
	{
		for (int i = 0; i < size; i++)
		{
			f[i].SetX(rand() % 100);
			f[i].SetY(rand() % 100);
		}
	}

	void Show()
	{
		for (int i = 0; i < size; i++)
			f[i].Show();
	}

	~Shape()
	{
		if (size)
			delete[] f;
	}

	Shape(const Shape<T>& obj)
	{
		if (obj.size == 0)
		{
			size = 0;
			f = nullptr;
		}
		else
		{
			f = new Point<t>[obj.size];
			size = obj.size;
			for (int i = 0; i < size; i++)
				f[i] = obj.f[i];
		}
	}

	Shape<T>& operator=(const Shape<T>& obj)
	{
		if (this == &obj)
			return *this;
		if (size)
			delete[] f;
		if (obj.size == 0)
		{
			size = 0;
			f = nullptr;
		}
		else
		{
			f = new Point<T>[obj.size];
			size = obj.size;
			for (int i = 0; i < size; i++)
				f[i] = obj.f[i];
		}
	}
};*/

class Node
{
	int data;
	Node *next;
	Node *prev;
public:
	Node(int d = 0)
	{
		data = d;
		next = prev = nullptr;
	}

	void SetData(int d)
	{
		data = d;
	}

	void SetNext(Node *n)
	{
		next = n;
	}

	void SetPrev(Node *n)
	{
		prev = n;
	}

	int GetData()
	{
		return data;
	}

	Node* GetNext()
	{
		return next;
	}

	Node* GetPrev()
	{
		return prev;
	}
};

class Queue
{
	Node* start;
	Node* end;
	int size;
public:
	Queue()
	{
		start = nullptr;
		size = 0;
	}

	void Push(int d)
	{
		Node *tmp = new Node(d);
		if (size == 0)
			start = end = tmp;
		else
		{
			tmp->SetPrev(end);
			end->SetNext(tmp);
			end = tmp;
		}
		size++;
	}

	Queue(const Queue& obj)
	{
		start = end = nullptr;
		size = 0;
		Node *cur = obj.start;
		while (cur)
		{
			Push(cur->GetData());
			cur = cur->GetNext();
		}
	}

	void Pop()
	{
		if (size == 0)
			return;
		if (size == 1)
		{
			delete start;
			start = end = nullptr;
			size = 0;
			return;
		}
		start = start->GetNext();
		delete start->GetPrev();
		start->SetPrev(nullptr);
		size--;
	}

	Queue& operator=(const Queue& obj)
	{
		if (this == &obj)
			return *this;
		while (size)
			Pop();
		Node *cur = obj.start;
		while (cur)
		{
			Push(cur->GetData());
			cur = cur->GetNext();
		}
		return *this;
	}

	~Queue()
	{
		while (size)
			Pop();
	}
};

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	/*Shape<int>a(3);
	Shape<int>b;
	a.Init();
	a.Show();
	cout << endl;
	b = a;
	b.Show();*/
	system("pause");
}