// TETRIS_v2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TETRIS_v2.h"
#include "Main.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

////////////////////////////////////

HWND Box_up;
HWND Box_down;
HWND Box_right;
HWND Box_left;
HWND Box_next_1;
HWND Box_next_2;
HWND Box_score;
HWND Box_score_title;
HWND Box_start;
HWND Start;
HWND Exit;
HWND Box_level;
HWND Box_level_title;

Game g;

double timer_speed;

//int current_score;
//int new_score;
int next_level = 100;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_TETRIS_V2, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TETRIS_V2));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TETRIS_V2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_TETRIS_V2);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, 600, 700, nullptr, nullptr, hInstance, nullptr);

   Box_up = CreateWindow(TEXT("BUTTON"), TEXT("PAUSE"), WS_CHILD | WS_VISIBLE, 300, 370, 70, 30, hWnd, (HMENU)BOX_UP, nullptr, nullptr);
  // Box_down = CreateWindow(TEXT("BUTTON"), TEXT("DOWN"), WS_CHILD | WS_VISIBLE, 300, 330, 70, 30, hWnd, (HMENU)BOX_DOWN, nullptr, nullptr);
   //Box_right = CreateWindow(TEXT("BUTTON"), TEXT("->"), WS_CHILD | WS_VISIBLE, 335, 300, 35, 30, hWnd, (HMENU)BOX_RIGHT, nullptr, nullptr);
   //Box_left = CreateWindow(TEXT("BUTTON"), TEXT("<-"), WS_CHILD | WS_VISIBLE, 300, 300, 35, 30, hWnd, (HMENU)BOX_LEFT, nullptr, nullptr);
   Box_next_1 = CreateWindow(TEXT("STATIC"), TEXT("NEXT_1"), WS_CHILD | WS_VISIBLE, 300, 20, 70, 20, hWnd, (HMENU)BOX_NEXT_1, nullptr, nullptr);
   Box_next_2 = CreateWindow(TEXT("STATIC"), TEXT("NEXT_2"), WS_CHILD | WS_VISIBLE, 400, 20, 70, 20, hWnd, (HMENU)BOX_NEXT_2, nullptr, nullptr);
   Box_score_title = CreateWindow(TEXT("STATIC"), TEXT("SCORE"), WS_CHILD | WS_VISIBLE, 310, 150, 50, 20, hWnd, (HMENU)BOX_SCORE_TITLE, nullptr, nullptr);
   Box_score = CreateWindow(TEXT("STATIC"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER | SS_RIGHT, 295, 180, 80, 20, hWnd, (HMENU)BOX_SCORE, nullptr, nullptr);
   Box_level_title = CreateWindow(TEXT("STATIC"), TEXT("LEVEL"), WS_CHILD | WS_VISIBLE | WS_BORDER, 295, 250, 50, 20, hWnd, (HMENU)BOX_LEVEL_TITLE, nullptr, nullptr);
   Box_level = CreateWindow(TEXT("STATIC"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER | SS_RIGHT, 350, 250, 30, 20, hWnd, (HMENU)BOX_LEVEL, nullptr, nullptr);
   Box_start = CreateWindow(TEXT("BUTTON"), TEXT("start"), WS_CHILD | WS_VISIBLE, 300, 300, 70, 30, hWnd, (HMENU)BOX_START, nullptr, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
 
   timer_speed = 1000;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_TIMER: 
	{
		g.Move_Down(hWnd, Box_score, TIMER);

		if(g.get_score() >= next_level)
		{
			KillTimer(hWnd, TIMER);
			timer_speed = timer_speed*0.9;
			SetTimer(hWnd, TIMER, timer_speed, nullptr);

			wchar_t buff[256];
			wsprintfW(buff, L"%d", next_level/100);
			SetWindowText(Box_level, buff);
			//g.set_next_level(FALSE);

			next_level = next_level + 100;
		}

	}
	break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
			switch (wmId)
			{
			case BOX_START:
				if (!g.get_first_init())
				{
					SetTimer(hWnd, TIMER, timer_speed, nullptr);
					g.Game_init(hWnd, Box_score, TIMER);
					wchar_t buff[256];
					wsprintfW(buff, L"%d", g.get_score() / 100);
					SetWindowText(Box_level, buff);
				}
				SetFocus(hWnd);
				break;
			case BOX_UP:
				KillTimer(hWnd, TIMER);				
				MessageBox(hWnd, L"Game is paused!", L"PAUSE!", MB_ICONERROR);
				SetTimer(hWnd, TIMER, timer_speed, nullptr);
				SetFocus(hWnd);
				break;
			/*case BOX_DOWN:
				if (g.get_first_init())
					g.Move_Down(hWnd, Box_score, TIMER);
				SetFocus(hWnd);
				break;
			case BOX_RIGHT:
				if (g.get_first_init())
					g.Move_Right(hWnd, Box_score);
				SetFocus(hWnd);
				break;
			case BOX_LEFT:
				if (g.get_first_init())
					g.Move_Left(hWnd, Box_score);
				SetFocus(hWnd);
				break;*/
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
	case WM_KEYDOWN:
	{
		SetFocus(hWnd);
		switch (wParam)
		{
		case VK_DOWN:
			if (g.get_first_init())
			{
				g.Move_Down(hWnd, Box_score, TIMER);
				SetFocus(hWnd);
			}
			break;
		case VK_RIGHT:
			if (g.get_first_init())			
				g.Move_Right(hWnd, Box_score);
			break;
		case VK_LEFT:
			if (g.get_first_init())
				g.Move_Left(hWnd, Box_score);
			break;
		case VK_UP:
			if (g.get_first_init())
				g.Rotation(hWnd, Box_score);
			break;
		}
	}

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
			Rectangle(hdc, 20, 20, 220, 520);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
