﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_Example
{
    class Program
    {
        static void InitArray(int[] a)
        {
            Random r = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = r.Next(-100, 100);
            }
        }
        static int Count(int[] a, int p, int q)
        {
            int count = 0;
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
                if (a[i] < q & a[i] > p)
                {
                    count++;
                }
            }
            return count;
        }
        static void Swap(ref int p, ref int q)
        {
            if (p > q)
            {
                int d = p;
                p = q;
                q = d;
            }
        }
        static void PosAndCount(int[] a, ref int pos, ref int count)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < 0)
                {
                    pos = i + 1;
                    count = a[i];
                }
            }
        }

        static void Main(string[] args)
        {
            int[] a = new int[10];
            InitArray(a);
            Console.WriteLine("Введите 2 числа: ");
            int p, q;
            p = Convert.ToInt32(Console.ReadLine());
            q = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");
            Swap(ref p, ref q);
            int count;
            count = Count(a, p, q);
            Console.WriteLine("\nКоличество = {0}", count);
            Console.WriteLine("\n----------------------------\n");
            int pos = -1;
            PosAndCount(a, ref pos, ref count);
            if (pos == -1)
                Console.WriteLine("Нет отрицательных чисел в массиве.");
            else
                Console.WriteLine("Число: {0}, позиция: {1}", count, pos);
            Console.ReadLine();
        }
    }
}
