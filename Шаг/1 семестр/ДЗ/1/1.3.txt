#include <iostream>
#include <locale.h>
#include <math.h>

using namespace std;

void main()
{
	setlocale(LC_ALL, "RUS");
	int x, y;
	float s, z;
	cout << "������� ��������� ���������: ";
	cin >> x;
	if ((x<1) || (x>3)){
		cout << "����������� ������� ���������, �������� ��������: 1, 2, 3.\n������� ��������� ���������: ";
		cin >> x;
	}
	cout << "������� ���� ���������: ";
	cin >> y;
	switch (x)
	{
	case 1:
		s = 3000.0;
		break;
	case 2:
		s = 2000.0;
		break;
	case 3:
		s = 1000.0;
		break;
	default:
		break;
	}
	if (y < 2)
		z = s-s*0.26;
	else if (y < 5)
		z = (s + s*0.1)-(s + s*0.1)*0.26;
	else if (y < 10)
		z = (s + s*0.2)-(s + s*0.2)*0.26;
	else
		z = (s + s*0.3)-(s + s*0.3)*0.26;
	cout << "�������� ���������� ����� ���������� " << z << endl;
	system("pause");
}