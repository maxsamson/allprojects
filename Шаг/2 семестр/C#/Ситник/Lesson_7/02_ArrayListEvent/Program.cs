﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ArrayListEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayListEvents list = new ArrayListEvents();

            EventReceiver1 r1 = new EventReceiver1();
            r1.RegisterEvent(list);

            list.Add(1);
            list.Add(12);
            list.Add(3);
            list.Add(6);

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }


        }
    }
}
