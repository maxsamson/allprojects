﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_HashTable
{
    public class Fish
    {
        public string name { get; set; }

        public Fish(string name)
        {
            this.name = name;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region 
            //var MyStack = new Stack();
            //MyStack.Push('A');
            //MyStack.Push(123);
            //MyStack.Push("test");//работает с объектами

            //Console.WriteLine("Исходный стек: ");
            //foreach (object s in MyStack)
            //    Console.Write(s);
            //Console.WriteLine("\n");

            //while (MyStack.Count > 0)
            //{
            //    Console.WriteLine("Pop -> {0}", MyStack.Pop());
            //}

            //if (MyStack.Count == 0)
            //    Console.WriteLine("\nСтек пуст!");
            #endregion
            var duplicates = new Hashtable();

            var key1 = new Fish("Herring");
            var key2 = new Fish("Herring");

            duplicates[key1] = "Hello";
            duplicates[key2] = "Hello2";

            //var hashtable = new Hashtable();
            //hashtable.Add("Ukraine", "Kiev");
            //hashtable.Add("USA", "Washington");
            //hashtable.Add("Russian", "Moscow");
            //Console.WriteLine(hashtable["Russian"]);
            //hashtable["Russian"] = "Vladivostok";
            //Console.WriteLine("Change Key Russian");
            //Console.WriteLine(hashtable["Russian"]);
        }
    }
}
