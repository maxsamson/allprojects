﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace train
{
    delegate void ChangedEventHandler(Car sender);


    class Program
    {
        static void Main(string[] args)
        {
            Train train = new Train();
            for (int i = 1; i <= 3; i++)
            {
                Car c = new Car(i);
                c.Changed += train.NessecityOfAdd;
                train.AddCar(c);
            }
            for (int i = 0; i < 300; i++)
            {
                Thread.Sleep(20);
                Random r = new Random(DateTime.Now.Millisecond);
                int x = r.Next(1, 100);
                int y;
                if (x <= 15)
                    y = 1;
                else if (x <= 70)
                    y = 2;
                else
                    y = 3;
                for (int j = 0; j < train.CarList.Count; j++)
                {
                    if (train.CarList[j].type == y)
                    {
                        if (train.CarList[j].soldPlace < train.CarList[j].allPlace - 1)
                        {
                            int n;
                            string name = "";
                            for (int k = 0; k < 3; k++)
                            {
                                n = r.Next(97, 122);
                                name += Convert.ToChar(n);
                            }
                            Place p = new Place(name);
                            train.CarList[j].AddPlace(p);
                            train.CarList[j].Show(p);
                            Console.WriteLine("\n_____________\n");
                        }
                    }
                }
            }
        }
    }
}
