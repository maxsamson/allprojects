#pragma once
#include <Windows.h>
#include <stdio.h>
#include <conio.h>

enum Key { M_UP = 72, M_LEFT = 75, M_RIGHT = 77, M_DOWN = 80, M_ENTER = 13, M_ESC = 27 };//���� ������ ����������

/*
	xxxx - Line
	
	xxx - Lblock
	x
	
	xxx - ReverseLblock
	  x
	
	xx  - Zblock
	 xx
	
	 xx - ReverseZblock
	xx

	xx - Square
	xx

	xxx - Tblock
	 x
*/

//abstract class
class Shape
{
protected:
	COORD pos[4];
	int type; //��� ������: 0 - Line, 1 - Lblock, 2 - reverseLblock, 3 - Zblock, 4 - reverseZblock, 5 - Square, 6 - Tblock

public:
	Shape();
	~Shape();
	void Draw(char);
	virtual void setState();
	virtual void Turn();
	virtual void ChangePos(char);
	COORD* getPos();
	int getType();
	void setType(int);
};

class Line : public Shape
{
protected:
	COORD coords[4] = { {-1,0},{0,0},{1,0},{2,0} };
	
	//0 - vertical, 1 - horizontal
	int state;

public:
	Line(COORD);
	~Line();
	void setState() override;
	void Turn() override;
};

class Lblock : public Shape
{
protected:
	COORD coords[4] = { { -1,0 },{ 0,0 },{ 1,0 },{ -1,1 } };

	int state;

public:
	Lblock(COORD);
	~Lblock();
	void setState() override;
	void Turn() override;
};

class ReverseLblock
{
protected:
	COORD coords[4] = { { -1,0 },{ 0,0 },{ 1,0 },{ 2,1 } };

public:
	ReverseLblock(COORD);
	~ReverseLblock();

};

class Zblock
{
protected:
	COORD coords[4] = { { -1,0 },{ 0,0 },{ 0,1 },{ 1,1 } };

public:
	Zblock(COORD);
	~Zblock();

};

class ReverseZblock
{
protected:
	COORD coords[4] = { { -1,1 },{ 0,1 },{ 0,0 },{ 1,0 } };

public:
	ReverseZblock(COORD);
	~ReverseZblock();

};

class Square
{
protected:
	COORD coords[4] = { { 0,0 },{ 1,0 },{ 0,1 },{ 1,1 } };

public:
	Square(COORD);
	~Square();

};

class Tblock
{
protected:
	COORD coords[4] = { { -1,0 },{ 0,0 },{ 1,0 },{ 0,1 } };

public:
	Tblock(COORD);
	~Tblock();

};