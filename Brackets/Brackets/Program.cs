﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brackets
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку: ");
            string line = Console.ReadLine();
            List brackets = new List();
            foreach (char item in line)
            {
                switch(item)
                {
                    case '(':  case '{' : case '[' :
                        brackets.Push(item);
                        break;
                    case ')':
                        if (brackets.Pop() != '(')
                        {
                            Console.WriteLine("Руки из жопы достань!");
                            goto Finish;
                        }
                        break;
                    case '}':
                        if (brackets.Pop() != '{')
                        {
                            Console.WriteLine("Руки из жопы достань!");
                            goto Finish;
                        }
                        break;
                    case ']':
                        if (brackets.Pop() != '[')
                        {
                            Console.WriteLine("Руки из жопы достань!");
                            goto Finish;
                        }
                        break;
                }
            }
            if (brackets.Head != null)
                goto Finish;
            Console.WriteLine("Усё гуд!");
            return;
            Finish:
                Console.WriteLine("Давай по новой, Миша, всё дурня.");
        }
    }

    class NodeList
    {
        public char data;
        public NodeList Next;

        public NodeList(char data)
        {
            this.data = data;
            this.Next = null;
        }
    }

    class List
    {
        public NodeList Head;
        private int count;
        public int Count
        {
            get { return count; }
        }

        public List()
        {
            this.Head = null;
            this.count = 0;
        }

        public List(char data) : this()
        {
            Push(data);
        }

        public void Push(char data)
        {
            if (this.Head == null)
            {
                this.Head = new NodeList(data);
                this.count++;
                return;
            }
            NodeList temp = new NodeList(data);
            temp.Next = this.Head;
            this.Head = temp;
            this.count++;
        }

        public char Pop()
        {
            char data = this.Head.data;
            this.Head = this.Head.Next;
            this.count--;
            return data;
        }
    }
}
