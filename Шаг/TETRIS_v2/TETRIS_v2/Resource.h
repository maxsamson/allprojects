//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TETRIS_v2.rc
//

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_TETRIS_V2_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_TETRIS_V2			107
#define IDI_SMALL				108
#define IDC_TETRIS_V2			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#define BOX_UP		1024
#define BOX_DOWN	1025
#define BOX_LEFT	1026
#define BOX_RIGHT	1027
#define BOX_NEXT_1	1028
#define BOX_NEXT_2	1035
#define BOX_SCORE	1029
#define BOX_SCORE_TITLE	1030
#define BOX_START	1031
#define TIMER				1032
#define BOX_LEVEL	1033
#define BOX_LEVEL_TITLE 1034



#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
