﻿using _02_DbFirst.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;

namespace _03_LazyLoading
{
    class Program
    {
        static void Main(string[] args)
        {

            HumanResources context = new HumanResources();
            //context.EmpPromotions.Load();
            var query = from p in context.EmpPromotions
                        select new
                        {
                            p.EmployeeId,
                            FullName = p.Employee.FirstName + " " + p.Employee.LastName,
                            p.HireDate,
                            p.Salary,
                            NameJobTitle = p.JobTitle.NameJobTitle
                        };

            foreach (var item in query)
            {
                Console.WriteLine("{0}  {1} {2:d} {3}", item.FullName, item.NameJobTitle, item.HireDate, item.Salary);
            }   
        }
    }
}
