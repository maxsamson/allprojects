﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEventArgs
{
    public delegate void MethodHandler(int arg);

    class ClassSender
    {
        public event MethodHandler SomeEvent;

        protected void OnSomeEvent(int arg)
        {
            if (SomeEvent != null)
                SomeEvent(arg);
        }

        public int Counter()
        {
            int sum = 0;
            for (int i = 0; i <= 100; i++)
            {
                sum += i;
                if (i == 12 || i == 33 || i == 71)
                {
                    OnSomeEvent(i);
                }
            }
            return sum;
        }

        public void ShowEvent(int a)
        {
            Console.WriteLine("Произошло событие - i = {0}", a);
        }
    }
}
