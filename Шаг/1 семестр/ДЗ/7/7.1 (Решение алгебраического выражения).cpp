#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

using namespace std;

void Delete(char *&A, int &size, int pos, int count); //�������� count ��������� ������� � pos �������
void Compound(char *&A, char *B, int &size1, int size2, int pos); //���������� ������� B �� pos ������� ������� �
bool CountBrack(char *A, int size, int &j); //�������� �� ������������ ����� ������
char *SearchBrack(char *A, int size, int &size1, int &pos1, int &pos2, int &count); //���������� ����� ���������� ������
char *PriorityOperation(char *B, int size1, int &size2, int &posB1, int &posB2, int &count); //������� ������������ ��������
int SolutionPriorityOperation(char *C, int size2, bool &check); //������� ������������ ��������
void ReturnResultToStart(char *&A, int size, int result, int posB21, int pos); //����������� ���������� �� ����� ��������
void Programm(char *A, int size); //�������� ���� �� ����������� �������, ���� �� ����� ������ ����� ��� ������

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	int size = 150;
	char *A = new char[size];
	cout << "������� ���������: ";
	gets(A);
	cout << endl;
	Programm(A, size);
	system("pause");
}

void Delete(char *&A, int &size, int pos, int count)
{
	size -= count;
	char *C = new char[size];
	for (int i = 0; i < pos; i++)
		C[i] = A[i];
	for (int i = pos + count; i < size + count; i++)
		C[i - count] = A[i];
	delete[] A;
	A = C;
}

void Compound(char *&A, char *B, int &size1, int size2, int pos)
{
	size1 += size2;
	char *C = new char[size1];
	for (int i = 0; i < pos; i++)
		C[i] = A[i];
	for (int i = pos + 1; i < size2 + pos + 1; i++)
		C[i - 1] = B[i - pos - 1];
	for (int i = size2 + pos + 1; i < size1 + 1; i++)
		C[i - 1] = A[i - size2 - 1];
	delete[] A;
	A = C;
}

bool CountBrack(char *A, int size, int &j)
{
	int count = 0;
	for (int i = j; A[i] != ')'; i++, j++)
	{
		if (A[i] == '(')
			count++;
		if (i == strlen(A) - 1)
			break;
	}
	for (int i = j; A[i] != '('; i++, j++)
	{
		if (A[i] == ')')
			count--;
		if (i == strlen(A) - 1)
			break;
	}
	if (count != 0)
		return 0;
	else if (j < strlen(A) - 1)
		return CountBrack(A, size, j);
	else
		return 1;
}

char *SearchBrack(char *A, int size, int &size1, int &pos1, int &pos2, int &count)
{
	for (int i = 0; A[i] != ')'; i++)
	{
		if (A[i] == '(')
			pos1 = i + 1;
		if (A[i] == '\0')
			break;
		pos2 = i;
	}
	if (pos1 == 0)
		count++;
	size1 = pos2 - pos1 + 2;
	char *B = new char[size1];
	for (int i = 0; i < size1 - 1; i++)
		B[i] = A[i + pos1];
	B[size1 - 1] = '\0';
	return B;
}

char *PriorityOperation(char *B, int size1, int &size2, int &posB1, int &posB2, int &count)
{
	int pos = 0;
	for (int i = 0; i < 3; i++)
	{
		if (i == 0)
		{
			for (int j = 0; B[j] != '\0'; j++)
			{
				if (B[j] == '^')
				{
					pos = j;
					break;
				}
			}
		}
		else if (i == 1)
		{
			for (int j = 0; B[j] != '\0'; j++)
			{
				if ((B[j] == '*') || (B[j] == '/'))
				{
					pos = j;
					break;
				}
			}
		}
		else
		{
			for (int j = 0; B[j] != '\0'; j++)
			{
				if ((B[j] == '+') || (B[j] == '-'))
				{
					pos = j;
					break;
				}
			}
		}
		if (pos != 0)
			break;
	}
	char *C;
	if (pos != 0)
	{
		for (int i = pos - 1; i > 0; i--)
		{
			if ((B[i] == '*') || (B[i] == '/') || (B[i] == '+') || (B[i] == '-')||(B[i]=='^'))
			{
				posB1 = i + 1;
				break;
			}
		}
		for (int i = pos + 1; i < size1 - 1; i++)
		{
			if ((B[i] == '*') || (B[i] == '/') || (B[i] == '+') || (B[i] == '-')||(B[i]=='^'))
			{
				posB2 = i - 1;
				break;
			}
		}
		size2 = posB2 - posB1 + 1;
	}
	else
	{
		size2 = size1 - 1;
		count++;
	}
	C = new char[size2];
	for (int i = 0; i < size2; i++)
		C[i] = B[i + posB1];
	C[size2] = '\0';
	delete[] B;
	return C;
}

int SolutionPriorityOperation(char *C, int size2, bool &check)
{
	int count = 0, pos = 0, number1 = 0, number2 = 0, result;
	for (int i = 0; i < size2; i++)
	{
		if ((C[i] == '^') || (C[i] == '*') || (C[i] == '/') || (C[i] == '+') || (C[i] == '-'))
		{
			pos = i;
			break;
		}
		count++;
	}
	if (pos != 0)
	{
		for (int i = 0; i < pos; i++)
		{
			number1 += pow(10, count - 1)*(int(C[i]) - 48);
			count--;
		}
		count = size2 - pos - 1;
		for (int i = pos + 1; i < size2; i++)
		{
			number2 += pow(10, count - 1)*(int(C[i]) - 48);
			count--;
		}
		if (C[pos] == '^')
			result = pow(number1, number2);
		if (C[pos] == '*')
			result = number1*number2;
		if (C[pos] == '/')
		{
			if (number2 == 0)
			{
				check = 1;
				return 0;
			}
			else
				result = number1 / number2;
		}
		if (C[pos] == '+')
			result = number1 + number2;
		if (C[pos] == '-')
			result = number1 - number2;
	}
	else
	{
		result = 0;
		count = size2;
		for (int i = 0; i < size2; i++)
		{
			result += pow(10, count - 1) *(int(C[i]) - 48);
			count--;
		}
	}
	return result;
}

void ReturnResultToStart(char *&A, int size, int result, int posB21, int pos)
{
	char *Y = new char[150];
	int i = 0;
	for (i; A[i] != '\0'; i++)
		Y[i] = A[i];
	Y[i] = '\0';
	int count = posB21 + 1;
	Delete(Y, size, pos, count);
	int x = result, sizeX = 0;
	if (x == 0)
		sizeX = 1;
	else
	{
		for (; x > 0;)
		{
			sizeX++;
			x /= 10;
		}
	}
	char *X = new char[sizeX];
	_itoa(result, X, 10);
	Compound(Y, X, size, sizeX, pos);
	if (strcmp(A, Y) == 0)
	{
		posB21 += 2;
		count = posB21 + 1;
		pos -= 1;
	}
	Delete(A, size, pos, count);
	Compound(A, X, size, sizeX, pos);
	int str = strlen(A);
	A[str] = '\0';
}

void Programm(char *A, int size)
{
	int j = 0;
	bool k = CountBrack(A, size, j);
	if (k == 0)
		cout << "������ �����: �� ��������� ���������� ������ ��� ������ ������� � �������� �������." << endl;
	else
	{
		int size1, posA1, posA2, size2, posB1 = 0, posB2, x, count = 0;
		bool check = 0;
		char *B, *C;
		for (;;)
		{
			posA1 = 0;
			posA2 = 0;
			B = SearchBrack(A, size, size1, posA1, posA2, count);
			posB1 = 0;
			posB2 = size1 - 2;
			C = PriorityOperation(B, size1, size2, posB1, posB2, count);
			x = SolutionPriorityOperation(C, size2, check);
			if (check == 1)
			{
				cout << "������� �� ���� ������ �������. ��������� ������������ �������." << endl;
				break;
			}
			if (count == 2)
			{
				cout << "�����: " << x << endl;
				break;
			}
			int posB21 = posB2 - posB1;
			int pos = posA1 + posB1;
			ReturnResultToStart(A, size, x, posB21, pos);
			count = 0;
		}
		cout << endl;
	}
}