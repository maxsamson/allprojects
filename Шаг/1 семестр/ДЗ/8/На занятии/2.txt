#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

using namespace std;

void Fill(char **A, int size);
void Show(char **A, int size);
void Add(char **&A, int &size);
void Remove(char **&A, int &size);

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	int size;
	cout << "������� ���������� �����: ";
	cin >> size;
	cout << endl;
	char **A = new char*[size];
	Fill(A, size);
	Show(A, size);
	Add(A, size);
	Show(A, size);
	Remove(A, size);
	Show(A, size);
	system("pause");
}

void Fill(char **A, int size)
{
	for (int i = 0; i < size; i++)
	{
		char S[256];
		cout << "������� ������: ";
		fflush(stdin);
		gets(S);
		cout << endl;
		int x = strlen(S);
		A[i] = new char[x];
		for (int j = 0; j <= x; j++)
			A[i][j] = S[j];
	}
}

void Show(char **A, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; A[i][j] != '\0'; j++)
			cout << A[i][j];
		cout << endl;
	}
	cout << endl;
}

void Add(char **&A, int &size)
{
	size++;
	char **B = new char*[size];
	for (int i = 0; i < size - 1; i++)
	{
		int j = -1;
		int x = strlen(A[i]);
		B[i] = new char[x];
		do
		{
			j++;
			B[i][j] = A[i][j];
		} while (A[i][j] != '\0');
	}
	char S[256];
	cout << "������� ������: ";
	fflush(stdin);
	gets(S);
	cout << endl;
	int x = strlen(S);
	B[size - 1] = new char[x];
	for (int j = 0; j <= x; j++)
		B[size - 1][j] = S[j];
	delete[] A;
	A = B;
}

void Remove(char **&A, int &size)
{
	size--;
	char **B = new char*[size];
	for (int i = 0; i < size; i++)
	{
		int j = -1;
		int x = strlen(A[i]);
		B[i] = new char[x];
		do
		{
			j++;
			B[i][j] = A[i][j];
		} while (A[i][j] != '\0');
	}
	delete[] A;
	A = B;
}