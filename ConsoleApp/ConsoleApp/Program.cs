﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            for (;;)
            {
                int year = Convert.ToInt32(Console.ReadLine());
                if (((year % 100 == 0) && (year % 400 != 0)) || (year % 4 != 0))
                {
                    Console.WriteLine("No");
                    continue;
                }
                Console.WriteLine("Yes");
            }
        }
    }
}
