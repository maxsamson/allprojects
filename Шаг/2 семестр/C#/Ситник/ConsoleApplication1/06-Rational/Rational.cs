﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Rational
{
    class Rational
    {
        // Описание тела класса Rational
        //Поля класса. Числитель и знаменатель рационального числа. 

        static Rational()
        {
            Zero = new Rational(0, 1, "Zero");
            One = new Rational(1, 1, "One");
        }
        private Rational(int a, int b, string str)
        {
            m = a;
            n = b;
        }
        public static readonly Rational Zero, One;

        int m, n;
        public Rational(int a, int b)
        {
            if (b == 0) { m = 0; n = 1; }
            else
            {
                //приведение знака
                if (b < 0) { b = -b; a = -a; }
                //приведение к несократимой дроби
                int d = nod(a, b);
                m = a / d;
                n = b / d;
            }
        }

        int nod(int m, int n)
        {
            int p = 0;
            m = Math.Abs(m);
            n = Math.Abs(n);
            if (n > m)
            {
                p = m;
                m = n;
                n = p;
            }
            //do
            //{
            //    p = m % n; m = n; n = p;
            //} while (n != 0); return (m);
            return gcd(m, n);
        }
        int gcd(int a, int b)
        {
            while (b != 0)
                b = a % (a = b);
            return a;
        }
        public void PrintRational(string name)
        {
            Console.WriteLine(" {0} = {1}/{2}", name, m, n);
        }

    }
}
