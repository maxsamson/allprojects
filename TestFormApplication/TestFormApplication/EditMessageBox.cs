﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace TestFormApplication
{
    public class EditMessageBox
    {
        static string StringToCorrectDoubleFormat(string s) // ++++++
        {
            //Изменение строки в правильный формат для типа double
            if (s.IndexOf(',') == -1)
            {
                s = s.Replace('.', ',');
                return s;
            }
            return s;
        }

        public static DialogResult InputBox()
        {
            #region creating controls

            var form = new Form();
            var lbMin = new Label();
            var lbMax = new Label();
            var lbDayMin = new Label();
            var cbDayMin = new ComboBox();
            var lbMonthMin = new Label();
            var cbMonthMin = new ComboBox();
            var lbYearMin = new Label();
            var cbYearMin = new ComboBox();
            var lbDayMax = new Label();
            var cbDayMax = new ComboBox();
            var lbMonthMax = new Label();
            var cbMonthMax = new ComboBox();
            var lbYearMax = new Label();
            var cbYearMax = new ComboBox();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            #endregion

            #region text and items of controls

            form.Text = "Задать интервал";

            lbMin.Text = "От:";
            lbMax.Text = "До:";

            lbDayMin.Text = "День:";
            for (var i = 1; i <= 31; i++)
            {
                cbDayMin.Items.Add(i);
            }
            cbDayMin.SelectedIndex = DateTime.Now.Day - 1;

            lbMonthMin.Text = "Месяц:";
            cbMonthMin.Items.AddRange(DateTimeFormatInfo.CurrentInfo.MonthNames);
            cbMonthMin.Items.RemoveAt(cbMonthMin.Items.Count - 1);
            cbMonthMin.SelectedIndex = DateTime.Now.Month - 1;

            lbYearMin.Text = "Год:";
            for (var i = 0; i < 20; i++)
            {
                cbYearMin.Items.Add(DateTime.Now.Year - i);
            }
            cbYearMin.SelectedIndex = 0;

            lbDayMax.Text = "День:";
            for (var i = 1; i <= 31; i++)
            {
                cbDayMax.Items.Add(i);
            }
            cbDayMax.SelectedIndex = DateTime.Now.Day - 1;

            lbMonthMax.Text = "Месяц:";
            cbMonthMax.Items.AddRange(DateTimeFormatInfo.CurrentInfo.MonthNames);
            cbMonthMax.Items.RemoveAt(cbMonthMax.Items.Count - 1);
            cbMonthMax.SelectedIndex = DateTime.Now.Month - 1;

            lbYearMax.Text = "Год:";
            for (var i = 0; i < 20; i++)
            {
                cbYearMax.Items.Add(DateTime.Now.Year - i);
            }
            cbYearMax.SelectedIndex = 0;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            #endregion

            #region bounds

            lbMin.SetBounds(32, 10, 124, 13);

            lbDayMin.SetBounds(32, 26, 124, 13);
            cbDayMin.SetBounds(35, 42, 40, 21);

            lbMonthMin.SetBounds(92, 26, 124, 13);
            cbMonthMin.SetBounds(95, 42, 90, 21);

            lbYearMin.SetBounds(202, 26, 124, 13);
            cbYearMin.SetBounds(205, 42, 50, 21);

            lbMax.SetBounds(32, 76, 124, 13);

            lbDayMax.SetBounds(32, 92, 124, 13);
            cbDayMax.SetBounds(35, 108, 40, 21);

            lbMonthMax.SetBounds(92, 92, 124, 13);
            cbMonthMax.SetBounds(95, 108, 90, 21);

            lbYearMax.SetBounds(202, 92, 124, 13);
            cbYearMax.SetBounds(205, 108, 50, 21);

            buttonOk.SetBounds(228, 144, 75, 23);
            buttonCancel.SetBounds(309, 144, 75, 23);

            #endregion

            #region anchor

            lbMin.AutoSize = true;

            lbDayMin.AutoSize = true;
            lbMonthMin.AutoSize = true;
            lbYearMin.AutoSize = true;

            lbMax.AutoSize = true;

            lbDayMax.AutoSize = true;
            lbMonthMax.AutoSize = true;
            lbYearMax.AutoSize = true;

            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            #endregion

            #region properties of date

            cbDayMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDayMin.IntegralHeight = false;
            cbDayMin.MaxDropDownItems = 6;

            cbMonthMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cbMonthMin.IntegralHeight = false;
            cbMonthMin.MaxDropDownItems = 6;

            cbYearMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cbYearMin.IntegralHeight = false;
            cbYearMin.MaxDropDownItems = 6;

            cbDayMax.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDayMax.IntegralHeight = false;
            cbDayMax.MaxDropDownItems = 6;

            cbMonthMax.DropDownStyle = ComboBoxStyle.DropDownList;
            cbMonthMax.IntegralHeight = false;
            cbMonthMax.MaxDropDownItems = 6;

            cbYearMax.DropDownStyle = ComboBoxStyle.DropDownList;
            cbYearMax.IntegralHeight = false;
            cbYearMax.MaxDropDownItems = 6;

            #endregion

            form.ClientSize = new Size(396, 179);
            form.Controls.AddRange(new Control[] { lbMin, lbDayMin, cbDayMin, lbMonthMin, cbMonthMin, lbYearMin, cbYearMin,
                lbMax, lbDayMax, cbDayMax, lbMonthMax, cbMonthMax, lbYearMax, cbYearMax, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, lbYearMin.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            var dialogResult = form.ShowDialog();
            //date = new DateTime(Convert.ToInt32(cbYearMin.SelectedItem.ToString()), cbMonthMin.SelectedIndex + 1, cbDayMin.SelectedIndex + 1);
            return dialogResult;
        }

        public static DialogResult InputBox(ref string name, ref double cost, ref double count, ref string type, ref string importance, List<string> listType, List<string> listImportance)
        {
            CultureInfo culInf = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentCulture = culInf;
            Thread.CurrentThread.CurrentUICulture = culInf;

            Form form = new Form();
            Label lbName = new Label();
            TextBox tbName = new TextBox();
            Label lbCost = new Label();
            TextBox tbCost = new TextBox();
            Label lbCount = new Label();
            TextBox tbCount = new TextBox();
            Label lbType = new Label();
            ComboBox cbType = new ComboBox();
            Label lbImportance = new Label();
            ComboBox cbImportance = new ComboBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = "Изменение";

            lbName.Text = "Название:";
            tbName.Text = name;

            lbCost.Text = "Стоимость:";
            tbCost.Text = Convert.ToString(cost);

            lbCount.Text = "Количество:";
            tbCount.Text = Convert.ToString(count);

            lbType.Text = "Тип:";
            foreach (string item in listType)
            {
                cbType.Items.Add(item);
            }
            cbType.SelectedIndex = cbType.Items.IndexOf(type);

            lbImportance.Text = "Важность:";
            foreach (string item in listImportance)
            {
                cbImportance.Items.Add(item);
            }
            cbImportance.SelectedIndex = cbImportance.Items.IndexOf(importance);

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            lbName.SetBounds(9, 20, 372, 13);
            tbName.SetBounds(12, 36, 372, 20);

            lbCost.SetBounds(9, 59, 372, 13);
            tbCost.SetBounds(12, 75, 372, 20);

            lbCount.SetBounds(9, 98, 372, 13);
            tbCount.SetBounds(12, 114, 372, 20);

            lbType.SetBounds(9, 137, 372, 13);
            cbType.SetBounds(12, 153, 372, 21);

            lbImportance.SetBounds(9, 177, 372, 13);
            cbImportance.SetBounds(12, 193, 372, 21);

            buttonOk.SetBounds(228, 235, 75, 23);
            buttonCancel.SetBounds(309, 235, 75, 23);

            lbName.AutoSize = true;
            tbName.Anchor = tbName.Anchor | AnchorStyles.Right;

            lbCost.AutoSize = true;
            tbCost.Anchor = tbName.Anchor | AnchorStyles.Right;

            lbCount.AutoSize = true;
            tbCount.Anchor = tbName.Anchor | AnchorStyles.Right;

            lbType.AutoSize = true;
            cbType.Anchor = cbType.Anchor | AnchorStyles.Right;

            lbImportance.AutoSize = true;
            cbImportance.Anchor = cbImportance.Anchor | AnchorStyles.Right;

            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 270);
            form.Controls.AddRange(new Control[] { lbName, tbName, lbCost, tbCost, lbCount, tbCount, lbType, cbType, lbImportance, cbImportance, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, lbName.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();

            name = tbName.Text;

            cost = Convert.ToDouble(StringToCorrectDoubleFormat(tbCost.Text));

            count = Convert.ToDouble(StringToCorrectDoubleFormat(tbCount.Text));

            type = cbType.SelectedItem.ToString();

            importance = cbImportance.SelectedItem.ToString();

            return dialogResult;
        }

        public static DialogResult InputBox(ref string name, ref double cost, ref double count, ref string type, ref string importance, ref DateTime date, List<string> listType, List<string> listImportance)
        {
            #region creating controls

            var form = new Form();
            var dtMin = new DateControl();
            var lbMin = new Label();
            var dtMax = new DateControl();
            var lbMax = new Label();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            #endregion

            #region text and items of controls

            form.Text = "Задать интервал";

            lbMin.Text = "От:";
            lbMax.Text = "До:";

            //lbDayMin.Text = "День:";
            //for (var i = 1; i <= 31; i++)
            //{
            //    cbDayMin.Items.Add(i);
            //}
            //cbDayMin.SelectedIndex = DateTime.Now.Day - 1;

            dtMin.FillDay();

            //lbMonthMin.Text = "Месяц:";
            //cbMonthMin.Items.AddRange(DateTimeFormatInfo.CurrentInfo.MonthNames);
            //cbMonthMin.Items.RemoveAt(cbMonthMin.Items.Count - 1);
            //cbMonthMin.SelectedIndex = DateTime.Now.Month - 1;

            dtMin.FillMonth();

            //lbYearMin.Text = "Год:";
            //for (var i = 0; i < 20; i++)
            //{
            //    cbYearMin.Items.Add(DateTime.Now.Year - i);
            //}
            //cbYearMin.SelectedIndex = 0;

            dtMin.FillYear();

            //lbDayMax.Text = "День:";
            //for (var i = 1; i <= 31; i++)
            //{
            //    cbDayMax.Items.Add(i);
            //}
            //cbDayMax.SelectedIndex = DateTime.Now.Day - 1;

            dtMax.FillDay();

            //lbMonthMax.Text = "Месяц:";
            //cbMonthMax.Items.AddRange(DateTimeFormatInfo.CurrentInfo.MonthNames);
            //cbMonthMax.Items.RemoveAt(cbMonthMax.Items.Count - 1);
            //cbMonthMax.SelectedIndex = DateTime.Now.Month - 1;

            dtMax.FillMonth();

            //lbYearMax.Text = "Год:";
            //for (var i = 0; i < 20; i++)
            //{
            //    cbYearMax.Items.Add(DateTime.Now.Year - i);
            //}
            //cbYearMax.SelectedIndex = 0;

            dtMax.FillYear();

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            #endregion

            #region bounds

            lbMin.SetBounds(32, 10, 124, 13);

            lbDayMin.SetBounds(32, 26, 124, 13);
            cbDayMin.SetBounds(35, 42, 40, 21);

            lbMonthMin.SetBounds(92, 26, 124, 13);
            cbMonthMin.SetBounds(95, 42, 90, 21);

            lbYearMin.SetBounds(202, 26, 124, 13);
            cbYearMin.SetBounds(205, 42, 50, 21);

            lbMax.SetBounds(32, 76, 124, 13);

            lbDayMax.SetBounds(32, 92, 124, 13);
            cbDayMax.SetBounds(35, 108, 40, 21);

            lbMonthMax.SetBounds(92, 92, 124, 13);
            cbMonthMax.SetBounds(95, 108, 90, 21);

            lbYearMax.SetBounds(202, 92, 124, 13);
            cbYearMax.SetBounds(205, 108, 50, 21);

            buttonOk.SetBounds(228, 144, 75, 23);
            buttonCancel.SetBounds(309, 144, 75, 23);

            #endregion

            #region anchor

            lbMin.AutoSize = true;

            lbDayMin.AutoSize = true;
            lbMonthMin.AutoSize = true;
            lbYearMin.AutoSize = true;

            lbMax.AutoSize = true;

            lbDayMax.AutoSize = true;
            lbMonthMax.AutoSize = true;
            lbYearMax.AutoSize = true;

            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            #endregion

            #region properties of date

            cbDayMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDayMin.IntegralHeight = false;
            cbDayMin.MaxDropDownItems = 6;

            cbMonthMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cbMonthMin.IntegralHeight = false;
            cbMonthMin.MaxDropDownItems = 6;

            cbYearMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cbYearMin.IntegralHeight = false;
            cbYearMin.MaxDropDownItems = 6;

            cbDayMax.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDayMax.IntegralHeight = false;
            cbDayMax.MaxDropDownItems = 6;

            cbMonthMax.DropDownStyle = ComboBoxStyle.DropDownList;
            cbMonthMax.IntegralHeight = false;
            cbMonthMax.MaxDropDownItems = 6;

            cbYearMax.DropDownStyle = ComboBoxStyle.DropDownList;
            cbYearMax.IntegralHeight = false;
            cbYearMax.MaxDropDownItems = 6;

            #endregion

            cbMonthMin.SelectedIndexChanged += cbMonthMin_SelectedIndexChanged;

            form.ClientSize = new Size(396, 179);
            form.Controls.AddRange(new Control[] { lbMin, lbDayMin, cbDayMin, lbMonthMin, cbMonthMin, lbYearMin, cbYearMin,
                lbMax, lbDayMax, cbDayMax, lbMonthMax, cbMonthMax, lbYearMax, cbYearMax, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, lbYearMin.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            var dialogResult = form.ShowDialog();
            dateMin = new DateTime(Convert.ToInt32(cbYearMin.SelectedItem.ToString()), cbMonthMin.SelectedIndex + 1, cbDayMin.SelectedIndex + 1);
            dateMax = new DateTime(Convert.ToInt32(cbYearMax.SelectedItem.ToString()), cbMonthMax.SelectedIndex + 1, cbDayMax.SelectedIndex + 1);
            return dialogResult;
        }
    }
}
