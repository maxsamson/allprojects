﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_SomeConstraints
{
    class Program
    {
        class Gen<T, V> where T : class where V : struct
        {
            T obj1;
            V obj2;

            public Gen()
            {
                obj1 = default(T);
                obj2 = default(V);
            }

            public Gen(T t, V v)
            {
                obj1 = t;
                obj2 = v;
            }

            public void Display()
            {
                Console.WriteLine("T - {0}, V - {1}", obj1, obj2);
            }
        }

        static void Main(string[] args)
        {
            Gen<string, int> gen = new Gen<string, int>();
            gen.Display();
        }
    }
}
