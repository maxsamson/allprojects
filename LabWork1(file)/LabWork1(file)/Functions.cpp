#include "Functions.h"


using namespace std;

GOODS* AddStruct(GOODS* Obj, const int amount) // ������� ���������� ����� � �������
{
	if (amount == 0)
	{
		Obj = new GOODS[amount + 1]; // ��������� ������ ��� ������ ���������
	}
	else
	{
		GOODS* tempObj = new GOODS[amount + 1];

		for (int i = 0; i < amount; i++)
		{
			tempObj[i] = Obj[i]; // �������� �� ��������� ������
		}
		delete[] Obj;

		Obj = tempObj;
	}
	return Obj;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setData(GOODS* &Obj, int &amount) // ������� ���������� ������ ���������� ���������
{
	ifstream in("..\\Debug\\1.txt");

	char name[80];
	float cost;
	int count;
	char suppliers[3][80];

	if (in.is_open())
	{
		while (in >> name >> cost >> count >> suppliers[0] >> suppliers[1] >> suppliers[2])
		{
			Obj = AddStruct(Obj, amount); // ����������� ������ ������� �� 1
			amount++;

			strcpy(Obj[amount - 1].name, name);
			Obj[amount - 1].cost = cost;
			Obj[amount - 1].count = count;
			strcpy(Obj[amount - 1].suppliers[0], suppliers[0]);
			strcpy(Obj[amount - 1].suppliers[1], suppliers[1]);
			strcpy(Obj[amount - 1].suppliers[2], suppliers[2]);
		}
	}
	in.close();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void showData(const GOODS* Obj, const int amount) // ������� ����������� �������
{
	system("cls");
	for (int i = 0; i < amount; i++)
	{
		cout << "����� �" << i + 1 << ":" << endl;
		cout << "\t�����: " << Obj[i].name << endl <<
			"\t�������: " << Obj[i].cost << endl <<
			"\tʳ������: " << Obj[i].count << endl <<
			"\t����������: ";
		for (int j = 0; j < 3; j++)
		{
			cout << Obj[i].suppliers[j];
			if (j < 2)
				cout << "; ";
			else
				cout << ".";
		}
		cout << endl;
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void sortData(GOODS* Obj, const int amount) // ������� ���������� ������� �� ��������� ���������
{
	GOODS tmp;
	bool exit = false;
	while (!exit)
	{
		exit = true;
		for (int i = 0; i < amount - 1; i++)
		{
			if (strcmp(Obj[i].name, Obj[i + 1].name) > 0)
			{
				tmp = Obj[i];
				Obj[i] = Obj[i + 1];
				Obj[i + 1] = tmp;
				exit = false;
			}
		}
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void searchData(const GOODS* Obj, const int amount) // ������� ������ �������� �� �����
{
	system("cls");
	cout << "������ ����� ������ ��� ������: ";
	char name[80];
	cin >> name;
	bool b = 0;
	for (int i = 0; i < amount; i++)
	{
		if (strcmp(Obj[i].name, name) == 0)
		{
			cout << endl << "����������: ";
			for (int j = 0; j < 3; j++)
			{
				cout << Obj[i].suppliers[j];
				if (j < 2)
					cout << "; ";
				else
					cout << ".";
			}
			cout << endl << "ʳ������ ������: " << Obj[i].count;
			b = 1;
		}
		if (b == 1)
			break;
	}
	if (b == 0)
		cout << "������ � ������ ��������� �� �������!" << endl;
}