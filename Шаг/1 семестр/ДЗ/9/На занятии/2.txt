#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

//���������� ������, ����� �� �����, ������� 2� ��������� � ��������� �������

using namespace std;

struct Date{ int a; int b; int c; };
struct Student{ char a[30]; char b[20]; char c[30]; int d; Date e; };
struct Group{ Student *Students; int count; };
void Fill(struct Student &a);
void Show(struct Student &a);
void Add(struct Group &a);
void Delete(struct Group &a);
int CompareF(Student a, Student b);
int CompareAge(Student a, Student b);
template <typename T> void Sort(T* a, int count, int (*cmp)(T, T));

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	int x;
	Group *X = new Group;
	cout << "������� ���������� ���������: ";
	cin >> x;
	X->count = x;
	cout << endl;
	if (X->count == 0)
		cout << "� ������ ��� ���������." << endl;
	else
	{
		X->Students = new Student[X->count];
		for (int i = 0; i < X->count; i++)
			Fill(X->Students[i]);
		for (int i = 0; i < X->count; i++)
			Show(X->Students[i]);
		Add(*X);
		for (int i = 0; i < X->count; i++)
			Show(X->Students[i]);
		int(*cmp)(Student, Student) = CompareF;
		Sort(X->Students, X->count, cmp);
		for (int i = 0; i < X->count; i++)
			Show(X->Students[i]);
		cout << endl;
		cmp = CompareAge;
		Sort(X->Students, X->count, cmp);
		for (int i = 0; i < X->count; i++)
			Show(X->Students[i]);
		cout << endl;
		/*Delete(*X);
		if (X->count == 0)
			cout << "� ������ ��� ���������." << endl;
		else
			for (int i = 0; i < X->count; i++)
				Show(X->Students[i]);*/
	}
	system("pause");
}

void Fill(struct Student &a)
{
	cout << "������� ������� ��������: ";
	fflush(stdin);
	gets(a.a);
	cout << "������� ��� ��������: ";
	fflush(stdin);
	gets(a.b);
	cout << "������� �������� ��������: ";
	fflush(stdin);
	gets(a.c);
	cout << "������� ������� ��������: ";
	cin >> a.d;
	cout << "������� ���� �������� � ���� ��, ��, ��: ";
	cin >> a.e.a;
	cin >> a.e.b;
	cin >> a.e.c;
	cout << endl;
}

void Show(struct Student &a)
{
	cout << a.a << endl;
	cout << a.b << endl;
	cout << a.c << endl;
	cout << a.d << endl;
	cout << a.e.a << '.' << a.e.b << '.' << a.e.c << endl;
	cout << endl;
}

void Add(struct Group &a)
{
	int x;
	cout << "������� ���������� ����������� ���������: ";
	cin >> x;
	cout << endl;
	Student* A = new Student[a.count + x];
	for (int i = 0; i < a.count; i++)
		A[i] = a.Students[i];
	for (int i = a.count; i < a.count + x; i++)
		Fill(A[i]);
	delete[] a.Students;
	a.Students = A;
	a.count += x;
}

void Delete(struct Group &a)
{
	char x[30];
	cout << "������� ������� �������� ��� ��������: ";
	fflush(stdin);
	gets(x);
	int count = 0;
	for (int i = 0; i < a.count; i++)
	{
		for (int j = 0; j < strlen(a.Students[i].a); j++)
		{
			if (a.Students[i].a[j] == x[j])
			{
				count = i;
				break;
			}
			else
				count = -1;
		}
	}
	if (count == -1)
	{
		cout << "��� ������ ��������." << endl;
	}
	else
	{
		a.count--;
		Student* A = new Student[a.count];
		for (int i = 0; i < count; i++)
			A[i] = a.Students[i];
		for (int i = count + 1; i < a.count; i++)
			A[i] = a.Students[i];
		delete[] a.Students;
		a.Students = A;
		cout << "�������� ����������� ������." << endl;
	}
}

int CompareF(Student a, Student b)
{
	return _stricmp(a.a, b.a);
}

int CompareAge(Student a, Student b)
{
	if (a.d > b.d)
		return 1;
	else
		return 0;
}

template <typename T> void Sort(T* a, int count, int (*cmp)(T, T))
{
	for (int i = 0; i < count; i++)
	{
		for (int j = i; j < count; j++)
		{
			if ((*cmp)(a[i], a[j]) > 0)
			{
				T X[30];
				X[0] = a[i];
				a[i] = a[j];
				a[j] = X[0];
			}
		}
	}
}