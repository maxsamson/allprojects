﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ConstraintBaseClass
{
    class PhoneList<T> where T : PhoneNumber
    {
        List<T> phList;
        public PhoneList()
        {
            phList = new List<T>();
        }
        // Добавить элемент в список. 
        public bool Add(T newEntry)
        {
            phList.Add(newEntry);
            return true;
        }
        // Найти и возвратить сведения о телефоне по заданному имени. 
        public T FindByName(string name)
        {
            // Имя может использоваться, потому что его свойство Name
            foreach (var item in phList)
            {
                if (item.Name == name)
                    return item;
            }
            // Имя отсутствует в списке. 
            throw new Exception("Not Found");
        }
        // Найти и возвратить сведения о телефоне по заданному номеру. 
        public T FindByNumber(string number)
        {
            foreach (var item in phList)
            {
                if (item.Number == number)
                    return item;
            }
            // Номер телефона отсутствует в списке. 
            throw new Exception("Not Found");
        }
    }
}
