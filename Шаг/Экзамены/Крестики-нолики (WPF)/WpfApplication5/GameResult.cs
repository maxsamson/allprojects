﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication5
{
    public enum GameResult : byte
    {
        /// <summary>
        /// Выйграли крестики
        /// </summary>
        CrossWin, 
        /// <summary>
        /// Выйграли нолики
        /// </summary>
        ZeroWin,
        /// <summary>
        /// Ничья
        /// </summary>
        Draw,
        /// <summary>
        /// Игра продолжается
        /// </summary>
        ContinueToPlay
    }
}
