﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace RandomEvent
{
    class Receiver
    {
        public void Message(object sender, MyEventArgs e)
        {
            //Console.WriteLine("Сгенерировано число: {0}", e.value);

            Debug.WriteLine("Сгенерировано число: {0}", e.value);
        }
    }
}
