#include "Shapes.h"

#define N 12 // ������ ����
#define M 22 //������ ����

int field[M][N]; //������� ����
char symb = '*'; //������, ������� ��������� ������
int Next; //��� ��������� ������

bool Check_Bump(Shape* s) //��������, ����� �� ������ �� �����
{
	bool f = false;
	COORD* tmp = s->getPos();
	for (int i = 0; i < 4; i++)
	{
		if (field[tmp[i].Y + 1][tmp[i].X] == 1)
		{
			f = true;
			break;
		}
	}
	if (f)
	{
		for (int i = 0; i < 4; i++)
		{
			field[tmp[i].Y][tmp[i].X] = 1;
			tmp[i].X = tmp[i].Y = 0;
		}
		return true;
	}
	return false;
}

int Random_Shape(int t) //��������� ����� ������
{
	t = Next;
	Next = rand() % 7;
	return t;
}

void main()
{
	//���� ��� �������� ��������� �������
	void* handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO structCursorInfo;
	GetConsoleCursorInfo(handle, &structCursorInfo);
	structCursorInfo.bVisible = FALSE;
	SetConsoleCursorInfo(handle, &structCursorInfo);

	//���� ��� ���������� ������� �������� ����
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if ((i == M - 1) || (j == 0) || (j == N - 1))
				field[i][j] = 1;
			else
				field[i][j] = 0;
		}
	}

	//���� ��� ������ ������ �������� ����
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (field[i][j] == 0)
				putchar(' ');
			else
				putchar(176);
		}
		putchar('\n');
	}

	COORD centre = { 5,0 }; //���������� ������, ��� ���������� ������

	//�������� ������ ��������� ������
	Next = rand() % 2;
	Shape* s;
	if (Next == 0)
		s = new Line(centre);
	else if (Next == 1)
		s = new Lblock(centre);
	/*else if (Next == 2)
	s = new ReverseLblock(centre);
	else if (Next == 3)
	s = new Zblock(centre);
	else if (Next == 4)
	s = new ReverseZblock(centre);
	else if (Next == 5)
	s = new Square(centre);
	else if (Next == 6)
	s = new Tblock(centre);*/
	else
		s = new Shape();
	s->setType(Next);
	Next = rand() % 2;

	COORD* tmp;
	int t;
	char c = '\0';
	int timer = 0;

	while (true)
	{
		timer++;

		if (_kbhit())
		{
			c = _getch();
			if (c < 0)
				c = _getch();

			tmp = s->getPos();

			if (c == M_UP)
			{
				tmp = s->getPos();
				t = s->getType();
				if ((tmp[1].X == 1 || tmp[1].X >= N - 2) && (t == 0))
					return;
				if ((t == 1 || t == 2 || t == 3 || t == 4) && (tmp[1].X == 1 || tmp[1].X > N - 2))
					return;
				s->setState();
				s->Turn();
				c = '\0';
			}
			if (c == M_DOWN)
			{
				while (true)
				{
					s->Draw(symb);

					tmp = s->getPos();

					for (int i = 0; i < 4; i++)
					{
						field[tmp[i].Y][tmp[i].X] = -1;
					}

					//�������� �� ��, �������� �� ������ �� ���-�� �����
					if (!Check_Bump(s))
					{
						s->Draw(' ');

						tmp = s->getPos();

						for (int i = 0; i < 4; i++)
						{
							field[tmp[i].Y][tmp[i].X] = 0;
						}

						centre.Y++;
						s->ChangePos(M_DOWN);
					}
					else
						goto down;
				}
			}
			if (c == M_LEFT)
			{
				for (int i = 0; i < 4; i++)
					if (field[tmp[i].Y][tmp[i].X - 1] == 1)
						goto left_right;

				s->Draw(' ');

				for (int i = 0; i < 4; i++)
				{
					field[tmp[i].Y][tmp[i].X] = 0;
				}

				s->ChangePos(M_LEFT);
			}
			if (c == M_RIGHT)
			{
				for (int i = 0; i < 4; i++)
					if (field[tmp[i].Y][tmp[i].X + 1] == 1)
						goto left_right;

				s->Draw(' ');

				for (int i = 0; i < 4; i++)
				{
					field[tmp[i].Y][tmp[i].X] = 0;
				}

				s->ChangePos(M_RIGHT);
			}
			if (c == M_ESC)
			{
				exit(0);
				break;
			}
		}

		if (timer >= 3000)
		{

		left_right: //����������� ��� �������� �����-������

			timer = 0;

			s->Draw(symb);

			tmp = s->getPos();

			for (int i = 0; i < 4; i++)
			{
				field[tmp[i].Y][tmp[i].X] = -1;
			}

			Sleep(300);

			//�������� �� ��, �������� �� ������ �� ���-�� �����
			if (!Check_Bump(s))
			{
				s->Draw(' ');

				tmp = s->getPos();

				for (int i = 0; i < 4; i++)
				{
					field[tmp[i].Y][tmp[i].X] = 0;
				}

				centre.Y++;
				s->ChangePos(M_DOWN);
			}
			else
			{
			down:

				centre.Y = 0;
				s->setType(Next);
				if (Next == 0)
					s = new Line(centre);
				else if (Next == 1)
					s = new Lblock(centre);
				/*else if (Next == 2)
					s = new ReverseLblock(centre);
				else if (Next == 3)
					s = new Zblock(centre);
				else if (Next == 4)
					s = new ReverseZblock(centre);
				else if (Next == 5)
					s = new Square(centre);
				else if (Next == 6)
					s = new Tblock(centre);*/

				Next = rand() % 2;
			}
		}
	}

	_getch();
}