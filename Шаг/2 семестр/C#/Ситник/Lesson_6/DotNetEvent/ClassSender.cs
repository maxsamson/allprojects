﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetEvent
{
    //delegate void MyHendler(object sender, EventArgs e);
    class MyEventArgs : EventArgs
    {
        public int value { get; set; } 
    }

    delegate void MyEventHandler(object sender, MyEventArgs e);

    class ClassSender
    {
        public event MyEventHandler MyEvent;
        MyEventArgs arg = new MyEventArgs();
        protected void OnMyEvent(MyEventArgs e)
        {
            if (MyEvent != null)
                MyEvent(this, e);
        }

        public int Counter()
        {
            int sum = 0;
            for (int i = 0; i <= 100; i++)
            {
                sum += i;
                if (i == 12 || i == 33 || i == 71)
                {
                    arg.value = i;
                    OnMyEvent(arg);
                }
            }
            return sum;
        }

        public void Display(object sender, MyEventArgs e)
        {
            Console.WriteLine("Fired Event i = {0}", e.value);
        }
    }
}
