﻿using System;
using System.Reflection;

namespace ConsoleApplication1
{
    // Тестовый класс, содержащий некоторые конструкции
    class MyTestClass
    {
        double d, f;

        public MyTestClass(double d, double f)
        {
            this.d = d;
            this.f = f;
        }

        public double Sum()
        {
            Console.WriteLine(d + f);
            return d + f;
        }

        public void Info()
        {
            Console.WriteLine(@"d = {0}
f = {1}", d, f);
        }

        public void Set(int a, int b)
        {
            d = (double)a;
            f = (double)b;
        }

        public void Set(double a, double b)
        {
            d = a;
            f = b;
        }

        public override string ToString()
        {
            return "MyTestClass";
        }
    }

    // В данном классе определены методы использующие рефлексию
    class Reflect
    {
        // Данный метод выводит информацию о содержащихся в классе методах
        public static void MethodReflectInfo<T>(T obj) where T : class
        {
            Type t = typeof(T);
            // Получаем коллекцию методов
            MethodInfo[] MArr = t.GetMethods();
            Console.WriteLine("*** Список методов класса {0} ***\n", obj.ToString());

            // Вывести методы
            foreach (MethodInfo m in MArr)
            {
                Console.Write(" --> " + m.ReturnType.Name + " \t" + m.Name + "(");
                // Вывести параметры методов
                ParameterInfo[] p = m.GetParameters();
                for (int i = 0; i < p.Length; i++)
                {
                    Console.Write(p[i].ParameterType.Name + " " + p[i].Name);
                    if (i + 1 < p.Length) Console.Write(", ");
                }
                Console.Write(")\n");
            }
        }

        public static Type GetTypeInfo<T>(T obj) where T : class
        {
            return typeof(T);
        }
    }

    class Program
    {
        static void Main()
        {
            MyTestClass mtc = new MyTestClass(12.0, 3.5);

            double d1 = 12.0;
            double d2 = 3.5;

            Type typeInfo = Reflect.GetTypeInfo(mtc);
            dynamic st = Activator.CreateInstance(typeInfo, new object[] { d1, d2 });
            MethodInfo[] methodInfo = typeInfo.GetMethods();

            foreach (var item in methodInfo)
            {
                if (item.Name == "Sum")
                {
                    item.Invoke(st, null);
                }
            }

            Console.ReadLine();
        }
    }
}