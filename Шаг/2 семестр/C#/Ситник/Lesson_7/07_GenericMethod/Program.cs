﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_GenericMethod
{
    class Program
    {
        static void ShowType<T>() where T : class, new()
        {
            Console.WriteLine("Type T - {0}", typeof(T));
        }

        static void Swap<T>(ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }

        static void Main(string[] args)
        {
            int a = 21, b = 12;
            Swap(ref a, ref b);
            Console.WriteLine("a = {0}, b = {1}", a, b);

            string s1 = "Hello!", s2 = "Test";
            Swap(ref s1, ref s2);
            Console.WriteLine("s1 = {0}, s2 = {1}", s1, s2);

            ShowType<Program>();
        }
    }
}
