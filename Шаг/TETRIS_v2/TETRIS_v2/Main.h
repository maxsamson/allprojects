#pragma once

#define row 25
#define col 10
#define cell_width 20

///////////////////////////////////////////////
/// ����� "������"
///////////////////////////////////////////////
class Cell
{
	int cell_x; // ���������� �
	int cell_y; // ���������� Y
	bool status; // ��������� on/off
	int id; // �������������

public:
	Cell();
	~Cell();

	Cell(int x, int y, int id); 
	
	Cell(const Cell &obj)
	{
		cell_x = obj.cell_x;
		cell_y = obj.cell_y;
		status = obj.status;
		id = obj.id;
	}

	Cell& operator=(const Cell& obj)
	{
		if (this == &obj)
			return *this;

		cell_x = obj.cell_x;
		cell_y = obj.cell_y;
		status = obj.status;
		id = obj.id;
		return *this;
	}

	void set_xy(int numX, int numY) { cell_x = numX; cell_y = numY; }
	int get_cell_x() { return cell_x; }
	int get_cell_y() { return cell_y; }
	void set_id(int num) { id = num; }
	int get_id() { return id; }
	void set_status(bool num) { status = num; }
	bool get_status() { return status; }

	void Draw(HWND h);

};

///////////////////////////////////////////////
/// ����� "������"
///////////////////////////////////////////////

class Shape
{
	/*  
	��� ������
	1 = Line
	2 = RightL
	3 = LeftL
	4 = Square;
	5 = Pyramide;
	6 = RightZ;
	7 = LeftZ; 
	*/
	
	int rot; // ���������� ��������� ��� ��������
	Shape* next; //��������� �� ��������� ������
	Shape* prev; //��������� �� ���������� ������

public:
	Cell* shape_cell[4]; //������ ���������� �� 4 ������, �� ������� ������� ������
	Shape();
	~Shape() {};

	void set_rot(int num) { rot = num; }
	int get_rot() { return rot; }

	void set_Next(Shape* s) { next = s; }
	Shape* get_Next() { return next; }

	void set_Prev(Shape* s) { prev = s; }
	Shape* get_Prev() { return prev; }
	
	virtual void Shape_Draw(HWND h);
	virtual void Shape_destroy(HWND h);
	virtual void New_Shape(int x, int y, Cell** arr) {};
	virtual bool Move_down(Cell** arr, HWND h) { return 1; };
	virtual void Move_right(Cell** arr, HWND h) {};
	virtual void Move_left(Cell** arr, HWND h) {};
	virtual void Rotate(Cell** arr, HWND h) {};

	Shape(const Shape &obj)
	{
		for (int i = 0; i < 4; i++)
			shape_cell[i] = obj.shape_cell[i];

		next = obj.next;
	}
	Shape& operator=(const Shape& obj)
	{
		if (this == &obj)
			return *this;

		for (int i = 0; i < 4; i++)
			shape_cell[i] = obj.shape_cell[i];

		next = obj.next;
	}
};

class Line : public Shape
{
public:

	Line() :Shape() {};

	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
	virtual void Rotate(Cell** arr, HWND h) override;
};

class RightL : public Shape
{
public:

	RightL() :Shape() {};

	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
	virtual void Rotate(Cell** arr, HWND h) override;
};

class LeftL : public Shape
{
public:
	LeftL() :Shape() {};
	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
	virtual void Rotate(Cell** arr, HWND h) override;
};

class Square : public Shape
{
public:
	Square() :Shape() {};
	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
};

class Pyramide : public Shape
{
public:
	Pyramide() :Shape() {};
	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
	virtual void Rotate(Cell** arr, HWND h) override;
};

class RightZ : public Shape
{
public:
	RightZ() :Shape() {};
	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
	virtual void Rotate(Cell** arr, HWND h) override;
};

class LeftZ : public Shape
{
public:
	LeftZ() :Shape() {};
	virtual void New_Shape(int x, int y, Cell** arr) override;
	virtual bool Move_down(Cell** arr, HWND h) override;
	virtual void Move_right(Cell** arr, HWND h) override;
	virtual void Move_left(Cell** arr, HWND h) override;
	virtual void Rotate(Cell** arr, HWND h) override;
};

///////////////////////////////////////////////
/// ����� "�������"
///////////////////////////////////////////////

class Node     //������� Last in - last out
{
	int data;
	Node* next;
	Node* prev;

public:
	Node(int num = 0)
	{
		data = num;
		next = prev = nullptr;
	}

	void set_Data(int num) { data = num; }
	int get_Data() { return data; }
	void set_Next(Node *num) { next = num; }
	Node* get_Next() { return next; }

	void set_Prev(Node *p) { prev = p; }
	Node* get_Prev() { return prev; }
};

class Queue
{
	Node* start;
	Node* end;
	int size;

public:
	Queue()
	{
		start = end = nullptr;
		size = 0;
	}

	Node* get_start() { return start; }
	Node* get_end() { return end; }

	
	void Push_back(int d); 
	void Pop_start();
	
	Queue(const Queue & obj); // ������������ �����������
	
	Queue & operator = (const Queue & obj) //���������� ��������� ����������
	{
		if (this == &obj)
			return *this;
		while (size)
		{
			Pop_start();
		}
		Node* cur = obj.start;
		while (cur)
		{
			Push_back(cur->get_Data());
			cur = cur->get_Next();
		}
		return *this;
	}
	
	~Queue() //����������
	{
		while (size)
			Pop_start();
	}
};

///////////////////////////////////////////////
/// ����� "����"
///////////////////////////////////////////////
class Game
{
	const int maxId = 5000;
	bool first_init;
	int cur_id;
	Cell **arr;
	Shape *MyShape[7];
	int cur_shape;
	Shape* next_shape;
	int score;
	Queue ShapeList;
	

public:
	Game();
	~Game();
	
	bool get_first_init() { return first_init; }
	int get_score() { return score; }

	void Game_init(HWND h, HWND Box_score, UINT_PTR timer);
	void Show(HWND h, HWND Box_score);
	void Destroy(HWND h);
	void Move_Down(HWND h, HWND Box_score, UINT_PTR timer);
	void Move_Right(HWND h, HWND Box_score);
	void Move_Left(HWND h, HWND Box_score);
	void Rotation(HWND h, HWND Box_score);
	void Check(HWND h, HWND Box_score); // �������� �� ���������� ����
	bool Check_end(HWND h, HWND Box_score, Cell** arr, UINT_PTR timer); //�������� ����� ����
	void Show_next(HWND h, int shape_type, int count);  //���������� ��������� ������
};

