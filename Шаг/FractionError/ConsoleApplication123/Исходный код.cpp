#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

using namespace std;

class Fraction
{
	int numerator;
	int denominator;
public:
	Fraction(int a = 1, int b = 1)
	{
		int c = NOD(a, b);
		numerator = a;
		if (b == 0)
		{
			cout << "����������� �� ����� ���� ����� ����." << endl;
			system("pause");
			exit(1);
		}
		denominator = b;
		if (c != 0)
		{
			numerator /= c;
			denominator /= c;
		}
	}

	Fraction(const Fraction& obj)
	{
		numerator = obj.numerator;
		denominator = obj.denominator;
	}

	~Fraction()
	{

	}

	int NOD(int a, int b)
	{
		while (a > 0 && b > 0)
		{
			if (a > b)
				a %= b;
			else
				b %= a;
		}
		return a + b;
	}

	Fraction& operator=(const Fraction& obj)
	{
		numerator = obj.numerator;
		denominator = obj.denominator;
		return *this;
	}

	Fraction operator+(const Fraction& obj)
	{
		Fraction x;
		x.numerator = numerator*obj.denominator + denominator*obj.numerator;
		x.denominator = denominator*obj.denominator;
		int c = NOD(x.numerator, x.denominator);
		if (c != 0)
		{
			x.numerator /= c;
			x.denominator /= c;
		}
		return x;
	}

	Fraction operator+(int obj)
	{
		Fraction x;
		x.numerator += denominator*obj;
		x.denominator = denominator;
		int c = NOD(x.numerator, x.denominator);
		if (c != 0)
		{
			x.numerator /= c;
			x.denominator /= c;
		}
		return x;
	}

	Fraction operator-(const Fraction& obj)
	{
		Fraction x;
		x.numerator = numerator*obj.denominator - denominator*obj.numerator;
		x.denominator = denominator*obj.denominator;
		int c = NOD(x.numerator, x.denominator);
		if (c != 0)
		{
			x.numerator /= c;
			x.denominator /= c;
		}
		return x;
	}

	Fraction operator*(const Fraction& obj)
	{
		Fraction x;
		x.numerator = numerator*obj.numerator;
		x.denominator = denominator*obj.denominator;
		int c = NOD(x.numerator, x.denominator);
		if (c != 0)
		{
			x.numerator /= c;
			x.denominator /= c;
		}
		return x;
	}

	Fraction operator/(const Fraction& obj)
	{
		Fraction x;
		x.numerator = numerator*obj.denominator;
		x.denominator = denominator*obj.numerator;
		int c = NOD(x.numerator, x.denominator);
		if (c != 0)
		{
			x.numerator /= c;
			x.denominator /= c;
		}
		return x;
	}

	bool operator==(const Fraction& obj)
	{
		if ((*this - obj).numerator == 0)
			return true;
		return false;
	}

	bool operator<(const Fraction& obj)
	{
		if ((*this - obj).numerator < 0)
			return true;
		return false;
	}

	bool operator>(const Fraction& obj)
	{
		if ((*this - obj).numerator > 0)
			return true;
		return false;
	}

	void Show()
	{
		cout << numerator << "/" << denominator << endl;
	}
};

class FractionArray
{
	Fraction* arr;
	int size;
public:
	FractionArray(Fraction* arr, int size)
	{
		this->size = size;
		this->arr = new Fraction[this->size];
		SetArr(arr);
	}

	FractionArray(const FractionArray& obj)
	{
		size = obj.size;
		Fraction* arr = new Fraction[size];
		for (int i = 0; i < size; i++)
			arr[i] = obj.arr[i];
	}

	FractionArray()
	{

	}

	~FractionArray()
	{
		if (size == 0)
			return;
		delete[] arr;
		size = 0;
	}

	FractionArray& operator=(const FractionArray& obj)
	{
		size = obj.size;
		Fraction* x = new Fraction[size];
		for (int i = 0; i < size; i++)
			x[i] = obj.arr[i];
		delete[] arr;
		arr = x;
		return *this;
	}

	FractionArray operator+(Fraction& obj)
	{
		size++;
		Fraction* x = new Fraction[size];
		for (int i = 0; i < size - 1; i++)
			x[i] = arr[i];
		x[size - 1] = obj;
		delete[] arr;
		arr = x;
		return *this;
	}

	FractionArray operator++()
	{
		size++;
		Fraction* x = new Fraction[size];
		for (int i = 0; i < size - 1; i++)
			x[i] = arr[i];
		cout << "������� ����� ��� ����������:\n���������: ";
		int numerator;
		cin >> numerator;
		cout << endl << "�����������: ";
		int denominator;
		cin >> denominator;
		cout << endl;
		Fraction a(numerator, denominator);
		x[size - 1] = a;
		delete[] arr;
		arr = x;
		return *this;
	}

	FractionArray operator--()
	{
		size--;
		Fraction* x = new Fraction[size];
		for (int i = 0; i < size; i++)
			x[i] = arr[i];
		delete[] arr;
		arr = x;
		return *this;
	}

	Fraction operator[](int pos)
	{
		if ((pos >= 0) && (pos < size))
			return arr[pos];
		return NULL;
	}

	void SetSize(int size)
	{
		this->size = size;
	}

	void SetArr(Fraction* arr)
	{
		for (int i = 0; i < size; i++)
			this->arr[i] = arr[i];
	}

	int GetSize()
	{
		return size;
	}

	Fraction* GetArr()
	{
		return arr;
	}

	void Show()
	{
		for (int i = 0; i < size; i++)
		{
			arr[i].Show();
			cout << endl;
		}
	}
};

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	Fraction b[2];
	b[0] = Fraction(1, 2);
	b[1] = Fraction(1, 3);
	FractionArray a(b, 2);
	a.Show();
	++a;
	a.Show();
	system("pause");
}