#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>

using namespace std;

void Fill(char **A, int size);
void Show(char **A, int size);
void MinMax(char **A, char **B, int size);

void main()
{
	setlocale(LC_ALL, "RUS");
	srand(time(NULL));
	int size;
	cout << "������� ���������� �����: ";
	cin >> size;
	cout << endl;
	char **A = new char*[size];
	char **B = new char*[size];
	for (int i = 0; i < size; i++)
	{
		B[i] = new char[3];
		B[i][2] = '\0';
	}
	Fill(A, size);
	Show(A, size);
	MinMax(A, B, size);
	Show(B, size);
	system("pause");
}

void Fill(char **A, int size)
{
	for (int i = 0; i < size; i++)
	{
		char S[256];
		cout << "������� ������: ";
		fflush(stdin);
		gets(S);
		cout << endl;
		int x = strlen(S);
		A[i] = new char[x];
		for (int j = 0; j <= x; j++)
			A[i][j] = S[j];
	}
}

void Show(char **A, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; A[i][j] != '\0'; j++)
			cout << A[i][j];
		cout << endl;
	}
	cout << endl;
}

void MinMax(char **A, char **B, int size)
{
	int k = 0;
	char Min, Max;
	for (int i = 0; i < size; i++)
	{
		Min = A[i][k];
		Max = A[i][k];
		for (int j = 0; A[i][j] != '\0'; j++)
		{
			if (A[i][j] < Min)
				Min = A[i][j];
			if (A[i][j] > Max)
				Max = A[i][j];
		}
		B[i][0] = Min;
		B[i][1] = Max;
	}
}